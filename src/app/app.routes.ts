import { PlantillaComponent } from './components/plantilla.component';
import { PlantillaSdkDaonComponent } from './components/plantillas/plantilla-sdk-daon/plantilla-sdk-daon.component';
import { Routes, RouterModule } from '@angular/router';
import { TermsComponent } from './components/terms/terms.component';
import { CorreoVerificacionComponent } from './components/correo-verificacion/correo-verificacion.component';
import { CorreoComponent } from './components/verificacion/correo/correo.component';
import { FinalComponent } from './components/final/final.component';
import { CuentaClabeComponent } from './components/verificacion/cuenta-clabe/cuenta-clabe.component';
import { CellPhoneComponent } from './components/cell-phone/cell-phone.component';
import { PersonComponent } from './components/person/person.component';
import { VerifyIdentityComponent } from './components/id-mission/document-verification/verify-identity/verify-identity.component';
import { ErrorComponent } from './components/error/error.component';
import { InstruccionesComponent } from './components/id-mission/selfie/instrucciones/instrucciones.component';
import { PageFaceCaptureComponent } from './components/id-mission/selfie/page-face-capture/page-face-capture.component';
import { FacialVerificationComponent } from './components/id-mission/selfie/facial-verification/facial-verification.component';
import { ValidaOcrComponent } from './components/id-mission/document-verification/valida-ocr/valida-ocr.component';
import { CodeQrComponent } from './components/code-qr/code-qr.component';
import { CurpComponent } from './components/curp/curp.component';
import { DireccionComponent } from './components/direccion/direccion.component';
import { SeguroSocialComponent } from './components/seguro-social/seguro-social.component';
import { CedulaProfesionalComponent } from './components/cedula-profesional/cedula-profesional.component';
import { EnvioPinComponent } from './components/analisis-fiscal/envio-pin/envio-pin.component';
import { TerminosCondicionesComponent } from './components/analisis-fiscal/terminos-condiciones/terminos-condiciones.component';
import { AutorizacionComponent } from './components/analisis-fiscal/autorizacion/autorizacion.component';
import { EnvioPinSignatureComponent } from './components/signature/envio-pin-signature/envio-pin-signature.component';
import { TerminosCondicionesSignatureComponent } from './components/signature/terminos-condiciones/terminos-condiciones.component';
import { QrFinalComponent } from './components/qr-final/qr-final.component' 
import { DeviceConnectionComponent } from './components/device-connection/device-connection.component' 
import { GlobalInstructionComponent } from './components/global-instruction/global-instruction.component';
import { DatoAdicionalComponent } from './components/dato-adicional/dato-adicional.component';
import { ArchivoAdjuntoComponent } from './components/archivo-adjunto/archivo-adjunto.component';
import { TarjetaBancariaComponent } from './components/tarjeta-bancaria/tarjeta-bancaria.component';

/** Comprobante domicilio */
import { InstructionComprobanteComponent } from './components/comprobante-domicilio/instruction-comprobante/instruction-comprobante.component';
import { UpdateInformationDataComponent } from './components/id-mission/document-verification/update-information-data/update-information-data.component';

/** IDMISSION */
import { CaptureDocumentComponent } from './components/id-mission/document-verification/capture-document/capture-document.component';
import { CaptureInstructionComponent } from './components/id-mission/document-verification/capture-instruction/capture-instruction.component';
import { DocumentoFrontalComponent } from './components/capturaManual/documento-frontal/documento-frontal.component';
import { DocumentoReversoComponent } from './components/capturaManual/documento-reverso/documento-reverso.component';
import { CapturaDatosComponent } from './components/capturaManual/captura-datos/captura-datos.component';
import { InstruccionesManualComponent } from './components/capturaManual/instrucciones-manual/instrucciones-manual.component';
import { InstruccionesReversoManualComponent } from './components/capturaManual/instrucciones-reverso-manual/instrucciones-reverso-manual.component';
import { EleccionDocumentoManualComponent } from './components/capturaManual/eleccion-documento-manual/eleccion-documento-manual.component';
import { CfdiComponent } from './components/cfdi/cfdi.component';


const APP_ROUTES: Routes = [
  {
      path: '',
      redirectTo: '/services',
      pathMatch: 'full'
  },
  {
      path: 'services',

    children: [
        {path: 'daon/selfie', component: PageFaceCaptureComponent},
        {path: 'idmission/document/capture', component: CaptureDocumentComponent},
    ]
  },
  {
      path: 'services',
      component: PlantillaComponent,
      children: [
          {path: 'idmission/instruction', component: InstruccionesComponent},
          {path: 'daon/selfie/verification', component: FacialVerificationComponent},
          {path: 'idmission/document/identity', component: VerifyIdentityComponent},
          {path: 'idmission/document/instruction', component: CaptureInstructionComponent},
          {path: 'idmission/document/update', component: UpdateInformationDataComponent},
          {path: 'idmission/document/ocr', component: ValidaOcrComponent},
          {path: 'cuenta-clabe', component: CuentaClabeComponent},
          {path: 'cell-phone', component: CellPhoneComponent},
          {path: 'cfdi', component: CfdiComponent},
          {path: 'correo', component: CorreoVerificacionComponent},
          {path: 'correo/code', component: CorreoComponent},
          {path: 'terminos/:id', component: TermsComponent},
          {path: 'person', component: PersonComponent},
          {path: 'code-qr', component: CodeQrComponent},
          {path: 'curp', component: CurpComponent},
          {path: 'direccion', component: DireccionComponent},
          {path: 'datos-adicionales', component: DatoAdicionalComponent},
          {path: 'archivos-adjuntos', component: ArchivoAdjuntoComponent},
          {path: 'seguro-social', component: SeguroSocialComponent},
          {path: 'cedula-profesional', component: CedulaProfesionalComponent},
          {path: 'analisis-financiero/envio-pin', component: EnvioPinComponent},
          {path: 'analisis-financiero/terminos', component: TerminosCondicionesComponent},
          {path: 'analisis-financiero/autorizacion', component: AutorizacionComponent},
          {path: 'signature/envio-pin', component: EnvioPinSignatureComponent},
          {path: 'signature/terminos', component: TerminosCondicionesSignatureComponent},
          {path: 'comprobante-domicilio/instrucciones', component: InstructionComprobanteComponent},
          {path: 'tarjeta-bancaria', component: TarjetaBancariaComponent},
          {path: 'qr-final', component: QrFinalComponent},
          {path: 'device-connection', component: DeviceConnectionComponent},
          {path: 'global-instruction/:id', component: GlobalInstructionComponent},
          {path: 'captura-manual/eleccion-documento', component: EleccionDocumentoManualComponent},
          {path: 'captura-manual/instrucciones', component: InstruccionesManualComponent},
          {path: 'captura-manual/instrucciones-reverso', component: InstruccionesReversoManualComponent},
          {path: 'captura-manual/frontal', component: DocumentoFrontalComponent},
          {path: 'captura-manual/reverso', component: DocumentoReversoComponent},
          {path: 'captura-manual/captura-datos', component: CapturaDatosComponent},
          {path: 'error', component: ErrorComponent},
          {path: 'final', component: FinalComponent},
          {path: '**', component: ErrorComponent},

      ],

  },
];

const routes: Routes = [
  {
      path: '',
      children: [
          ...APP_ROUTES,
          {
              path: '',
              component: TermsComponent
          }
      ]
  },
];

export const appRoutingProviders: any[] = [];
export const APP_ROUTING = RouterModule.forRoot(routes, {useHash: true});
// export const APP_ROUTING = RouterModule.forRoot(routes);

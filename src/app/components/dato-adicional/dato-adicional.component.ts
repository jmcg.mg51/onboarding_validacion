import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { VirtualTimeScheduler } from 'rxjs';
import { each } from 'jquery';

@Component({
  selector: 'app-dato-adicional',
  templateUrl: './dato-adicional.component.html',
  styleUrls: ['./dato-adicional.component.css']
})
export class DatoAdicionalComponent implements OnInit {

    //Mark: variables of components
   // razonPatron;
   // errorGeneral: boolean;
   // asalarido: boolean;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }

  errorGeneral: boolean;
  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;

  //MARK - Variables
  intentsDato;
  datos;
  showDato1:boolean;
  showDato2:boolean;
  showDato3:boolean;
  showDato4:boolean;
  dato1Value:string;
  dato2Value:string;
  dato3Value:string;
  dato4Value:string;


  async ngOnInit() {
    this.spinner.show;
  
    if (!(await this.alredySessionExist())) { return; }
    this.getDatos();
    this.id = this.object._id;
    if(sessionStorage.getItem('intentsDatoAdicional')){
      this.intentsDato = sessionStorage.getItem('intentsDatoAdicional');
    }else{
      sessionStorage.setItem('intentsDatoAdicional', '0') 
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("datosAdicionales", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsDato = sessionStorage.getItem('intentsDatoAdicional');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
        
    if(this.object.servicios['datosAdicionales']['propiedades'][0]){
      document.getElementById(this.object.servicios['datosAdicionales']['propiedades'][0]).focus();
    }else{
      document.getElementById('validarBoton').focus();
    }
    
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    console.log("el objeto", this.object);
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.datosAdicionales===true){
        this.router.navigate([this.workFlowService.redirectFlow('datosAdicionales',this.object.servicios)]); 
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsDato =  parseInt(this.intentsDato) + 1;
    sessionStorage.setItem('intentsDatoAdicional', this.intentsDato);
    
    var datos = this.object.servicios['datosAdicionales']['propiedades'];
    const requestToSend = {};
    var showMessage;
    $.each(datos, function (i, item) {
      console.log("el value es:", item);
      var datoValue = (document.getElementById(item) as HTMLInputElement).value;
      console.log("el datoValue es:", datoValue);

       if(datoValue === ""){
         showMessage = true; 
         return
       }else{
          var inputData = datoValue;
          var headData = item;
          requestToSend[headData] = inputData;  
       }
       
    });

    if(showMessage === true){
        this.errorGeneral = true;
        this.spinner.hide();
    }else{
      console.log(requestToSend);
      this.errorGeneral  = false;
      const datosAdicionales = requestToSend;
      await this.saveDatosAdicionales(datosAdicionales);
      this.spinner.hide();
    }


  }



  async saveDatosAdicionales(datosAdicionales){
    
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("datosAdicionales", device, this.intentsDato, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
    await this.middle.updateDataUser({datosAdicionales}, this.id);

    const object = this.session.getObjectSession();
    object.datosAdicionales = true;
    this.session.updateModel(object);

    this.continue();

    
  }

  getDatos(){
    console.log(this.object.servicios);
    //this.object = this.session.getObjectSession();
    if(this.object.servicios['datosAdicionales']['propiedades']){
    this.datos = this.object.servicios['datosAdicionales']['propiedades'];
    console.log(this.datos);
      
    }
    
  }


  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('datosAdicionales',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

}
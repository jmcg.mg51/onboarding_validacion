import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { info } from 'console';
import { Rutas } from 'src/app/model/RutasUtil';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';

declare const deviceInfo: any;
@Component({
  selector: 'app-device-connection',
  templateUrl: './device-connection.component.html',
  styleUrls: ['./device-connection.component.css']
})


export class DeviceConnectionComponent implements OnInit {

  constructor( private session: SessionService, private spinner: NgxSpinnerService,  public router: Router) { }

  error: string;
  copiar:string = '';
  url;
  object: sesionModel;
  async ngOnInit() {
    this.spinner.show

    this.url=sessionStorage.getItem('urlTerms');
    this.spinner.hide();
  }

copyLink(){
  var aux = document.createElement("input");
  aux.setAttribute("value", this.url);
  document.body.appendChild(aux);
  console.log(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  this.copiar="Copiado!"; 
  setTimeout(() => {
    this.copiar=" ";
  }, 2000);
}
}
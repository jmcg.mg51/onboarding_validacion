//var token = "66246A3F8B88643DAD417192F6295AE864E80B784162EC1904D813D94DC64FE5.ZGVTBZAWMVSXMC4WLJAUMJM4XQ";


function initializeSdk(token, serviceId, idType, serviceName) {
    var serviceParamsJson = {
        'serviceName' : serviceName,
        // 'serviceName' : "Validate-id-match-face",
        'enableswagger' : 'Y',
        'Enabled GEO Location': 'N',
        "tokenId" : token,  
        "baseUrl" : "https://kyc.idmission.com/apps/",
//        "serviceId" : serviceId,
        "idType" : idType,
        "idCountry" : "MEX",    
        "idState" : "",
        "productId" : "920",
        "merchantId" : "39258",
        "requestId" : "Test123"
    };
    WebSDK.setServices(serviceParamsJson);
}    

function captureIDsdk(token, idType, configSDK, callback, isMobile) {
    console.log("CONFIG", configSDK);
    var configuration = {
        "CanvasColor" : "FFFFFF",      
        "TokenId" : token,  
        "RequestId" : "Test123",
        "BaseUrl" : "https://kyc.idmission.com/apps/",
        "IdOutlineColor" : configSDK.colorButton,
        "DetectedIdOutlineColor" : "3FB900",
        "IdCaptureType" : idType,
        "RetryButtonColor" : configSDK.colorButton,
        "ConfirmButtonColor" : configSDK.colorButton,
        "ContinueButtonColor" :configSDK.colorButton,
        "ContinueButtonTextColor" : configSDK.colorLabel,
        "HeaderTextLabelMessage" : "Capturando documento",
        "InitialMessageForId" : "Por favor espera",
        "MoveCloserInstruction" : "Acércalo",
        "MoveBackInstuction" : "Aléjalo",
        "IdNotFoundInstruction" : "Documento no encontrado",
        "AlignIdInsideRectangle" : "Mantelo firme y alineado.",
        "InvalidFocusMessage" : "Desenfocado",
        "IdCapturePreviewHeader" : "Imagen captada",
        "IdCapturePreviewMessage" : "Verifica que puedes leer claramente los datos del documento.",
        "IdMinFocus" : "30",
        "IdMaxFocus" : "85",
        "SaturationValue" : "5",
        "EnableDebugMode" : "N",
        "EnableIDSpoofingAPI" : "N",
        "EnableInstructionScreen" : "Y",
        "InstructionScreenIDMessage" : "Ajusta tu documento dentro del recuadro.",
        "InstructionScreenIDContinueMessage" : "Aceptar",
        'EnableInstructionScreen' : 'Y'
        }
        if(!isMobile) {
            configuration['EnableManualCapture'] = 'N';
			configuration['AllowIDDesktopUpload'] = 'Y';
			configuration['SupportStockCamera'] = 'N';
			configuration['DisableAutoCapture'] = 'N';
			configuration['SkipIDpreview'] = 'N';
        }
        console.log("CONFIG", configuration);
    WebSDK.initAutoIDDocCapture(configuration, callback, "IDFront", false);
}

function captureIDBacksdk(token, serviceId, idType, configSDK, callback, isMobile) {
    var configuration = {
        "CanvasColor" : "FFFFFF",      
        "TokenId" : token,  
        "RequestId" : "Test123",
        "BaseUrl" : "https://kyc.idmission.com/apps/",
        "IdOutlineColor" : configSDK.colorButton,
        "DetectedIdOutlineColor" : "3FB900",
        "IdCaptureType" : idType,
        "RetryButtonColor" : configSDK.colorButton,
        "ConfirmButtonColor" : configSDK.colorButton,
        "ContinueButtonColor" :configSDK.colorButton,
        "ContinueButtonTextColor" : configSDK.colorLabel,
        "HeaderTextLabelMessage" : "Capturando documento",
        "InitialMessageForId" : "Por favor espera",
        "MoveCloserInstruction" : "Acércalo",
        "MoveBackInstuction" : "Aléjalo",
        "IdNotFoundInstruction" : "Documento no encontrado",
        "AlignIdInsideRectangle" : "Mantelo firme y alineado.",
        "InvalidFocusMessage" : "Desenfocado",
        "IdCapturePreviewHeader" : "Imagen captada",
        "IdCapturePreviewMessage" : "Verifica que puedes leer claramente los datos del documento.",
        "IdMinFocus" : "30",
        "IdMaxFocus" : "85",
        "SaturationValue" : "5",
        "EnableDebugMode" : "N",
        "EnableIDSpoofingAPI" : "N",
        "EnableInstructionScreen" : "Y",
        "InstructionScreenIDMessage" : "Ajusta tu documento dentro del recuadro.",
        "InstructionScreenIDContinueMessage" : "Aceptar"
        }
        if(!isMobile) {
            configuration['EnableManualCapture'] = 'N';
			configuration['AllowIDDesktopUpload'] = 'Y';
			configuration['SupportStockCamera'] = 'N';
			configuration['DisableAutoCapture'] = 'N';
			configuration['SkipIDpreview'] = 'N';
        }
    WebSDK.initAutoIDDocCapture(configuration, callback, "IDBack", false);
    //submitSdk(token, serviceId, idType, callback);
}

function getIDDocResponseCallback(response){
	if(response && response.Status == "SUCCESS"){
		if(response.IDDetected){
			if(response.IDDetected == "Y"){
				if(response.ImageType == "IDFront"){
					var idImageType = "IDFront";
                    console.log(idImageType+ ": " +response.ImageData);
                    sessionStorage.setItem("idFront", response.ImageData);
                    $("#captureGaleriaFront").css("display", "none");
                    $("#captureGaleriaBack").css("display", "block");

				}else if(response.ImageType == "IDBack"){
                    console.log(idImageType+ ": " +response.ImageData)
                    sessionStorage.setItem("idBack", response.ImageData);
                    //$("#captureGaleriaFront").css("display", "none");
                   // submitSdk(token, serviceId, idType, callback);
				}
			}else{
				alert(response.StatusMessage);
			}
		}else{
			if(response.ImageType == "IDFront"){
				var idImageType = "IDFront";
                console.log(idImageType+ ": " +response.ImageData)
                sessionStorage.setItem("idFront", response.ImageData);
                $("#captureGaleriaFront").css("display", "none");
                $("#captureGaleriaBack").css("display", "block");
			}else if(response.ImageType == "IDBack"){
				var idImageType = "IDBack";
                console.log(idImageType+ ": " +response.ImageData);
                sessionStorage.setItem("idBack", response.ImageData);
			}
		}
	}else{
        alert(response.StatusMessage);
	}
}

function detectFaceSdk(token, configSDK, callback) {
    console.log("colores", configSDK.colorButton, configSDK.colorButton );
       var configuration = {
        "FaceImageType" : "OVAL_FACE",
        "DetectedFaceOutlineColor" : "3FB900",
        "RetryButtonColor" : configSDK.colorButton,
        "ConfirmButtonColor" : configSDK.colorButton,
        "ContinueButtonColor" :configSDK.colorButton,
        "ContinueButtonTextColor" : configSDK.colorLabel,
        "InitialMessage" : "Mira directamente a la cámara",
        "FacemaskMessage" : "Cubrebocas detectado",
        "EyeCoveringMessage" : "Lentes detectado",
        "CellPhoneMessage" : "Celular encontrado",
        "HeadcoveringMessage" : "Gorro detectado",
        "FaceNotFoundMessage" : "Rostro no encontrado",
        "MoveCenterMessage" : "Muevase al centro",
        "FaceDetectedMessage" : "Imagen captada",
        "MoveBackMessage":"Retrocede un poco",
        'AllowedTotalLiveFaceRequest': '1',
        "TitleFont" : "Raleway",
        'LiveFaceNotDetectedMsg' : 'Intenta nuevamente',
        "SelfiePreviewMessageFound":"Verifica que tus ojos estén abiertos y no haya luz excesiva en el entorno",
        "TokenId" : token,  
        "RequestId" : "Test123",
        "BaseUrl" : "https://kyc.idmission.com/apps/",
        "SendDataToPFDModel" : "N",
        "EnableInstructionScreenFace" : "Y",
        "InstructionScreenFaceMessage" : "Sin lentes ni gorras, ni iluminación excesiva.",
        "InstructionScreenFaceContinueMessage" : "Aceptar"
        }
        console.log("la configuración de face es", configuration);
        WebSDK.initRealFace(configuration, callback, false);
        //$('#continue-FaceCaptureInstruction').attr('style', 'background-color: red !important');
}

function getFaceResponseCallback(response){
    
	if(response && response.Status == "SUCCESS"){
		if(response.ImageType == "Face"){
			if(response.FaceDetected && response.FaceDetected == "Y"){
				liveFaceDetected = "Y";
                console.log("liveFaceDetected " + liveFaceDetected + ": " +response.ImageData);
                sessionStorage.setItem("imageSelfie", response.ImageData);
			}else{
				liveFaceDetected = "N";
                //alert(response.StatusMessage);
			}
		}
	}else{
        alert(response.StatusMessage);
	}
}

function submitSdk(token, serviceId, idType, trackId, email, callback, serviceName) {
    var configuration = {
        // "baseUrl" : "https://kyc.idmission.com/apps/",
        // "tokenId" : token,
        // // 'serviceName' : serviceName,
        // 'serviceName' : "Validate-id-match-face",
        // "merchantId" : "39258",
        // "productId" : "920",
        // "requestId" : "Test123",
        // "serviceId" : serviceId,
        // "idType" : idType,
        // "idCountry" : "MEX",
        // "idState" : null,
        // // "CustomModelPathPrefix": "assets/js/",
        // // "CustomImagePathPrefix": "assets/js/images",
        "uniqueCustomerNumber": trackId,
        // "customerName":"",
        // "customerPhone":"",
        // "customerEmail": email,
        // "liveFaceDetected":"",
        // "employeeCode":"",
        // "employeeName":"",
        // "employeeType":"",
        // "clearFormKey": "Y",
        // "needImmediateResponse": "N",
        // "requestFromWeb": "Y"
        'manualReviewRequired' : 'N',

        'bypassAgeValidation' : 'N',

        'postDataAPIRequired' : 'N',

        'sendInputImagesInPost' : 'N',

        'dedupSynchronous' : 'N',

        'idBackImageRequired' : 'N',

        'dedupManualReviewRequired' : 'N',

        'dedupRequired' : 'N',

        'bypassNameMathing' : 'N',

        'needImmediateResponse' : 'N',//We have a quick response for a form goes in Async manner set 'Y', Process Async call (Immediate Response)

        'sendProcessedImagesInPost' : 'N',

        'verifyDataWithHost' : 'N',

        'stripSpecialCharacters' : 'N',

        'SendDataToPFDModel' : 'N'

    };
        // if(serviceId == 50) {
        //     configuration['dedupRequired'] = 'Y';
        //     configuration['dedupSynchronous'] = 'Y';
        //     configuration['dedupManualReviewRequired'] = 'Y';
        // }
    
    console.log("la configuración final es:", configuration);
    try {
        WebSDK.submitData(configuration, callback);
    } catch (error) {
        console.error('e' , error);
    }

}    

function getSubmitResponseCallback(response){
	if(response && response.Status == "FAIL"){
        alert(response.StatusMessage);
	}else{
        console.log("Status: " + JSON.stringify(response.Status));
        console.log("FormDetails: " + response.FormDetails);
        //xml2json(response.FormDetails);
        //xml2json(JSON.stringify(response.FormDetails));
        sessionStorage.setItem("OCR", response.FormDetails);
        
	}

   // return response.FormDetails;
}

function getResponseFace(response){
    console.log(response);
    return response;
}

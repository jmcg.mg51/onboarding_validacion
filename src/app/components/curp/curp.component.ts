import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-curp',
  templateUrl: './curp.component.html',
  styleUrls: ['./curp.component.css']
})
export class CurpComponent implements OnInit {

    //Mark: variables of components
    curpInput: string;
    errorCurp: boolean;


  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService,  private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsCurp;

  async ngOnInit() {

    this.ti=sessionStorage.getItem('ti');
    this.curpInput =sessionStorage.getItem('curp');
    console.log("this.curpInput = ", this.curpInput);
    this.spinner.show;
    
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    if(sessionStorage.getItem('intentsCurp')){
      this.intentsCurp = sessionStorage.getItem('intentsCurp');
    }else{
      sessionStorage.setItem('intentsCurp', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("curp", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsCurp = sessionStorage.getItem('intentsCurp'); 
    }
    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
    document.getElementById('curpInput').focus();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.identity===true){
        if(this.object.servicios['direccion']){
          this.router.navigate([Rutas.direccion]);
        } else {
          this.router.navigate([Rutas.ocrValidation]);
        }
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.curpInput = $("#curpInput").val().toString();
    this.curpInput = this.curpInput.toUpperCase();
    this.CurpValidate(this.curpInput);
  }

  async CurpValidate(curp){
    
    var regexCurp = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;

    if(curp.match(regexCurp)){
        console.log("validado");
        this.saveCurp(curp);
        this.errorCurp = false;
    }else{
      this.intentsCurp =  parseInt(this.intentsCurp) + 1;
      sessionStorage.setItem('intentsCurp', this.intentsCurp);
        console.log("No validado");
        this.errorCurp = true;
    }
  }

  async saveCurp(curp){
    //Metodo para guardar el curp
    const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("curp", device, this.intentsCurp, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
    let result = await this.middle.updateDataUser({ curp: curp }, this.id); 
    this.object.curp = true;
    this.session.updateModel(this.object);
    sessionStorage.setItem('ti', this.ti);
    sessionStorage.setItem('curp', curp);
    console.log("this.curp = ", curp);
    this.continue();
    
  }

  async continue(){
    if(this.object.servicios['direccion']){
      this.router.navigate([Rutas.direccion]);
    } else {
      this.router.navigate([Rutas.ocrValidation]);
    }
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
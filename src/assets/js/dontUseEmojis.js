function isEmoji(str) {
    var ranges = [       
       '[\uE000-\uF8FF]',
       '\uD83C[\uDC00-\uDFFF]',
       '\uD83D[\uDC00-\uDFFF]',
       '[\u2011-\u26FF]',
       '\uD83E[\uDD10-\uDDFF]'         
    ];
    if (str.match(ranges.join('|'))) {
        return true;
    } else {
        return false;
    }
}

$(document).ready(function(){
    $('input').on('input',function(){
       var $th = $(this);
       //console.log("Value of Input"+$th.val());
       emojiInput= isEmoji($th.val());
       if (emojiInput==true) {
           $th.val("");
       }
   });
   });
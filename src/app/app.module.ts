import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ModalModule} from 'ng2-modal';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import { AppComponent } from './app.component';
import { PlantillaComponent } from './components/plantilla.component';
import { APP_ROUTING } from './app.routes';
import {NgxImageCompressService} from 'ngx-image-compress';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TermsComponent } from './components/terms/terms.component';
import { CorreoVerificacionComponent } from './components/correo-verificacion/correo-verificacion.component';
import { FinalComponent } from './components/final/final.component';
import { VerifyIdentityComponent } from './components/id-mission/document-verification/verify-identity/verify-identity.component';
import { ErrorComponent } from './components/error/error.component';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InstruccionesComponent } from './components/id-mission/selfie/instrucciones/instrucciones.component';
import { PageFaceCaptureComponent } from './components/id-mission/selfie/page-face-capture/page-face-capture.component';
import { FacialVerificationComponent } from './components/id-mission/selfie/facial-verification/facial-verification.component';
import { ValidaOcrComponent } from './components/id-mission/document-verification/valida-ocr/valida-ocr.component';
import { CorreoComponent } from './components/verificacion/correo/correo.component';
import { FooterComponent } from './components/compartidos/footer/footer.component';
import { HeaderComponent } from './components/compartidos/header/header.component';
import { PlantillaSdkDaonComponent } from './components/plantillas/plantilla-sdk-daon/plantilla-sdk-daon.component';
import { CuentaClabeComponent } from './components/verificacion/cuenta-clabe/cuenta-clabe.component';
import { PersonComponent } from './components/person/person.component';
import { CellPhoneComponent } from './components/cell-phone/cell-phone.component';
import { GlobalInstructionComponent } from './components/global-instruction/global-instruction.component';
import { CodeQrComponent } from './components/code-qr/code-qr.component';
import { CurpComponent } from './components/curp/curp.component';
import { DireccionComponent } from './components/direccion/direccion.component';
import { CedulaProfesionalComponent } from './components/cedula-profesional/cedula-profesional.component';
import { SeguroSocialComponent } from './components/seguro-social/seguro-social.component';
import { EnvioPinComponent } from './components/analisis-fiscal/envio-pin/envio-pin.component';
import { TerminosCondicionesComponent } from './components/analisis-fiscal/terminos-condiciones/terminos-condiciones.component';
import { AutorizacionComponent } from './components/analisis-fiscal/autorizacion/autorizacion.component';
import {QrFinalComponent} from './components/qr-final/qr-final.component';
import {DeviceConnectionComponent} from './components/device-connection/device-connection.component';
import { LottieAnimationViewModule } from 'ng-lottie';

import { EnvioPinSignatureComponent } from './components/signature/envio-pin-signature/envio-pin-signature.component';
import { TerminosCondicionesSignatureComponent } from './components/signature/terminos-condiciones/terminos-condiciones.component';
// import {  NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import {  NgbModal } from '../../node_modules/@ng-bootstrap/ng-bootstrap';
import { ServicesGeneralService, isMobile, isIphone } from './services/general/services-general.service';
import { SessionService } from './services/session/session.service';
import { WorkFlowService } from './services/session/work-flow.service';
import { MiddleDaonService } from './services/http/middle-daon.service';
import { MiddleMongoService } from './services/http/middle-mongo.service';
import { MiddleVerificacionService } from './services/http/middle-verificacion.service';
import { ShareFaceService } from './services/share/share-face.service';
import { PruebaService } from './services/share/prueba.service';
import { ErrorVidaService } from './services/errores/error-vida.service';
import { ErrorSelfieService } from './services/errores/error-selfie.service';
import { DatoAdicionalComponent } from './components/dato-adicional/dato-adicional.component';
import { ArchivoAdjuntoComponent } from './components/archivo-adjunto/archivo-adjunto.component';
import { TarjetaBancariaComponent } from './components/tarjeta-bancaria/tarjeta-bancaria.component';

/** Comprobante domicilio */
import { InstructionComprobanteComponent } from './components/comprobante-domicilio/instruction-comprobante/instruction-comprobante.component';
import { UpdateInformationDataComponent } from './components/id-mission/document-verification/update-information-data/update-information-data.component';
import { CaptureDocumentComponent } from './components/id-mission/document-verification/capture-document/capture-document.component';
import { CaptureInstructionComponent } from './components/id-mission/document-verification/capture-instruction/capture-instruction.component';
import { DocumentoFrontalComponent } from './components/capturaManual/documento-frontal/documento-frontal.component';
import { DocumentoReversoComponent } from './components/capturaManual/documento-reverso/documento-reverso.component';
import { CapturaDatosComponent } from './components/capturaManual/captura-datos/captura-datos.component';
import { InstruccionesManualComponent } from './components/capturaManual/instrucciones-manual/instrucciones-manual.component';
import { InstruccionesReversoManualComponent } from './components/capturaManual/instrucciones-reverso-manual/instrucciones-reverso-manual.component';
import { EleccionDocumentoManualComponent } from './components/capturaManual/eleccion-documento-manual/eleccion-documento-manual.component';
import { ValidateDeviceComponent } from './components/util/validate-device/validate-device.component';
import { CfdiComponent } from './components/cfdi/cfdi.component';


@NgModule({
  declarations: [
    AppComponent,
    PlantillaComponent,
    TermsComponent,
    CorreoVerificacionComponent,
    InstruccionesComponent,
    FinalComponent,
    PageFaceCaptureComponent,
    FacialVerificationComponent,
    VerifyIdentityComponent,
    CaptureInstructionComponent,
    CaptureDocumentComponent,
    ErrorComponent,
    ValidaOcrComponent,
    CorreoComponent,
    FooterComponent,
    HeaderComponent,
    PlantillaSdkDaonComponent,
    CuentaClabeComponent,
    PersonComponent,
    CellPhoneComponent,
    GlobalInstructionComponent,
    CurpComponent,
    DireccionComponent,
    CodeQrComponent,
    QrFinalComponent,
    DeviceConnectionComponent,
    SeguroSocialComponent,
    CedulaProfesionalComponent,
    TerminosCondicionesComponent,
    AutorizacionComponent,
    DatoAdicionalComponent,
    EnvioPinComponent,
    InstructionComprobanteComponent,
    UpdateInformationDataComponent,
    EnvioPinSignatureComponent,
    TerminosCondicionesSignatureComponent,
    ArchivoAdjuntoComponent,
    TarjetaBancariaComponent,
    DocumentoFrontalComponent,
    DocumentoReversoComponent,
    CapturaDatosComponent,
    InstruccionesManualComponent,
    InstruccionesReversoManualComponent,
    EleccionDocumentoManualComponent,
    ValidateDeviceComponent,
    CfdiComponent,
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    APP_ROUTING,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    QRCodeModule,
    LottieAnimationViewModule.forRoot(),
    
  ],
  bootstrap: [AppComponent],
  providers: [NgxImageCompressService,ServicesGeneralService,NgxSpinnerService,
    SessionService,WorkFlowService,MiddleDaonService,MiddleMongoService,MiddleVerificacionService,
    ShareFaceService,PruebaService,ErrorVidaService,ErrorSelfieService, TermsComponent, GlobalInstructionComponent
  ]
})
export class AppModule { }

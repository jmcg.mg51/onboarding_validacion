export class sesionModel {
  _id: string; // este es el de mongo

  daon: {
    daonClientHref: string;
    daonHref: string;  // este me lo regresa daon
    selfie: boolean;
    identity: boolean;
    pruebaVida: boolean;
  };
  callback: string;
  estatus: string;
  oferta: string;
  correo: boolean;
  terminos: boolean;
  score: any = null;
  cuentaClabe: boolean;
  curp: boolean;
  direccion: boolean;
  telefono: boolean;
  cedulaProfesional: boolean;
  envioPin: boolean;
  ficoScore: boolean;
  reporteCrediticio: boolean;
  envioPinSignature: boolean;
  nip: boolean;
  seguroSocial: boolean;
  datosFiscales: boolean;
  datosAdicionales: boolean;
  archivosAdjuntos: boolean;
  emailVerified: boolean;
  comprobanteDeDomicilio: boolean;
  tarjetaBancaria: boolean; 
  servicios: Object;
  tipoPersona: Object;
}

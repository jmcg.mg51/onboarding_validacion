import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateDeviceComponent } from './validate-device.component';

describe('ValidateDeviceComponent', () => {
  let component: ValidateDeviceComponent;
  let fixture: ComponentFixture<ValidateDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

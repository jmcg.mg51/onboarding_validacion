import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-tarjeta-bancaria',
  templateUrl: './tarjeta-bancaria.component.html',
  styleUrls: ['./tarjeta-bancaria.component.css']
})
export class TarjetaBancariaComponent implements OnInit {

    //Mark: variables of components
    tarjeta;
    BIN;
    MCC;
    vigencia;
    CVV;
    cedulaProfesionalInput;
    errorGeneral: boolean;
    chosenYearDate: Date;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsTarjetaBancaria;
  yearsCard = [];
  VISA =  /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/;
  MASTERCARD = /^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/;
  AMEX = /^3[47][0-9-]{16}$/;
  error; 
  month;
  year; 
  currentDate = new Date();
  currentYear = this.currentDate.getFullYear();
  currentMonth = this.currentDate.getMonth();

 // /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/  //Visa
  ///^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/  //Mastercard

  async ngOnInit() {
    console.log("Tarjeta");
    this.spinner.show;
    this.setYearsCard();

    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;

    if(sessionStorage.getItem('intentsTarjetaBancaria')){
      this.intentsTarjetaBancaria = sessionStorage.getItem('intentsTarjetaBancaria');
    }else{
      sessionStorage.setItem('intentsTarjetaBancaria', '0');
      console.log("El valor de intents es:",sessionStorage.getItem('intentsTarjetaBancaria'));
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("tarjetaBancaria", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsTarjetaBancaria = sessionStorage.getItem('intentsTarjetaBancaria');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
   // document.getElementById('carreraName').focus();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.tarjetaBancaria===true){
        this.router.navigate([this.workFlowService.redirectFlow('tarjetaBancaria',this.object.servicios)]);   
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsTarjetaBancaria =  parseInt(this.intentsTarjetaBancaria) + 1;
    sessionStorage.setItem('intentsTarjetaBancaria', this.intentsTarjetaBancaria);
    let tarjetaInput = document.getElementById("tarjetaInput") as HTMLInputElement;
    this.tarjeta = tarjetaInput.value; 
    //this.MCC = 
    //this.vigencia;
    this.CVV =  (document.getElementById("cvv") as HTMLInputElement).value;//cvv
    this.month =  (document.getElementById("month") as HTMLInputElement).value;
    this.year =  (document.getElementById("year") as HTMLInputElement).value;

    if(this.tarjeta === "" || this.CVV == "" || this.month == "00" || this.year == "00"){
      this.error = "todos los campos son obligatorios"
      this.errorGeneral = true; 
      this.spinner.hide();
    }else{
      if(this.tarjeta.match(this.VISA) || this.tarjeta.match(this.MASTERCARD) || this.tarjeta.match(this.AMEX)){
        let validateMonth = String(this.currentDate.getMonth() + 1).padStart(2, '0');
        if(this.year == this.currentYear && this.month < validateMonth){
          this.error = "Tu tarjeta se encuentra vencida"
          this.errorGeneral = true; 
          this.spinner.hide();
          return;
        }
        this.errorGeneral = false; 
        this.BIN = tarjetaInput.value.slice(0, 4);
        this.vigencia = this.month+"/"+this.year;
        this.MCC = tarjetaInput.value.slice(Math.max(tarjetaInput.value.length - 4, 1))
        const tarjetaBancaria = {
          mcc: this.MCC,
          bin: this.BIN,
          vigencia: this.vigencia
         // cvv: this.CVV

        }
  
        console.log(tarjetaBancaria);
        await this.saveTarjeta(tarjetaBancaria);
        this.spinner.hide();
      }else{
        this.error = "Inserte una tarjeta valida";
        this.errorGeneral = true; 
        this.spinner.hide();
      }
      
    }

  }



  async saveTarjeta(tarjeta){
    
    sessionStorage.setItem('tarjetaBancaria', "si");
    //sessionStorage.setItem('patron', this.razonPatron);
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("tarjetaBancaria", device, this.intentsTarjetaBancaria, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
    await this.middle.updateDataUser({tarjeta}, this.id);

    const object = this.session.getObjectSession();
    object.tarjetaBancaria = true;
    this.session.updateModel(object);

    this.continue();

    
  }

  async setYearsCard(){
    let date = new Date();
    let year = date.getFullYear();
    let finalYear = year + 15; 
    while ( year <= finalYear ) {
      this.yearsCard.push(year++);
    } 

    console.log("el rango de años es", this.yearsCard);
  }

  async validateCard(numberCard){
    let regex = /^(\d\s?){15,16}$/
    let result = regex.test(numberCard);

    return result; 
  }


  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('tarjetaBancaria',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
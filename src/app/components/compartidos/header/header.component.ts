import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { TermsComponent } from '../../terms/terms.component';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';
import { timeout } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {

  rutaAWS: string;
  apiToken: string;
  oferta: string;
  merchantCode: string;
  id:string;
  href:string;
  constructor(private location: Location, private middle: MiddleMongoService, private term: TermsComponent,
    private actRoute: ActivatedRoute, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    this.href= window.location.href;
    if(this.href.includes('global-instruction')){
      let n = this.href.indexOf('global-instruction') + 19;
      this.id = this.href.substring(n,this.href.length)
      if(this.id.indexOf('?') !== -1){
        this.id = this.id.substring(0,this.id.indexOf('?'));
        console.log("id_1", this.id);
      }else{
        console.log("id_2", this.id);
      }
    }

    var merchanApiToken: string;
    var merchanOferta: string;

    this.rutaAWS = sessionStorage.getItem('rutaDelIconoAWS');
    console.log(this.rutaAWS);
    if (this.rutaAWS) {
      console.log('ya tengo un valor para el header');
      return;
    }
    console.log('no tengo un valor para el header');
    let ruta = sessionStorage.getItem('urlIcon');


    setTimeout(async () => {
      await this.spinner.show();
      this.merchantCode = sessionStorage.getItem('merchantCode');
       console.log("this.merchantCode",this.merchantCode);
      if(this.merchantCode){ 
        const values = atob(this.merchantCode).split(':');
        if (values.length > 1) {
          merchanApiToken = values[0];
          merchanOferta = values[1];
          //nuevo código
          console.log("Los valores del merchant son:", merchanApiToken, merchanOferta);
          sessionStorage.setItem("apiId", merchanApiToken);
          sessionStorage.setItem("apiOferta", merchanOferta);
        }
      }else{
        var dataUserResponse = await this.middle.getDataUser(this.id);
        console.log("dataUserResponse",JSON.stringify(dataUserResponse));
        if(dataUserResponse !== undefined) {
          merchanApiToken = dataUserResponse["merchantId"];
          merchanOferta = dataUserResponse["nombreOferta"];
        }
      }

      const infoOffer = await this.middle.getInfoOferta(merchanOferta, merchanApiToken);
      const imageURL = infoOffer["urlIcon"];
      sessionStorage.setItem('urlIcon', imageURL);


      this.rutaAWS = imageURL + '?temp=' + new Date().getTime();
      sessionStorage.setItem('rutaDelIconoAWS', this.rutaAWS);
      await this.spinner.hide();
    }, .001);

  }

  public getValueOffers(){
    if(sessionStorage.getItem('propiedadesOffer')){
      return true;
    }else{
      return false; 
    }
  }

}

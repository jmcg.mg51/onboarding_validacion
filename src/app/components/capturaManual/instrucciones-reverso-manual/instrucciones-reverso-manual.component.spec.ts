import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstruccionesReversoManualComponent } from './instrucciones-reverso-manual.component';

describe('InstruccionesReversoManualComponent', () => {
  let component: InstruccionesReversoManualComponent;
  let fixture: ComponentFixture<InstruccionesReversoManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstruccionesReversoManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstruccionesReversoManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

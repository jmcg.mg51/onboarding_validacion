import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  anio: number;
  constructor() {
    this.anio = new Date().getFullYear();
  }

  async ngOnInit() {
    // await this.delay(2000);
    this.loadScripts();

  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async loadScripts() {

    console.log('voy a cargar el boot');
    const dynamicScripts = [
      'https://widget.botlers.io/sdk/main.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      let bot = sessionStorage.getItem('bot');
      console.log('ya hice la valdiacion : ', bot);
      if (bot && bot != null && bot === 'true') {
        console.log('Estoy cargando el boot');
        const node = document.createElement('script');
        node.src = dynamicScripts[i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        node.setAttribute('id', 'botlers-messaging-sdk-v2');
        node.setAttribute('bmid', '5ec0a3e39b8c4905a717142500e01ff6');
        node.setAttribute('button', 'true');
        node.setAttribute('skip-start', 'false');
        node.setAttribute('bm-params', "%7B%22ui%22%3A%7B%22attachFiles%22%3Atrue%2C%22attachImages%22%3Atrue%7D%7D");
        document.getElementById('boot').appendChild(node);
      }
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service.js';
import { Router, ActivatedRoute } from '@angular/router';
import { Rutas } from 'src/app/model/RutasUtil.js';
import { FormControl, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { NgxSpinnerService } from 'ngx-spinner';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../../environments/environment';
import { sesionModel } from '../../../model/sesion/SessionPojo';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import {GlobalInstructionComponent} from '../../global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-cuenta-clabe',
  templateUrl: './cuenta-clabe.component.html',
  styleUrls: ['./cuenta-clabe.component.css']
})

export class CuentaClabeComponent implements OnInit {

  id: string;
  object: sesionModel;
  constructor(private workFlowService :WorkFlowService, private session: SessionService, public router: Router,
              private actRoute: ActivatedRoute, private middle: MiddleMongoService,
              private spinner: NgxSpinnerService,
              private global: GlobalInstructionComponent, private analyticService: AnaliticService) { }

  isOK = false;
  submitted = false;
  intentsClabe
  errorString = '';
  //myForm:any;
  myForm = new FormGroup({
    cuentaClabe: new FormControl('', [Validators.minLength(18), Validators.maxLength(18), Validators.required, Validators.pattern('[0-9]*'), this.IsValidated()])
  });

  async ngOnInit() {
    await this.spinner.show();
    $('#inputCuentaClabe').attr('maxlength', 18);
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;

    if(sessionStorage.getItem('intentsClabe')){
      this.intentsClabe = sessionStorage.getItem('intentsClabe');
    }else{
      sessionStorage.setItem('intentsClabe', '0') 
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("cuentaClabe", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsClabe = sessionStorage.getItem('intentsClabe');
    }
    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();

    document.getElementById('inputCuentaClabe').focus();
  }

  get f() {
    return this.myForm.controls;
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.cuentaClabe===true){
        console.log('si entre al final');
        this.router.navigate([this.workFlowService.redirectFlow('cuentaClabe',this.object.servicios)]);
        return false;
      } else {
        console.log('no entre al final');
        return true;
      }
    }
  }

  async onSubmit() {
    this.submitted = true;
    if (this.myForm.invalid) {
      await this.spinner.hide();
      console.log("tiene errores= ");
      return;
    }
  }

  onReset() {
    this.submitted = false;
    this.myForm.reset();
  }

  async continuar() {
    await this.spinner.show();
    this.errorString = '';
    this.intentsClabe =  parseInt(this.intentsClabe) + 1;
    sessionStorage.setItem('intentsClabe', this.intentsClabe);
    console.log('>>> : ' + this.f.cuentaClabe.value);
    if (this.myForm.valid) {
      console.log('>>> : ' + this.f.cuentaClabe.value);
      let responseCuentaClabe = await this.middle.updateDataUser({ cuentaClabe: this.f.cuentaClabe.value }, this.id);
      if(responseCuentaClabe === 'ERROR') {
        this.errorString = 'Ocurrio un error, favor de reintentar';
        await this.spinner.hide();
        return;
      }
      const object = this.session.getObjectSession();
      object.cuentaClabe = this.f.cuentaClabe.value;
      object.estatus = 'Terminado';
      this.session.updateModel(object);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("cuentaClabe", device, this.intentsClabe, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
      console.log('ya termine con la CC' + JSON.stringify(object, null, 2));
      await this.spinner.hide();
      this.router.navigate([this.workFlowService.redirectFlow('cuentaClabe',this.object.servicios)]);
    } else {
      this.errorString = 'No es un numero de cuenta válido';
    }
    await this.spinner.hide();


  }

  CLABE_LENGTH = 18;
  CLABE_WEIGHTS = [3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7];
  getDc(clabe) {
    const clabeList = clabe.split('');
    const clabeInt = clabeList.map((i) => Number(i));
    const weighted = [];
    for (let i = 0; i < this.CLABE_LENGTH - 1; i++) {
      weighted.push(clabeInt[i] * this.CLABE_WEIGHTS[i] % 10);
    }
    const summed = weighted.reduce((curr, next) => curr + next) % 10;
    const controlDigit = (10 - summed) % 10;
    return controlDigit.toString();
  }

  validaClabe(clabe) {
    return this.isANumber(clabe) &&
      clabe.length === this.CLABE_LENGTH &&
      clabe.substring(this.CLABE_LENGTH - 1) === this.getDc(clabe);
  }
  isANumber(str) {
    return !/\D/.test(str);
  }

  IsValidated(): ValidatorFn {
    return () => {
      if (this.myForm !== undefined) {
        if (!this.validaClabe(this.f.cuentaClabe.value)) {
          return { valid: true };
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}

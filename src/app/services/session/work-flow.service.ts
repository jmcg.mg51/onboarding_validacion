import { NgZone, Injectable } from '@angular/core';
import { Rutas } from 'src/app/model/RutasUtil';
import { Router, ActivatedRoute } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class WorkFlowService {

  constructor(public ngZone: NgZone, public router: Router) { }
  workFlow = {
    "flow": [
      { "link": Rutas.terminos, "name": "terminos" },
      { "link": Rutas.globalInstruction, "name": "globalInstruction" },
      { "link": Rutas.correo, "name": "correoElectronico" },
      { "link": Rutas.person, "name": "rfc" },
      { "link": Rutas.telefono, "name": "telefono" },
      { "link": Rutas.instrucciones, "name": "selfie" },
      { "link": Rutas.chooseIdentity, "name": "documento" },
      // { "link": Rutas.livenessInstruction, "name": "pruebaDeVida" },
      { "link": Rutas.eleccionDocumentoManual, "name": "documentoManual" },
      { "link": Rutas.cfdi, "name": "cfdi" },
      { "link": Rutas.comprobanteDomicilio, "name": "comprobanteDeDomicilio" },
      { "link": Rutas.cuentaClabe, "name": "cuentaClabe" },
      { "link": Rutas.seguroSocial, "name": "seguroSocial" },
      { "link": Rutas.cedulaProfesional, "name": "cedulaProfesional" },
      { "link": Rutas.datosAdicionales, "name": "datosAdicionales" },
      { "link": Rutas.archivosAdjuntos, "name": "archivosAdjuntos" },
      { "link": Rutas.envioPin, "name": "envioPin" },
      // { "link": Rutas.analisisFinancieroTerminos, "name": "ficoScore" },
      // { "link": Rutas.analisisFinancieroAutorizacion, "name": "reporteCrediticio" },
      { "link": Rutas.envioPinSignature, "name": "envioPinSignature" },
      { "link": Rutas.nip, "name": "nip" },
      { "link": Rutas.tarjetaBancaria, "name": "tarjetaBancaria" },
      { "link": Rutas.fin, "name": "fin" },
    ]
  };
  SECURE_TOUCH = 'secureTouch';
  redirectFlow(current, services) {
    var i;
    for (i = 0; i < this.workFlow.flow.length; i++) {
      // console.log("i= " + i);
      // console.log("this.workFlow.flow_i = ", this.workFlow.flow[i]);
      // console.log("current = ", current);
      // console.log("services = ", services);
      if (this.workFlow.flow[i].name === current) {
        if (i + 1 === this.workFlow.flow.length) {
          return this.workFlow.flow[i].link;
        } else {
          // console.log("services[this.workFlow.flow[i+1].name] = ", services[this.workFlow.flow[i+1].name]);
          if (services[this.workFlow.flow[i + 1].name] !== undefined) {
            // console.log("this.workFlow.flow[i+1].link = " + this.workFlow.flow[i+1].link);
            return this.workFlow.flow[i + 1].link;
            break;
          } else {
            current = this.workFlow.flow[i + 1].name;
          }
        }
      }
    }
    return Rutas.error;
  }

  redirectFlowName(current, services) {
    var i;
    for (i = 0; i < this.workFlow.flow.length; i++) {
      // console.log("i= " + i);
      // console.log("this.workFlow.flow_i = ", this.workFlow.flow[i]);
      // console.log("current = ", current);
      // console.log("services = ", services);
      if (this.workFlow.flow[i].name === current) {
        if (i + 1 === this.workFlow.flow.length) {
          return this.workFlow.flow[i].name;
        } else {
          // console.log("services[this.workFlow.flow[i+1].name] = ", services[this.workFlow.flow[i+1].name]);
          if (services[this.workFlow.flow[i + 1].name] !== undefined) {
            // console.log("this.workFlow.flow[i+1].link = " + this.workFlow.flow[i+1].link);
            return this.workFlow.flow[i + 1].name;
            break;
          } else {
            current = this.workFlow.flow[i + 1].name;
          }
        }
      }
    }
    return 'error';
  }
  isEnabledSecureTouch(services) {
    if (services[this.SECURE_TOUCH] !== undefined) {
      return true;
    } else {
      return false;
    }
  }

}

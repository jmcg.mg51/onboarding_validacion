
export let Rutas = {
  // DAON
  instrucciones: '/services/idmission/instruction/',
  selfie: '/services/idmission/selfie/',
  selfieVerification: '/services/idmission/selfie/verification/',
  chooseIdentity: '/services/idmission/document/identity/',
  documentInstruction: '/services/idmission/document/instruction/',
  documentCapture: '/services/idmission/document/capture/',
  documentConfirm: '/services/daon/document/confirm/',
  updateValues: '/services/idmission/document/update/',
  livenessInstruction: '/services/daon/liveness/instruction/',
  ocrValidation: '/services/idmission/document/ocr/',
  livenessCapture: '/services/daon/liveness/capture/',
  livenessResult: '/services/daon/liveness/result/',
  comprobanteDomicilio: '/services/comprobante-domicilio/instrucciones',
  cuentaClabe: '/services/cuenta-clabe/',
  telefono: '/services/cell-phone/',
  cfdi: '/services/cfdi/',
  correo: '/services/correo/',
  globalInstruction: '/services/global-instruction/',
  correoCode: '/services/correo/code/',
  error: '/services/error',
  fin: '/services/final',
  terminos: '/services/terminos',
  person: '/services/person/',
  curp: '/services/curp/',
  direccion: '/services/direccion/',
  seguroSocial: '/services/seguro-social/',
  cedulaProfesional: '/services/cedula-profesional/',
  envioPin: '/services/analisis-financiero/envio-pin/',
  analisisFinancieroTerminos: '/services/analisis-financiero/terminos/',
  analisisFinancieroAutorizacion: '/services/analisis-financiero/autorizacion',
  envioPinSignature: '/services/signature/envio-pin/',
  nip: '/services/signature/terminos/',
  datosAdicionales: '/services/datos-adicionales/',
  archivosAdjuntos: '/services/archivos-adjuntos/',
  codeQr: '/services/code-qr/',
  qrFinal: '/services/qr-final',
  tarjetaBancaria: '/services/tarjeta-bancaria',
  deviceConnection: '/services/device-connection',
  eleccionDocumentoManual: '/services/captura-manual/eleccion-documento',
  instruccionesManual: '/services/captura-manual/instrucciones',
  instruccionesReversoManual: '/services/captura-manual/instrucciones-reverso',
  documentoManualFrontal: '/services/captura-manual/frontal',
  documentoManualReverso: '/services/captura-manual/reverso',
  capturaDatos: '/services/captura-manual/captura-datos'


};

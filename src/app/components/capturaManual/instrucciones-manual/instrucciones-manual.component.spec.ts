import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstruccionesManualComponent } from './instrucciones-manual.component';

describe('InstruccionesManualComponent', () => {
  let component: InstruccionesManualComponent;
  let fixture: ComponentFixture<InstruccionesManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstruccionesManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstruccionesManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

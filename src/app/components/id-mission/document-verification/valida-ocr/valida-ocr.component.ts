import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../../model/sesion/SessionPojo';
import { ServicesGeneralService} from 'src/app/services/general/services-general.service';
import { WorkFlowService } from '../../../../services/session/work-flow.service';
import {GlobalInstructionComponent} from '../../../global-instruction/global-instruction.component';
import { MiddleMongoService } from '../../../../services/http/middle-mongo.service';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-valida-ocr',
  templateUrl: './valida-ocr.component.html',
  styleUrls: ['./valida-ocr.component.css']
})
export class ValidaOcrComponent implements OnInit {

  constructor(private workFlowService :WorkFlowService, private serviciogeneralService: ServicesGeneralService, private actRoute: ActivatedRoute, private session: SessionService, public router: Router,
              private middleDaon: MiddleDaonService, private spinner: NgxSpinnerService,
              private global: GlobalInstructionComponent, private middle: MiddleMongoService, private analyticService: AnaliticService) { 

                console.log("sessionStorage.getItem('ti')= ", sessionStorage.getItem('ti'));
                if(sessionStorage.getItem('ti')==='ID_CARD'){
                  this.nodoPrincipal='visualZone';
                }else if(sessionStorage.getItem('ti')==='PASSPORT') {
                  this.nodoPrincipal='mrz';
                }
              }

  filtersLoaded: Promise<boolean>;
  id: string;
  foto:string;
  public nombre: string;
  public direccion: string;
  public curp: string = null;
  object: sesionModel;
  nodoPrincipal: string;
  directionShow: boolean
  intentsDocument;
  zipCode;
  errorMensaje = '';
  colonia;
  edoValids = ['AGU','BCN','BCS','CAMP','CHP','CHH','COA','COL','DIF','DUR','GUA','GRO','HID','JAL','MEX','MIC','MOR','NAY','NLE','OAX','PUE','QUE','ROO','SLP','SIN','SON','TAB','TAM','TLA','VER','YUC','ZAC','CDMX']
  
  async ngOnInit() {
    await this.spinner.show();
    this.errorMensaje = '';
    if(sessionStorage.getItem('intentsDocument')){
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    }else{
      sessionStorage.setItem('intentsDocument', '0')
      this.intentsDocument = sessionStorage.getItem('intentsDocument'); 
    }

    if(sessionStorage.getItem("selfieImage")){
      this.foto = "data:image/png;base64,"+sessionStorage.getItem("selfieImage");
      console.log("OCR= " + this.foto);
    }
    
    
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});

    if(sessionStorage.getItem('reqAddress')){
      this.directionShow = true;
    }else{
      this.directionShow = false; 
    }
    await this.getDataOcr();
    //await this.validateOCR();
    // await this.getDataOcrConsume();
    await this.spinner.hide();

  }

  async getDataOcr() {
    //let additionalData = JSON.parse(sessionStorage.getItem('additionalData'));
    //let reqAddress = JSON.parse(sessionStorage.getItem('reqAddress'));
    //let reqName = JSON.parse(sessionStorage.getItem('reqName'));
    //console.log("reqAdrres OCR", reqAddress);
    this.curp = sessionStorage.getItem('curp');
    var calle = sessionStorage.getItem('calle');
    var numero = sessionStorage.getItem('numero');
    this.colonia = sessionStorage.getItem('colonia');
    this.zipCode = sessionStorage.getItem('zip');
    var city = sessionStorage.getItem('municipio');
    var edo = sessionStorage.getItem('edo');

    var nombre = sessionStorage.getItem('nombres');
    var apellido = sessionStorage.getItem('aPaterno');
    var apellidoM = sessionStorage.getItem('aMaterno');

    // if(this.zipCode.length <= 1){
    //   this.getZipCode(this.colonia);
    // }

    this.nombre = nombre + " "+ apellido + " " + apellidoM;
    this.direccion = calle + " " + numero + " " + this.colonia + " " + this.zipCode + " " + city + " " + edo;   
    this.showDirecction();

    

  }

  getZipCode(colony){
    let idx = colony.search(/\d{5}/);
    if (idx >= 0) {
      this.zipCode = colony.substr(idx, idx + 5);
      console.log("zip,", this.zipCode);
    }
    this.colonia = this.colonia.substr(0, this.colonia.length -5);

  }

  // async getDataOcrConsume() {
  //   var resultDatos = await this.middleDaon.getDataOCR(this.id);
  //   console.log("rrrres=",resultDatos) ;
  //   this.curp = sessionStorage.getItem('curp');
  //   const modal = document.getElementById('modalErrorOCR2');
  //   modal.style.display = 'none';
  //   if (resultDatos['error']) {
  //     //modal que indiaca un error al leer el OCR

  //     let m = undefined;
  //     if (resultDatos['error']['error']) {
  //       m = JSON.stringify(resultDatos['error']['error']);  
  //     }
      
  //     console.log("resultDatos", m);
  //     if(m && m.includes('QUEUED')){
  //       modal.style.display = 'block';
  //       setTimeout(() => {
  //         console.log('sleep');
  //         this.getDataOcrConsume();
  //         return;
  //       },  Math.floor(Math.random() * (+3000 + 1000 - +1000)) + +1000);
      
  //     }else{ 
  //       const modal = document.getElementById('modalErrorOCR');
  //       modal.style.display = 'block';
  //     }
  //     await this.spinner.hide();
  //     return;
  //   } else {
  //     console.log("this.nodoPrincipal=", this.nodoPrincipal);
  //     if(resultDatos[this.nodoPrincipal]){ 

  //       if(resultDatos[this.nodoPrincipal]['25']){
  //         if(resultDatos[this.nodoPrincipal]['25']['value']){ 
  //           this.nombre = resultDatos[this.nodoPrincipal]['25']['value'].replace(/\^/g, ' ');
  //         }
  //         sessionStorage.setItem('nombres', resultDatos[this.nodoPrincipal]['25'].name);
  //         sessionStorage.setItem('aPaterno', resultDatos[this.nodoPrincipal]['25'].lastName);
  //         sessionStorage.setItem('aMaterno', resultDatos[this.nodoPrincipal]['25'].mothersLastName);
  //       }

  //       if(sessionStorage.getItem('direccion') === "si"){
  //         console.log("Si hay valor en la dirección de session");
  //         var calle = sessionStorage.getItem('calle');
  //         var numero = sessionStorage.getItem('numero');
  //         var colonia = sessionStorage.getItem('colonia');
  //         var zip = sessionStorage.getItem('zip');
  //         var city = sessionStorage.getItem('city');
  //         var edo = sessionStorage.getItem('edo');

  //         this.direccion = calle + " " + numero + " " + colonia + " " + zip + " " + city + " " + edo;   
  //         this.showDirecction();
  //       }else{
          
  //         if (resultDatos[this.nodoPrincipal]['134873105'] || resultDatos[this.nodoPrincipal]['17']) {
  //           this.direccion = resultDatos[this.nodoPrincipal]['17'] ? resultDatos[this.nodoPrincipal]['17']['value']
  //           :( resultDatos[this.nodoPrincipal]['134873105'] ?  resultDatos[this.nodoPrincipal]['134873105']['value'] : undefined);
  //           if( resultDatos[this.nodoPrincipal]['17'] ) {
  //             sessionStorage.setItem('calle', resultDatos[this.nodoPrincipal]['17'].street );
  //             sessionStorage.setItem('numero', resultDatos[this.nodoPrincipal]['17'].externalNumber );
  //             sessionStorage.setItem('colonia', resultDatos[this.nodoPrincipal]['17'].colony );
  //             if(resultDatos[this.nodoPrincipal]['17'].zipCode){
  //               sessionStorage.setItem('zip', resultDatos[this.nodoPrincipal]['17'].zipCode );
  //             }else{
  //               sessionStorage.setItem('zip', resultDatos[this.nodoPrincipal]['17'].zip );
  //             }
              
  //             sessionStorage.setItem('city', resultDatos[this.nodoPrincipal]['17'].city );
  //             sessionStorage.setItem('edo', resultDatos[this.nodoPrincipal]['17'].state );
  //           }
  //           if (this.direccion) {
  //             this.directionShow = true;
  //             this.direccion = this.direccion.replace(/\^/g, ' ');
  //             console.log(this.direccion)
  //           }else{
  //             this.directionShow = false;
  //           }
  //           console.log(resultDatos[this.nodoPrincipal]['17']['value'])
  //         }
  //       }

  //       if (resultDatos[this.nodoPrincipal]['7']){
  //        if(resultDatos[this.nodoPrincipal]['7']['value'].length > 10 ) {
  //         this.curp = resultDatos[this.nodoPrincipal]['7']['value'];
  //         sessionStorage.setItem('curp' , this.curp);
  //         if(sessionStorage.getItem('direccion') === "si"){
  //           console.log("Si hay valor en la dirección de session");
  //           var calle = sessionStorage.getItem('calle');
  //           var numero = sessionStorage.getItem('numero');
  //           var colonia = sessionStorage.getItem('colonia');
  //           var zip = sessionStorage.getItem('zip');
  //           var city = sessionStorage.getItem('city');
  //           var edo = sessionStorage.getItem('edo');
            
  //           this.directionShow= true;
  //           this.direccion = calle + " " + numero + " " + colonia + " " + zip + " " + city + " " + edo;
  //           console.log("El nodo de dirección de passaporte es:", this.direccion);  
            
  //         }else if(resultDatos[this.nodoPrincipal]['17'] && resultDatos[this.nodoPrincipal]['17']['value']){
  //           this.directionShow = true;
  //         }
  //         else{
  //           this.directionShow = false;
  //         }
          
  //       }
  //     }
  //   }
  //   }
  //   this.filtersLoaded = Promise.resolve(true);
  //   await this.spinner.hide();
  //   //this.setColor();
  // }


  async alredySessionExist() { 
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.identity===true){
        //this.router.navigate([Rutas.livenessInstruction]);

        this.router.navigate([this.workFlowService.redirectFlow('documento',this.object.servicios)]);
        return false;
      } else {
        return true;
      }
    }
  }

  async updateValuesSession() {
    await this.spinner.show();
    var edo = sessionStorage.getItem('edo');
    if( (this.object  && this.object.servicios && this.object.servicios['ficoScore']) && !this.edoValids.includes(edo)) {
      this.errorMensaje = 'Favor de actualizar el estado de tu dirección';
      await this.spinner.hide();
      return;
    }
    const object = this.session.getObjectSession();
    object.daon.identity = true;
    this.session.updateModel(object);
    console.log("Antes== " , object);

    const device = this.analyticService.getTypeDevice();
    var intentsDocumentSession = sessionStorage.getItem('intentsDocument');
    this.intentsDocument = parseInt(intentsDocumentSession) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);

    var intentsDireccion;
    var intentsCurp; 
    if(sessionStorage.getItem('intentsDirection')){
        intentsDireccion = sessionStorage.getItem('intentsDirection');
        const analytics = this.analyticService.updateAnalytics("direccion", device, intentsDireccion, "NO", false);
        console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
    }

    if(sessionStorage.getItem('intentsCurp')){
      intentsCurp = sessionStorage.getItem('intentsCurp');
      const analytics = this.analyticService.updateAnalytics("curp", device, intentsCurp, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
  }
    
    const analytics = this.analyticService.updateAnalytics("documento", device, this.intentsDocument, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);


  await this.middleDaon.updateDaonDataUser(object, this.id);
    
  
    if(sessionStorage.getItem('direccion') === "si"){
      var street = sessionStorage.getItem('calle');
      var number = sessionStorage.getItem('numero');
      var colonia = sessionStorage.getItem('colonia');
      var zip = sessionStorage.getItem('zip');
      var city = sessionStorage.getItem('municipio');


      const address = {
        calle: street,
        numero: number,
        colonia: colonia,
        zip: zip,
        city: city,
        edo: edo
    }
    
    let result = await this.middle.updateDataUser({ address: address }, this.id);
    console.log("El valor de la dirección", result);

    }

    sessionStorage.setItem("contratante", this.nombre);
    sessionStorage.setItem("direccionContratante", this.direccion);
    
    //Save images DOC. 
 
    let front = sessionStorage.getItem("idFront");
    let back = sessionStorage.getItem("idBack");

    if(sessionStorage.getItem('serviceId') === "PP"){
      await this.middleDaon.sendInfoDaon({data:front, type: "Passport", capturaManual: "true"}, this.id, 'document'); 
    }else{
      await this.middleDaon.sendInfoDaon({data:front, type: "Voting Card Front", capturaManual: "true"}, this.id, 'document'); 
      await this.middleDaon.sendInfoDaon({data:back, type: "Voting Card Back", capturaManual: "true"}, this.id, 'document'); 
    }


    let reqAddress = {
      calle : sessionStorage.getItem('calle'),
      numero : sessionStorage.getItem('numero'),
      colonia : sessionStorage.getItem('colonia'),
      zip : sessionStorage.getItem('zip'),
      city : sessionStorage.getItem('municipio'),
      edo : sessionStorage.getItem('edo')
    }
    let reqName = {
      nombres: sessionStorage.getItem('nombres'),
      aPaterno: sessionStorage.getItem('aPaterno'),
      aMaterno: sessionStorage.getItem('aMaterno'),
    }

    let curpSession = sessionStorage.getItem('curp');

    let responseName = await this.middle.updateDataUser({ name: reqName }, this.id);
    let responseAddress = await this.middle.updateDataUser({ address: reqAddress }, this.id);
    let reqCurp = {
      curp: curpSession
    };
    let cic = sessionStorage.getItem('cic');
    let identificadorCiudadano = sessionStorage.getItem('identificadorCiudadano');
    if(cic && cic != null && identificadorCiudadano && identificadorCiudadano != null) {
      reqCurp['cic'] = cic;
      reqCurp['identificadorCiudadano'] = identificadorCiudadano;
    }
    let resultCurp = await this.middle.updateDataUser(reqCurp, this.id);
    
    let ruleDocumento = sessionStorage.getItem("statusDocument");
    let ruleSelfie = sessionStorage.getItem("statusSelfie");
    
    
    if(sessionStorage.getItem("actualizoNombre")  && sessionStorage.getItem("actualizoNombre")  !== null ){
      ruleDocumento += ", User has update";
    }

    if(sessionStorage.getItem("rulesDocumentoOCR")) {
      let ocrDocumentRules = JSON.parse(sessionStorage.getItem("rulesDocumentoOCR"));
      if(ocrDocumentRules.RealnessCheck_LABEL_ID_BACK  === "screen" || 
      ocrDocumentRules.RealnessCheck_LABEL_ID_FRONT  === "screen"){
        // ruleDocumento += ", RealnessCheck_LABEL_ID_FRONT:"+ocrDocumentRules.RealnessCheck_LABEL_ID_FRONT+", RealnessCheck_LABEL_ID_BACK:"+ocrDocumentRules.RealnessCheck_LABEL_ID_BACK;
        ruleDocumento += ", Verification_Result:" + ocrDocumentRules;
      }
    }

    if((sessionStorage.getItem("statusSelfie")  && sessionStorage.getItem("statusSelfie")  !== null ) && 
    (sessionStorage.getItem("rules") && sessionStorage.getItem("rules") !== null)
    ){
      let rulesSelfie;
      if(sessionStorage.getItem("rules") === "no se pudo recuperar las reglas"){
        ruleSelfie += ", reglas: no se recupero ninguna regla";
      }else{
      rulesSelfie = JSON.parse(sessionStorage.getItem("rules"));
      ruleSelfie += ", eyeConvering:"+rulesSelfie.eyeConvering+", faceMask:"+rulesSelfie.faceMask + ", faceMaskScore:"+rulesSelfie.faceMaskScore+  ", faceHeadCovering:"+rulesSelfie.faceHeadCovering+", liveFace:"+rulesSelfie.liveFace + ", faceVerificationResult:"+rulesSelfie.faceVerificationResult;
      }
    }
    console.log('el ruleDocumento es: ', ruleDocumento);
    if(sessionStorage.getItem("statusSelfie") && sessionStorage.getItem("statusDocument")){
      await this.middle.updateDataUser({ statusIdMission: ruleSelfie+"-"+ ruleDocumento }, this.id);
    }else{
      await this.middle.updateDataUser({ statusIdMission:   ruleDocumento }, this.id);
    }

    
    

    this.router.navigate([this.workFlowService.redirectFlow('documento',this.object.servicios)]);
    await this.spinner.hide();
  }

async updateInfo() {
  this.router.navigate([Rutas.updateValues]);
}

  async backAndTakeAgain() {
    await this.spinner.show();
    sessionStorage.removeItem('fb');
    sessionStorage.setItem('fb', 'front');
    this.serviciogeneralService.setFrontAndBack('front');
    sessionStorage.setItem('direccion', "no");


    //inicializar valores de servicio
    const device = this.analyticService.getTypeDevice(); 
    var intentsDocument = sessionStorage.getItem('intentsDocument');
    var intentsDireccion;
    var intentsCurp; 
    if(sessionStorage.getItem('intentsDirection')){
        intentsDireccion = sessionStorage.getItem('intentsDirection');
        const analytics = this.analyticService.updateAnalytics("direccion", device, intentsDireccion, "SI", false);
        console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
    }

    if(sessionStorage.getItem('intentsCurp')){
      intentsCurp = sessionStorage.getItem('intentsCurp');
      const analytics = this.analyticService.updateAnalytics("curp", device, intentsCurp, "SI", false);
      console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
  }
    
    const analytics = this.analyticService.updateAnalytics("documento", device, this.intentsDocument, "SI", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);

    this.router.navigate([Rutas.documentInstruction]);
    await this.spinner.hide();
  }

  setColor(){
    $("#againOCR").css("color","#6c757d");

    $("#confirmOCR").hover(function(){
      var movilContinue = document.getElementById("againOCR") as HTMLInputElement;
      movilContinue.style.backgroundColor = "white"; 
      movilContinue.style.color = "#6c757d";
      });

      $("#againOCR").hover(function(){
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        

        });
  }

  async cerrarModal() {
    const modal = document.getElementById('modalErrorOCR');
    modal.style.display = 'none';
  }
  async aceptar() {
    await this.spinner.hide();
    sessionStorage.setItem('errorDocument' , 'Ocurrio un error, favor de volver a intentar');
    sessionStorage.removeItem('fb');
    sessionStorage.setItem('fb', 'front');
    this.serviciogeneralService.setFrontAndBack('front')
    this.router.navigate([Rutas.documentInstruction]);
    await this.spinner.hide();
  }

  showDirecction(){
    let typeID = sessionStorage.getItem("serviceId");
    this.directionShow = typeID === "PP" ? false : true; 
  }

  async validateOCR(){
    if(sessionStorage.getItem("nombres") === "" || sessionStorage.getItem("aPaterno") === "" ||
    sessionStorage.getItem("aMaterno") === "" || sessionStorage.getItem("curp") === "" ||
    sessionStorage.getItem("calle") === "" || sessionStorage.getItem("zip") === "" ||
    sessionStorage.getItem("municipio") === "" || sessionStorage.getItem("edo") === "" ||
    sessionStorage.getItem("colonia") === "" ){
        this.updateInfo();
    }else{
      this.router.navigate([Rutas.ocrValidation]);
    }

  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

}

import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Rutas } from '../../model/RutasUtil';
import { SessionService } from '../../services/session/session.service';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import { sesionModel } from '../../model/sesion/SessionPojo';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { MiddleVerificacionService } from '../../services/http/middle-verificacion.service';
import { ServicesGeneralService, isMobile } from '../../services/general/services-general.service';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { TermsComponent } from '../terms/terms.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-correo-verificacion',
  templateUrl: './correo-verificacion.component.html',
  styleUrls: ['./correo-verificacion.component.css']
})

export class CorreoVerificacionComponent implements OnInit {

  constructor(private workFlowService: WorkFlowService, public serviciogeneralService: ServicesGeneralService, private router: Router, private session: SessionService,
              private actRoute: ActivatedRoute, private middleDaon: MiddleDaonService,
              private middleMongo: MiddleMongoService, private spinner: NgxSpinnerService,
              private middleVerifica: MiddleVerificacionService,
              private term: TermsComponent, private analyticService: AnaliticService) {  }

  filtersLoaded: Promise<boolean>;
  object: sesionModel;
  correoText = '';
  error = '';
  id: any;
  isMobileBool: boolean;
  copiar:string = '      ';
  url;
  intentsSendMail;
  showErrorEmail;

  // ngAfterViewInit() {
  //   document.getElementById('correoInput').focus();
  // }

  async ngOnInit() {

    await this.spinner.show();
    this.showErrorEmail = false;
    this.isMobileBool = isMobile(navigator.userAgent);
    console.log("es celular?", this.isMobileBool);
    
    // if(this.isMobileBool ){
    //   await this.spinner.show();
    //   //this.abrirModal();
    // }else{
    //   this.spinner.show();
    // }
    
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    
    if(sessionStorage.getItem('intentsEmail')){
      this.intentsSendMail = sessionStorage.getItem('intentsEmail');
    }else{
      sessionStorage.setItem('intentsEmail', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("correoElectronico", device)

      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middleMongo.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsSendMail = sessionStorage.getItem('intentsEmail'); 
    }


    var baseUrl = document.location.origin;
    console.log(baseUrl);
    console.log(this.id);
    this.url = baseUrl + '#' + Rutas.globalInstruction + `${this.id}`;  

    try{
      const fp = await FP.load({token: environment.fingerJsToken, 
      endpoint: environment.fpDomain});
      fp.get({tag: {'tag':this.id}});
    }catch(error) {
      await this.middleVerifica.sendDataWebhookFinger({ message: error, id: this.id });
    }


    if(sessionStorage.getItem("email")){
      this.serviciogeneralService.setCorreo(sessionStorage.getItem("email"));
      await this.middleVerifica.generaCodigoEmail(this.id, sessionStorage.getItem("email"), false);
      this.object.emailVerified = true;
      this.object.correo = true;
      this.session.updateModel(this.object);
      this.router.navigate([this.workFlowService.redirectFlow('correoElectronico', this.object.servicios)]);

    }
    

    
    this.loadScript();
    this.deleteEmojis();
    this.filtersLoaded = Promise.resolve(true);
    if(document.getElementById('correoInput')){
      document.getElementById('correoInput').focus();
    }
    
    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    console.log(this.object);
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.correo===true && this.object.emailVerified===true){
        this.router.navigate([this.workFlowService.redirectFlow('correoElectronico',this.object.servicios)]);
        return false;
      } else {
        return true;
      }
    }
  }

  onSearchChange(searchValue: string): void {
    this.correoText = searchValue;
  }

  async aceptar() {
    await this.spinner.show();
    this.intentsSendMail =  parseInt(this.intentsSendMail) + 1;
    sessionStorage.setItem('intentsEmail', this.intentsSendMail);

    this.correoText=this.correoText.toLowerCase().trim();
    console.log('correo= ' + this.correoText);
    if (this.correoText.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')) {
      this.correoText = this.correoText.trim();
      this.showErrorEmail = false;
      await this.middleVerifica.generaCodigoEmail(this.id, this.correoText, true);
      this.serviciogeneralService.setCorreo(this.correoText);
      this.router.navigate([Rutas.correoCode]);
      await this.spinner.hide();
      return true;
    } else {
      this.showErrorEmail = true;
      await this.spinner.hide();
        return false;
    }
  }

  public loadScript() {
    let body = <HTMLDivElement> document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/SecureOnBoarding.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
}

public deleteEmojis(){
  let body = <HTMLDivElement> document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/dontUseEmojis.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

}

async cerrarModal() {
  const modal = document.getElementById('modalAlertBrowser');
  modal.style.display = 'none';
}

async abrirModal() {
  const modal = document.getElementById('modalAlertBrowser');
  modal.style.display = 'block';
  sessionStorage.setItem('alertBrowser', 'true');
}

public getColorButton(){
  return this.term.getButtonColorSystem();
}

public getColorLabel(){
  return this.term.getLabelColorSystem();
}

public getImageSystem(){
  return this.term.getImageConfig();
}

copyLink(){
  var aux = document.createElement("input");
  var value = $("#link").val().toString();
  aux.setAttribute("value", value);
  document.body.appendChild(aux);
  console.log(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  this.copiar="Copiado!";
  
  setTimeout(() => {
    this.copiar=" ";
  }, 2000);
}


}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoReversoComponent } from './documento-reverso.component';

describe('DocumentoReversoComponent', () => {
  let component: DocumentoReversoComponent;
  let fixture: ComponentFixture<DocumentoReversoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoReversoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoReversoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { SessionService } from 'src/app/services/session/session.service';

declare const deviceInfo: any;

@Component({
  selector: 'app-validate-device',
  templateUrl: './validate-device.component.html',
  styleUrls: ['./validate-device.component.css']
})
export class ValidateDeviceComponent implements OnInit {

  copiar: string = '      ';
  isMobileBool: boolean;
  sessionGetAlert: string;

  url: string;
  id: string;
  object: sesionModel;

  constructor(public router: Router, private session: SessionService,) { }

  ngOnInit() {
    var responseNavigator = this.validateDevive();
    console.log("validación del navegador", responseNavigator);
    this.isMobileBool = this.isMobile(navigator.userAgent);
    this.sessionGetAlert = sessionStorage.getItem('alertBrowser');

    var baseUrl = document.location.origin;
    this.object = this.session.getObjectSession();
    console.log(baseUrl);
    console.log(this.id);
    this.url = baseUrl + '#' + Rutas.globalInstruction + `${this.id}`;
    if (this.isMobileBool) {
      console.log("valor de session alert", this.sessionGetAlert);
      if (this.sessionGetAlert == 'true') {

      } else {
        console.log('aqui prendia el spinner');
      }
    } else {
      console.log('aqui prendia el spineer ');
    }
    if (!responseNavigator) {
      this.router.navigate([Rutas.deviceConnection]);
    }

  }


  async cerrarModal() {
    const modal = document.getElementById('modalAlertBrowser');
    modal.style.display = 'none';
  }

  isMobile(userAgent) {
    return !!userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone)/i);
  }

  validateDevive() {
    let device = deviceInfo();
    console.log("información del dispositivo", device);
    switch (device['os.name']) {
      case 'Macintosh':
        console.log("Soy una mac con:", device['browser.name']);
        return true;
        break;
      case 'Windows':
        console.log("Soy una windows con:", device['browser.name']);
        return true;
        break;
      case 'Android':
        console.log("Soy un android con:", device['browser.name']);
        if (device['browser.name'] === "Firefox" || device['browser.name'] === "Chrome" || device['browser.name'] === "Opera"
          || device['browser.name'] === "Safari") {
          if ("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices) {
            console.log("Soy un navegador valido android con:", device['browser.name'])
            return true;
          } else {
            console.log("HOLA NAVEGADOR RECHAZED");
            return false;
          }

        } else {
          return false;
        }
        break;
      case 'iPad':
        console.log("Soy un ipad con:", device['browser.name']);
        if (device['browser.name'] === "Safari") {
          console.log("Soy un navegador valido android con:", device['browser.name']);
          if ("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices) {
            console.log("Soy un navegador valido android con:", device['browser.name'])
            return true;
          } else {
            console.log("HOLA NAVEGADOR RECHAZED");
            return false;
          }
        } else {
          //Redirect a página de error
          return false;
        }
        break;
      case 'iPhone':
        console.log("Soy un iphone con:", device['browser.name']);
        if (device['browser.name'] === "Safari") {
          console.log("Soy un navegador valido android con:", device['browser.name']);
          if ("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices) {
            console.log("Soy un navegador valido android con:", device['browser.name'])
            return true;
          } else {
            console.log("HOLA NAVEGADOR RECHAZED");
            /**CAMBIAR A FALSE */
            return true;
          }
        } else {
          //Redirect a página de error
          return false;
        }
        break;
      default:
        return false;
        break;
    }
  }

  copyLink() {
    var aux = document.createElement("input");
    var value = $("#link").val().toString();
    aux.setAttribute("value", value);
    document.body.appendChild(aux);
    console.log(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
    this.copiar = "Copiado!";

    setTimeout(() => {
      this.copiar = " ";
    }, 2000);
  }

  async abrirModal() {
    const modal = document.getElementById('modalAlertBrowser');
    modal.style.display = 'block';
    sessionStorage.setItem('alertBrowser', 'true');
  }

}

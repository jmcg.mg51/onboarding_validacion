import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';
import { async } from 'rxjs/internal/scheduler/async';
import { Rutas } from 'src/app/model/RutasUtil';
import { Router } from '@angular/router';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { SessionService } from 'src/app/services/session/session.service';
import { GlobalInstructionComponent } from 'src/app/components/global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-update-information-data',
  templateUrl: './update-information-data.component.html',
  styleUrls: ['./update-information-data.component.css']
})
export class UpdateInformationDataComponent implements OnInit {

  showOtherColony: boolean;
  arrayStreet: any;
  resulSearch: any;
  coloniaInput: any;
  errorMessage: string;
  zipDinamic: boolean;
  id: string;
  object: sesionModel;
  updateValuesForm = this.formBuilder.group({
    nombres: ['', { validators: [Validators.required] }],
    aPaterno: [''],
    aMaterno: [''],
    curp: ['', { validators: [Validators.required] }],
    calle: ['', { validators: [Validators.required] }],
    numero: ['', { validators: [Validators.required] }],
    colonia: [''],
    codigoPostal: ['', { validators: [Validators.required] }],
    ciudad: [''],
    estado: ['', { validators: [Validators.required] }]
  });
  intentsDocument;

  listEstado = [
    { clave: 'AGU', nombre: 'Aguascalientes' },
    { clave: 'BCN', nombre: 'Baja California' },
    { clave: 'BCS', nombre: 'Baja California Sur' },
    { clave: 'CAMP', nombre: 'Campeche' },
    { clave: 'CHP', nombre: 'Chiapas' },
    { clave: 'CHH', nombre: 'Chihuahua' },
    { clave: 'COA', nombre: 'Coahuila' },
    { clave: 'COL', nombre: 'Colima' },
    { clave: 'DIF', nombre: 'Ciudad de México' },
    { clave: 'DUR', nombre: 'Durango' },
    { clave: 'GUA', nombre: 'Guanajuato' },
    { clave: 'GRO', nombre: 'Guerrero' },
    { clave: 'HID', nombre: 'Hidalgo' },
    { clave: 'JAL', nombre: 'Jalisco' },
    { clave: 'MEX', nombre: 'México' },
    { clave: 'MIC', nombre: 'Michoacán' },
    { clave: 'MOR', nombre: 'Morelos' },
    { clave: 'NAY', nombre: 'Nayarit' },
    { clave: 'NLE', nombre: 'Nuevo León' },
    { clave: 'OAX', nombre: 'Oaxaca' },
    { clave: 'PUE', nombre: 'Puebla' },
    { clave: 'QUE', nombre: 'Querétaro' },
    { clave: 'ROO', nombre: 'Quintana Roo' },
    { clave: 'SLP', nombre: 'San Luis Potosí' },
    { clave: 'SIN', nombre: 'Sinaloa' },
    { clave: 'SON', nombre: 'Sonora' },
    { clave: 'TAB', nombre: 'Tabasco' },
    { clave: 'TAM', nombre: 'Tamaulipas' },
    { clave: 'TLA', nombre: 'Tlaxcala' },
    { clave: 'VER', nombre: 'Veracruz' },
    { clave: 'YUC', nombre: 'Yucatán' },
    { clave: 'ZAC', nombre: 'Zacatecas' }
  ];
  nrSelect = '';

  constructor(private formBuilder: FormBuilder, private spinner: NgxSpinnerService,
    private middle: MiddleMongoService, public router: Router,
    private session: SessionService, private global: GlobalInstructionComponent,
    private analyticService: AnaliticService) { }

  async ngOnInit() {
    await this.spinner.show();
    if (!(await this.alredySessionExist())) { return; }
    console.log(this.object);
    this.id = this.object._id;

    if (sessionStorage.getItem('intentsDocument')) {
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    } else {
      sessionStorage.setItem('intentsDocument', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("documento", device)
      console.log("El id es:", this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    }

    let zipId;
    sessionStorage.getItem('zip') === "undefined" ? zipId = "" : zipId = sessionStorage.getItem('zip');
    if (zipId && zipId != '') {
      this.zipDinamic = true;
      await this.searchCode2(zipId);
    }
    // let zipId = sessionStorage.getItem('zip') ? "";
    this.updateValuesForm.patchValue({
      nombres: (sessionStorage.getItem('nombres')),
      aPaterno: (sessionStorage.getItem('aPaterno')),
      aMaterno: (sessionStorage.getItem('aMaterno')),
      curp: (sessionStorage.getItem('curp')),
      calle: (sessionStorage.getItem('calle')),
      numero: (sessionStorage.getItem('numero')),
      ciudad: (sessionStorage.getItem('municipio')),
      codigoPostal: zipId,
      //ciudad: (sessionStorage.getItem('city')),
      // estado: (sessionStorage.getItem('edo')),
      colonia: (sessionStorage.getItem('colonia') || ''),
    });
    let estadoCombo = sessionStorage.getItem('edo');
    if (estadoCombo && estadoCombo != null && estadoCombo != '' && estadoCombo.length >= 3) {
      this.nrSelect = estadoCombo.substring(estadoCombo.length - 3, estadoCombo.length);
    }
    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    }
    return true;
  }


  async searchCode($event: Event) {
    this.zipDinamic = true;
    const code = ($event.target as HTMLInputElement).value;
    if (code !== "") {
      this.spinner.show();
      console.log("voy a bucar:", code);
      this.resulSearch = await this.middle.getInfoStreet(code);
      console.log(this.resulSearch);
      if (this.resulSearch === "ERROR" || this.resulSearch === undefined || this.resulSearch.errorType) {
        //Eliminar 
        //this.arrayStreet = this.testDireccion;
        //console.log("array", this.arrayStreet);
        console.log("true");
        this.arrayStreet = [];
        this.showOtherColony = true;
        try {
          const select = document.getElementById("ciudadSelect") as HTMLInputElement;
          select.value = "OTRO";
          select.disabled = true;
          select.style.display = "none";
        } catch (error) {
          console.log('Error al cargar el select de ciudad', error);
        }

      } else {
        console.log("validacion");
        const select = document.getElementById("ciudadSelect") as HTMLInputElement;
        select.style.display = "block";
        this.showOtherColony = false;
        this.arrayStreet = this.resulSearch;

      }

      this.spinner.hide();
    }
  }

  async searchCode2(code: string) {
    if (code !== "") {
      console.log("voy a bucar:", code);
      this.resulSearch = await this.middle.getInfoStreet(code);
      console.log(JSON.stringify(this.resulSearch));
      if (this.resulSearch === "ERROR" || this.resulSearch === undefined || this.resulSearch.errorType) {
        //Eliminar 
        //this.arrayStreet = this.testDireccion;
        //console.log("array", this.arrayStreet);
        console.log("true");
        this.arrayStreet = [];
        this.showOtherColony = true;
        try {
          const select = document.getElementById("ciudadSelect") as HTMLInputElement;
          select.value = "OTRO";
          select.disabled = true;
          select.style.display = "none";
        } catch (error) {
          console.log('Error al cargar el select de ciudad', error);
        }

      } else {
        console.log("validacion");
        try {
          const select = document.getElementById("ciudadSelect") as HTMLInputElement;
          select.style.display = "block";
          this.showOtherColony = false;
          this.arrayStreet = this.resulSearch;
        } catch (error) {
          console.log('Error al cargar el select de ciudad ' +  JSON.stringify(error));
        }
      }
    }
  }



  async getInfoCiudad($event: Event) {
    const value = (document.getElementById("ciudadSelect") as HTMLInputElement).value;
    console.log("el value es", value);
    if (value === "OTRO") {
      this.showOtherColony = true;
      this.updateValuesForm.patchValue({
        ciudad: '',
        estado: '',
        colonia: ''
      });
    } else {
      this.showOtherColony = false;
      for (var i = 0; i < this.arrayStreet.length; i++) {
        console.log("iteracion", this.arrayStreet[i]);
        if (this.arrayStreet[i].colony === value) {
          this.updateValuesForm.patchValue({
            ciudad: this.arrayStreet[i].municipality,
            estado: this.arrayStreet[i].state,
            colonia: this.arrayStreet[i].colony
          });
        }
      }
    }
  }
  async cancelar() {
    this.errorMessage = '';
    this.router.navigate([Rutas.ocrValidation]);
  }

  curpValidate(curp) {
    var regexCurp = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
    if (curp.match(regexCurp)) {
      console.log("validado");
      return true;
    } else {
      return false;
    }
  }

  async saveValues() {
    await this.spinner.show();
    this.errorMessage = '';
    let curp = this.updateValuesForm.controls.curp.value.toUpperCase();
    let colonia = this.updateValuesForm.controls.colonia.value.toUpperCase() || sessionStorage.getItem('colonia');
    if (!colonia || colonia === '') {
      colonia = (document.getElementById('coloniaInput') as HTMLInputElement) ?
        (document.getElementById('coloniaInput') as HTMLInputElement).value.toUpperCase() : '';
    }
    if (!this.updateValuesForm.valid || !colonia || colonia === '') {
      console.log('no es un form valido');
      this.errorMessage = 'Favor de llenar todos los campos';
      await this.spinner.hide();
      return;
    }
    if (!this.curpValidate(curp)) {
      this.errorMessage = 'El CURP es Incorrecto';
      await this.spinner.hide();
      return;
    }
    curp = curp.toUpperCase();
    const reqAddress = {
      calle: (this.updateValuesForm.controls.calle.value).toUpperCase(),
      numero: this.updateValuesForm.controls.numero.value.toUpperCase(),
      colonia,
      zip: this.updateValuesForm.controls.codigoPostal.value.toUpperCase(),
      city: this.updateValuesForm.controls.ciudad.value.toUpperCase(),
      edo: this.updateValuesForm.controls.estado.value.toUpperCase()
    };
    console.log(reqAddress);
    const reqName = {
      nombres: this.updateValuesForm.controls.nombres.value.toUpperCase(),
      aPaterno: this.updateValuesForm.controls.aPaterno.value.toUpperCase(),
      aMaterno: this.updateValuesForm.controls.aMaterno.value.toUpperCase()
    };
    console.log(reqName);


    if (sessionStorage.getItem('nombres') !== this.removeAccents(reqName.nombres)
      || sessionStorage.getItem('aPaterno') !== this.removeAccents(reqName.aPaterno)
      || sessionStorage.getItem('aMaterno') !== this.removeAccents(reqName.aMaterno) ) {
      reqName['cambio'] = true;
      sessionStorage.setItem("actualizoNombre", "true");
    }


    this.intentsDocument = parseInt(this.intentsDocument) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);
    console.log("si cuento el intento :) ");
    sessionStorage.setItem('nombres', this.updateValuesForm.controls.nombres.value.toUpperCase());
    sessionStorage.setItem('aPaterno', this.updateValuesForm.controls.aPaterno.value.toUpperCase());
    sessionStorage.setItem('aMaterno', this.updateValuesForm.controls.aMaterno.value.toUpperCase());
    sessionStorage.setItem('curp', this.updateValuesForm.controls.curp.value.toUpperCase());

    sessionStorage.setItem('calle', this.updateValuesForm.controls.calle.value.toUpperCase());
    sessionStorage.setItem('numero', this.updateValuesForm.controls.numero.value.toUpperCase());
    sessionStorage.setItem('zip', this.updateValuesForm.controls.codigoPostal.value.toUpperCase());
    sessionStorage.setItem('city', this.updateValuesForm.controls.ciudad.value.toUpperCase());
    sessionStorage.setItem('municipio', this.updateValuesForm.controls.ciudad.value.toUpperCase());
    sessionStorage.setItem('edo', reqAddress.edo);
    // sessionStorage.setItem('colonia',this.updateValuesForm.controls.colonia.value.toUpperCase());
    sessionStorage.setItem('colonia', reqAddress.colonia);
    await this.spinner.hide();
    this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton() {
    return this.global.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.global.getLabelColorSystem();
  }

  public removeAccents (str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  } 
}

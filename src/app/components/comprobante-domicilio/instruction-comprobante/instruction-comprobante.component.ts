import { Component, OnInit } from '@angular/core';
import { ServicesGeneralService, isMobile } from '../../../services/general/services-general.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import IconDefinitions from '../../../../assets/icons/icons-svn';
import { ErrorSelfieService } from 'src/app/services/errores/error-selfie.service';
import { environment } from '../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../model/sesion/SessionPojo';
import {TermsComponent} from '../../terms/terms.component';
import {GlobalInstructionComponent} from '../../global-instruction/global-instruction.component';
import { MiddleNubariumService } from 'src/app/services/http/middle-nubarium.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { WorkFlowService } from '../../../services/session/work-flow.service';

@Component({
  selector: 'app-instruction-comprobante',
  templateUrl: './instruction-comprobante.component.html',
  styleUrls: ['./instruction-comprobante.component.css']
})
export class InstructionComprobanteComponent implements OnInit {

  constructor(public router: Router, public serviciogeneralService: ServicesGeneralService, private actRoute: ActivatedRoute,
    private session: SessionService, private spinner: NgxSpinnerService,private middle: MiddleMongoService,
    private term: TermsComponent, private global: GlobalInstructionComponent,
    private nubariumService: MiddleNubariumService, private analyticService: AnaliticService,
    private workFlowService :WorkFlowService,) {
      

  }

  errorMensaje: string;
  description: string;
  id: string;
  dc: any;
  mensaje: string;
  img: any;
  fotoFT: string;
  idcard: any;
  icon: IconDefinitions;
  object: sesionModel;
  isMobileBool: boolean;
  isEdge: boolean;
  intentsComprobante;
  titulo:string = "Comprobante de domicilio";
  instruction:string;
  stepOne: string = "Captura alguno de los siguientes documentos:";
  stepTwo: string = "Telmex";
  stepThree: string = "CFE";

  continueOption: boolean;
  backOption: boolean;
  galeryOption: boolean;
  captureFoto: boolean;
  showViewCapture: boolean;
  image: string;
  data;
  foto;
  showGaleryOption: boolean;
  cameraCapture: boolean;
  countIntentsResponse;


//intentsDocument
  async ngOnInit() {
    await this.spinner.show();

    this.continueOption = false;
    this.backOption =false;
    this.captureFoto = false;
    this.showViewCapture = true;
    this.galeryOption = true;

    this.showGaleryOption = false;
    this.cameraCapture = true;
    
    this.isMobileBool = isMobile(navigator.userAgent);
    this.isEdge = window.navigator.userAgent.indexOf('Edge') > -1;
 //   document.getElementById('MovilContinue').style.display = 'none';
    console.log("es movil:",this.isMobileBool);
    this.fotoFT = "../../../assets/img/Daon/docs.png";
    this.titulo = "Comprobante de domicilio";
    this.instruction = "Captura alguno de los siguientes comprobantes en formato JPG o JPEG : CFE ó TELMEX";
    this.mensaje = "";

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    this.countIntentsResponse = 0;
    await this.spinner.hide();

    if(sessionStorage.getItem('intentsComprobante')){
      this.intentsComprobante = sessionStorage.getItem('intentsComprobante');
    }else{
      sessionStorage.setItem('intentsComprobante', '0')
      this.intentsComprobante = sessionStorage.getItem('intentsComprobante'); 
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("comprobanteDeDomicilio", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsComprobante = sessionStorage.getItem('intentsComprobante'); 
    }

    if (this.isMobileBool) {
      console.log("es celular " + navigator.userAgent);
      //document.getElementById('MovilContinue').style.display = 'nome';
      document.getElementById('captureCameraWeb').style.display = 'block';
      
    } else {
      console.log("es PC " + navigator.userAgent);
      //document.getElementById('MovilContinue').style.display = 'block';
      document.getElementById('captureCameraWeb').style.display = 'none';

    }

    


    $("#MovilContinue").css("color","#6c757d");

    $("#galeryButton").hover(function(){
      var movilContinue = document.getElementById("MovilContinue") as HTMLInputElement;
      //movilContinue.style.backgroundColor = "white"; 
      //movilContinue.style.color = "#6c757d";
      });

      $("#MovilContinue").hover(function(){
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        

        });

  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if (this.object.comprobanteDeDomicilio === true) {
        this.router.navigate([Rutas.comprobanteDomicilio]);
        return false;
      } else {
        return true;
      }
    }
  }


  async captureCamera() {
    this.spinner.show();
    this.showViewCapture = false;
    this.captureFoto = true;


    const constraints = {
      video: {
        facingMode: {
          exact: 'environment'
        }
        //width: { min: 1024, ideal: 1280, max: 1920 },
        //height: { min: 776, ideal: 720, max: 1080 }
        
      },
      
      };
    console.log("hasta aqui estoy bien");
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    this.intentsComprobante =  parseInt(this.intentsComprobante) + 1;
    sessionStorage.setItem('intentsComprobante', this.intentsComprobante);
    
    var $video = document.getElementById("video") as HTMLMediaElement;
    //$video.src = window.URL.createObjectURL();
    $video.srcObject = stream;
    $video.play();
    this.spinner.hide();
    //this.router.navigate([Rutas.documentCapture]);
  }

  movilContinue() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'block';
  }

  async processFile(imageInput: any) {
    await this.spinner.show();
    this.intentsComprobante =  parseInt(this.intentsComprobante) + 1;
    sessionStorage.setItem('intentsComprobante', this.intentsComprobante);
    var baseString;
    try {
      this.mensaje = '';
      const file: File = imageInput.files[0];
      console.log(file);

      var reader = new FileReader();
      this.foto = await this.blobToBase64(file);
      sessionStorage.setItem("imageComprobante", this.foto);
      //this.foto = this.foto.replace(/^data:image\/[a-z]+;base64,/, "");
      console.log('Foto= ' + this.foto);

      /** 
      reader.onloadend = async function () {
          baseString = await reader.result;
          console.log("64",baseString);
          sessionStorage.setItem("imageComprobante", baseString); 

      };
     await reader.readAsDataURL(file);**/

      const data = {
        trackId: this.id,
        comprobante: this.foto
      }

      //llamar al servicio
      //console.log("image to send",this.foto);
      const respuesta =  await this.nubariumService.validateDocument(data);
      console.log("respuesta", respuesta);

      if(respuesta === 200){
        this.titulo = "Previsualización del comprobante que se enviará";
        this.fotoFT = sessionStorage.getItem("imageComprobante");
        this.instruction = "";
        this.continueOption = true;
        this.backOption = false;
        this.galeryOption = false;
        this.captureFoto = false;
        this.showViewCapture = true;
        this.stopCameraJs();
        this.spinner.hide();
      }else{
        this.titulo = "Error en el comprobante";
        this.instruction = 'Haz click en "Volver a capturar" para intentarlo de nuevo '
        this.fotoFT = sessionStorage.getItem("imageComprobante");
        this.continueOption = false;
        this.backOption = true;
        this.galeryOption = false; 
        this.captureFoto = false;
        this.showViewCapture = true;
        this.stopCameraJs();
         //TODO

        this.spinner.hide();
      }

    } catch (e) {
      console.log('Error en el servicio intente de nuevo: ', e);
      this.mensaje = 'Error en el servicio intente de nuevo ';
      await this.spinner.hide();
    }
  }


  async enter(){
    this.spinner.show();
    //Evaluar la foto
    //Llamar al servicio
    this.countIntentsResponse = this.countIntentsResponse + 1; 
    var canvas = document.getElementById('canvas') as HTMLCanvasElement; 
    var video = document.getElementById('video') as HTMLVideoElement;

   
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height); 
    const image = canvas.toDataURL();
    sessionStorage.setItem("imageComprobante", image); 
    const data = {
      trackId: this.id,
      comprobante: image
    }

    console.log("enviar al servicio", data);

    this.image = image;
    const respuesta =  await this.nubariumService.validateDocument(data);
      if(respuesta === 200){
        this.titulo = "Previsualización del comprobante que se enviará";
        this.fotoFT = this.image;
        this.instruction = "";
        this.continueOption = true;
        this.backOption = false;
        this.galeryOption = false;
        this.captureFoto = false;
        this.showViewCapture = true;
        video.pause();
        this.spinner.hide();
        
      }else{
        this.titulo = "Error en el comprobante";
        this.instruction = 'Haz click en "Volver a capturar" para intentarlo de nuevo '
        this.fotoFT = this.image;
        this.continueOption = false;
        this.backOption = true;
        this.galeryOption = false; 
        this.captureFoto = false;
        this.showViewCapture = true;

        console.log("conteo", this.countIntentsResponse);
        if(this.countIntentsResponse % 2 == 0) {
          this.showGaleryOption = true; 
          this.cameraCapture = false; 
       }

        this.spinner.hide();
       //$video.src = window.URL.createObjectURL();
        video.pause();
      }


  }


  async aceptar() {
    await this.spinner.show();
    this.router.navigate([Rutas.codeQr]);
    await this.spinner.hide();
  }

  async back(){
    this.ngOnInit();
    this.router.navigate([Rutas.comprobanteDomicilio]);
  }

  async continue(){
    const object = this.session.getObjectSession();
      object.comprobanteDeDomicilio = true;
      this.session.updateModel(object);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("comprobanteDeDomicilio", device, this.intentsComprobante, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
     // await this.middle.updateDataUser({}, this.id); // GUARDAR INFORMACIÓN
      console.log('ya termine con el comprobante' + JSON.stringify(object, null, 2));
      this.router.navigate([this.workFlowService.redirectFlow('comprobanteDeDomicilio',this.object.servicios)]);
  }

  blobToBase64(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      try {
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
          resolve(reader.result);
        };
      } catch (err) {
        reject(err);
      }
    });
  }

  continueCamera(){
    this.showGaleryOption = false; 
    this.cameraCapture = true;
    this.mensaje = "";
  }

  stopCameraJs(){
    let htmlVideo = document.querySelector('video');
    if(!htmlVideo || htmlVideo === null) { 
      return;
    }
    var stream = navigator.mediaDevices.getUserMedia({video: true}).then(mediaStream => {
      console.log('el valor obtendio es: ' , htmlVideo);
        document.querySelector('video').srcObject = mediaStream;
        // Stop the stream after 5 seconds
        setTimeout(() => {
          const tracks = mediaStream.getTracks();
          console.log("los tracks son", tracks);
          tracks[0].stop()
        }, .001);

    });
  }

  async cerrarModal() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'none';
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}



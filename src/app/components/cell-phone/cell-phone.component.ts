import { Component, OnInit } from '@angular/core';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { NgxSpinnerService } from 'ngx-spinner';
import { Rutas } from '../../model/RutasUtil';
import { SessionService } from '../../services/session/session.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { sesionModel } from '../../model/sesion/SessionPojo';
import { FormControl, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { WorkFlowService } from '../../services/session/work-flow.service';
import {TermsComponent} from '../terms/terms.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
@Component({
  selector: 'app-cell-phone',
  templateUrl: './cell-phone.component.html',
  styleUrls: ['./cell-phone.component.css']
})
export class CellPhoneComponent implements OnInit {

  constructor(private workFlowService :WorkFlowService, private spinner: NgxSpinnerService, private actRoute: ActivatedRoute,
    private middle: MiddleMongoService, private session: SessionService, private router: Router,
    private term: TermsComponent, private analyticService: AnaliticService) { 
    }

  object: sesionModel;
  filtersLoaded: Promise<boolean>;
  id: any;
  myForm:any;
  submitted = false;
  intentsCell;

  async ngOnInit() {
    

    this.myForm = new FormGroup({
      celular: new FormControl('',[Validators.required, Validators.pattern("[0-9]{10}")])
    });

    await this.spinner.show();
    $('#cellphoneNumber').attr('maxlength', 10);
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;

    if(sessionStorage.getItem('intentsCell')){
      this.intentsCell = sessionStorage.getItem('intentsCell');
    }else{
      sessionStorage.setItem('intentsCell', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("telefono", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsCell = sessionStorage.getItem('intentsCell'); 
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    this.filtersLoaded = Promise.resolve(true);
    await this.spinner.hide();
    document.getElementById('cellphoneNumber').focus();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    console.log("tel= " + this.object);
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.telefono===true){
        this.router.navigate([this.workFlowService.redirectFlow('telefono',this.object.servicios)]);   
        return false;
      } else {
        return true;
      }
    }
  }

  async onSubmit() {
    this.submitted = true;
    if (this.myForm.invalid) { 
      await this.spinner.hide();
        return;
    }
  }

  onReset() {
      this.submitted = false;
      this.myForm.reset();
  }

  get f(){
    return this.myForm.controls;
  }

  async aceptar(){
    await this.spinner.show();
    this.intentsCell =  parseInt(this.intentsCell) + 1;
    sessionStorage.setItem('intentsCell', this.intentsCell);
    if(this.myForm.valid){
      console.log(">>> : " + this.f.celular.value); 
      const object = this.session.getObjectSession();
      object.telefono = true;
      this.session.updateModel(object);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("telefono", device, this.intentsCell, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
      await this.middle.updateDataUser({telefono:this.f.celular.value}, this.id);
      console.log('ya termine con la telefono' + JSON.stringify(object, null, 2));
      this.router.navigate([this.workFlowService.redirectFlow('telefono',this.object.servicios)]);   
    }
    await this.spinner.hide();
  }

  public getColorButton(){

    return this.term.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.term.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.term.getImageConfig();
  }
  
}

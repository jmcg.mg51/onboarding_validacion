
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service.js';
import { ServicesGeneralService, isMobile, isIphone } from '../../../../services/general/services-general.service';
import { Rutas } from 'src/app/model/RutasUtil.js';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../../model/sesion/SessionPojo';
import {NgxImageCompressService} from 'ngx-image-compress';
import { WorkFlowService } from '../../../../services/session/work-flow.service';
import {GlobalInstructionComponent} from '../../../global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';

// TODO verificar las rutas de los archivos
@Component({
  selector: 'app-page-face-capture',
  templateUrl: './page-face-capture.component.html',
  styleUrls: ['./page-face-capture.component.css']
})
export class PageFaceCaptureComponent implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  ctx: CanvasRenderingContext2D;
  isMobileBool: boolean;
  isIphone: boolean;
  activator = true;
  imageGreen: any;
  mensaje: string;
  isEdge: boolean;
  imageData: any;
  btnB: boolean;
  videoEl: any;
  id: string;
  img: any;
  fc: any;
  object: sesionModel;
  cameraDenied: boolean; 
  cameraAcess: boolean;
  cameraNotSupport: boolean;
  intentsSelfie;
  showSkipButton: boolean;

  //MARK: messages of error 
  notFace = "No face could be found in the image"; 
  notEyes = "Could not find the eyes in the image";
  faceSmall = "Face too small. Please move closer to camera";
  FaceFrontCamera = "Face is tilted. Please look directly at the camera";
  imageDark = "Image too dark. Please improve lighting";
  imageBlur = "Image blur detected. Reduce motion or improve lighting";
  multipleFaces = "Multiple faces found in the image";
  faceLigth = "Face lighting not uniform";
  faceDont = "Face image too soft, lacking details. Please improve lighting";
  faceNotFrontal = "Face not frontal. Please look directly at the camera";
  closedEyes = "The eyes in the image appear to be closed";
  imageQuality = "Face image not of sufficient quality";
  noCara = "No se ha encontrado ninguna cara.";
  caraPequena = "Cara demasiado pequeña, acérquese a la cámara.";
  caraIluminacion = "Imagen demasiado oscura. Por favor, mejore la iluminación.";
  noOjos = "No se han podido encontrar ojos en la imagen.";
  caraladeada = "La cara esta ladeada. Por favor, mire directamente a la cámara.";
  caraladeada2 = "La cara esta ladeada.Por favor, mire directamente a la cámara.";
  errorFetch = ":type:error filed to fetch";


  constructor(private workFlowService :WorkFlowService, private router: Router, private session: SessionService, private actRoute: ActivatedRoute,
              private serviciogeneralService: ServicesGeneralService, private imageCompress: NgxImageCompressService,
              private global: GlobalInstructionComponent,  private analyticService: AnaliticService,
              private mongoMid: MiddleMongoService) {
    this.htmlCanvasToBlob();
    

  }
  myVar;
  async ngOnInit() {
    this.showSkipButton = false; 
    if(sessionStorage.getItem('intentsSelfie')){
      this.intentsSelfie = sessionStorage.getItem('intentsSelfie');
    }else{
      sessionStorage.setItem('intentsSelfie', '0')
      this.intentsSelfie = sessionStorage.getItem('intentsSelfie'); 
    }
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    this.isMobileBool = isMobile(navigator.userAgent);
    this.isIphone = isIphone(navigator.userAgent);
    this.isEdge = window.navigator.userAgent.indexOf('Edge') > -1;
    if (this.isEdge) {
      this.drawOutline(document.getElementById('scream'));
    }
    this.btnB = true;
    
    this.imageData = '';
    this.videoEl = document.querySelector('video');
    var i =5;
    this.myVar = setInterval(() => {
      //console.log("i= " + i);
      this.mensaje = 'Espere por favor';
      //console.log(this.mensaje);
      
      if(i===0){
        this.mensaje = 'Posiciona tu cara dentro del área marcada';
        clearInterval(this.myVar);
      }
      i=i-1;
    }, 1000);
     
    // if(this.fc.startCamera(this.videoEl, 1)){
      console.log("TRUE EN CAMAERA COMPONENT");
      this.fc.startCamera(this.videoEl).then((response) => {
       // this.cameraDenied = false; 
        this.cameraAcess = true; 
  
        console.log("respose=");
        console.log(response);
        this.onCameraStarted(this.fc, this.videoEl);
  
        setTimeout(() => {
          console.log('sleep');
          this.tomarSelfie();
        }, 5000);
  
      }).catch(err => {
        this.abrirModal();
        //this.cameraDenied = true; 
        this.cameraAcess = false; 
        console.log(err);
      });
    // }else{
    //   console.log("Error EN COMPONENT CAMERA");
    //   this.cameraNotSupport = true;

    // }
    
    /** 
    this.fc.startCamera(this.videoEl).then((response) => {
      this.cameraDenied = false; 
      this.cameraAcess = true; 

      console.log("respose=");
      console.log(response);
      this.onCameraStarted(this.fc, this.videoEl);

      setTimeout(() => {
        console.log('sleep');
        this.tomarSelfie();
      }, 5000);

    }).catch(err => {
      this.cameraDenied = true; 
      this.cameraAcess = false; 
      console.log(err);
    });
    */
    
    /*navigator.mediaDevices.getUserMedia().then(function(mediaStream) {
    }).catch(function(err) {
      alert("Debes permitir el acceso a tu cámara");
    });*/

  }
  
  

  async alredySessionExist() {
    console.log('');
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.selfie===true){
        this.router.navigate([this.workFlowService.redirectFlow('selfie',this.object.servicios)]);
        return false;
      } else {
        return true;
      }
    }
  }

  captura() {
    console.log();
    this.fc.stopAutoCapture();
  }

  onCameraStarted = (fc, video) => {
    video.onloadedmetadata = () => {
      fc.startFaceDetector({
        // urlFaceDetectorWasm: window.location.origin + '/assets/js/DaonFaceQualityLite.wasm',
        onFaceDetectorInitialized: () => {
          fc.findFace();
        },
        onFaceDetectorError: (err) => {
          console.error('DEMO FaceDetector error', err);
        },
        onFaceDetection: coords => {
          if (coords) {

          } else {
          }
        }
      });
    };
  }

  returnId() {
    return this.id;
  }

  tomarSelfie() {
    this.btnB = false;
    this.fc.startAutoCapture(response => {
      if (response.result === 'FAIL') {
        this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
      sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
      
      if(this.intentsSelfie % 20 == 0){
        if(sessionStorage.getItem("skipStep")){
          this.showSkipButton = true;
        }
      }

         this.mensaje = response.feedback;
        //  switch (response.feedback) {
        //   case this.notFace:
        //     this.mensaje = "Ajusta tu rostro dentro del área marcada.";
        //     break;
        //   case this.noOjos:
        //     this.mensaje = "Ajusta tu rostro dentro del área marcada.";
        //     break;
        //   case this.noCara:
        //     this.mensaje = "Ajusta tu rostro dentro del área marcada.";
        //     break;
        //   case this.caraPequena:
        //       this.mensaje = "Acércate más a la cámara. ";
        //       break;
        //   case this.caraIluminacion:
        //     this.mensaje = "Imagen muy oscura. Por favor mejore la iluminación";
        //     break;
        //   case this.notEyes:
        //     this.mensaje = "Acércate más a la cámara";
        //     break;
        //   case this.faceSmall:
        //     this.mensaje = "Acércate más a la cámara.";
        //     break;
        //   case this.FaceFrontCamera:
        //     this.mensaje = "La cara está inclinada. Por favor mira directamente a la cámara.";
        //     break;
        //   case this.caraladeada:
        //     this.mensaje = "La cara está inclinada. Por favor mira directamente a la cámara.";
        //     break;
        //   case this.caraladeada2:
        //     this.mensaje = "La cara está inclinada. Por favor mira directamente a la cámara.";
        //     break;
        //   case this.imageDark:
        //     this.mensaje = "Imagen muy oscura. Por favor mejore la iluminación";
        //     break;
        //   case this.imageBlur:
        //     this.mensaje = "Imagen borrosa detectada. Reduce el movimiento o mejora la iluminación.";
        //     break;
        //   case this.multipleFaces:
        //     this.mensaje = "Múltiples caras encontradas en la imagen.";
        //     break;
        //   case this.faceLigth:
        //     this.mensaje = "Busca que la iluminación de tu cara sea uniforme.";
        //     break;
        //   case this.faceDont:
        //     this.mensaje = "Cara borrosa. Ubícate en un lugar mejor iluminado.";
        //     break;
        //   case this.faceNotFrontal:
        //     this.mensaje = "Mira de frente a la cámara.";
        //     break;
        //   case this.closedEyes:
        //     this.mensaje = "Tus ojos parecen estar cerrados.";
        //     break;
        //   case this.imageQuality:
        //     this.mensaje = "La imagen de la cara no tiene la calidad suficiente. .";
        //     break;
        //   case this.errorFetch:
        //     this.mensaje = "Error al capturar la imagén";
        //     break;
        //   default:
        //     this.mensaje = "";
        //     break;
        // }
         console.log("responseF",response);
        //this.mensaje = 'No se ha podido encontrar tu rostro';
        console.log('no pasa, cai en el fail');
      } else if (response.result === 'PASS') {
        this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
        sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
        console.log("response",response);
        this.serviciogeneralService.setIsCamNative(false);
        console.log('si pasa' + JSON.stringify(response, null, 2));
        this.mensaje = response.feedback;
        this.fc.stopAutoCapture();
        this.fc.stopCamera();
        this.imageData = response.sentBlobImage;
        this.serviciogeneralService.setImg64(this.imageData);
        this.router.navigate([Rutas.selfieVerification]);
        this.activator = false;
      }
    },
      (error) => {
        console.log('error durante la captura');
        let valueMensjae = this.mensaje;
        if(!valueMensjae || valueMensjae === '' || valueMensjae.includes('obscuro')) {
          this.mensaje = 'Acércate';
        } else  {
          this.mensaje = 'Asegúrate que haya fondo obscuro atrás de ti';
        }
        console.log(error);
        this.btnB = true;
        // this.tomarSelfie();
      });
  }

  imprimirImagen() {
    return this.imageData;
  }
  

  async skipStep(){
    //Update de análitica
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("selfie", device, this.intentsSelfie, "NO", true);
    console.log("El valor actual de analytics es:", analytics);
    await this.mongoMid.updateDataUser(analytics, this.id);
    const smartSkip = {
      selfie: true
    }
    let result = await this.mongoMid.updateDataUser({ smartSkip: smartSkip }, this.id);
    console.log("result smartSkip", result);
    sessionStorage.setItem("smartSkipSelfie", "true");
    //Saltar nodo
    this.fc.stopAutoCapture();
    this.fc.stopCamera();
    this.router.navigate([this.workFlowService.redirectFlow('selfie',this.object.servicios)]);



  }


  drawOutline(img) {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    const scale = 0.48;
    const dx = (this.canvas.nativeElement.width - img.width * scale) / 2;
    const dy = (this.canvas.nativeElement.height - img.height * scale * 3.5) / 2;
    this.ctx.globalAlpha = 0.7;
    this.ctx.drawImage(img, dx, dy,
      img.width * scale,
      img.height * scale * 3.5);
  }

  htmlCanvasToBlob() {
    if (!HTMLCanvasElement.prototype.toBlob) {
      console.log('HTMLCanvasElement.prototype.toBlob 1 ' + HTMLCanvasElement.prototype.toBlob);
      Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
        value: function (callback, type, quality) {
          var canvas = this;
          setTimeout(function () {
            var binStr = atob(canvas.toDataURL(type, quality).split(',')[1]),
              len = binStr.length,
              arr = new Uint8Array(len);

            for (var i = 0; i < len; i++) {
              arr[i] = binStr.charCodeAt(i);
            }
            callback(new Blob([arr], { type: type || 'image/png' }));
          });
        }
      });
    }
  }

  processFile(imageInput: any) {
    this.mensaje = '';
    const file: File = imageInput.files[0];
    console.log(file.type);
    if (file.type.toUpperCase().includes('JPG'.toUpperCase()) || file.type.toUpperCase().includes('JPEG'.toUpperCase())) {
      this.fc.assessQuality(file)
        .then(response => {
          if (response.result === 'FAIL') {
            this.mensaje = response.feedback;
            console.log('no pasa');
          } else if (response.result === 'PASS') {
            console.log('si pasa' + JSON.stringify(response, null, 2));
            this.mensaje = response.feedback;
            this.fc.stopAutoCapture();
            this.fc.stopCamera();
            this.imageData = response.sentBlobImage;
            this.serviciogeneralService.setImg64(this.imageData);
            this.router.navigate([Rutas.selfieVerification]);
            this.activator = false;
          }
        })
        .catch(err => {
          this.mensaje = 'No es una fotografia valida';
          console.log(err);
          this.btnB = true;
        });
    } else {
      this.mensaje = 'La extencion ' + file.type.substr(6) + ' es incorrecta';
    }
  }

  blobToBase64(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      try {
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
          resolve(reader.result.toString().replace('data:image/jpeg;base64,', ''));
        };
      } catch (err) {
        reject(err);
      }
    });
  }

  async cerrarModal() {
    const modal = document.getElementById('modalPermission');
    modal.style.display = 'none';
    this.router.navigate([Rutas.instrucciones]);
  }
  
  async abrirModal() {
    console.log("abrir modal");
    const modal = document.getElementById('modalPermission');
    modal.style.display = 'block';
  }

  reload(){
    console.log("permisos");
    location.reload();
  }

  stopCameraJs(){
    var stream = navigator.mediaDevices.getUserMedia({video: true}).then(mediaStream => {
      document.querySelector('video').srcObject = mediaStream;
      // Stop the stream after 5 seconds
      setTimeout(() => {
        const tracks = mediaStream.getTracks();
        console.log("los tracks son", tracks);
        tracks[0].stop()
      }, .001);

    });
  }

  async abrirModalSkip() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'block';
  }

  async cerrarModalSkip() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'none';
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { VirtualTimeScheduler } from 'rxjs';
import { each } from 'jquery';

@Component({
  selector: 'app-archivo-adjunto',
  templateUrl: './archivo-adjunto.component.html',
  styleUrls: ['./archivo-adjunto.component.css']
})
export class ArchivoAdjuntoComponent implements OnInit {
 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }

  errorGeneral: boolean;
  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  showMessageError = false;
  showMessageErrorFormat = false;
  regexPDF = '^(a-zA-Z0-9\sáéíóúÁÉÍÓÚ)';
  regexName = '/-/g'

  //MARK - Variables
  intentsArchivo;
  datos = [];

  async ngOnInit() {
    this.spinner.show;
  
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    this.getDatos();
    if(sessionStorage.getItem('intentsArchivoAdjunto')){
      this.intentsArchivo = sessionStorage.getItem('intentsArchivoAdjunto');
    }else{
      sessionStorage.setItem('intentsArchivoAdjunto', '0') 
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("archivosAdjuntos", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsArchivo = sessionStorage.getItem('intentsArchivoAdjunto');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
        
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    console.log("el objeto", this.object);
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.archivosAdjuntos===true){
        this.router.navigate([this.workFlowService.redirectFlow('archivosAdjuntos',this.object.servicios)]); 
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsArchivo =  parseInt(this.intentsArchivo) + 1;
    sessionStorage.setItem('intentsArchivoAdjunto', this.intentsArchivo);
    var datos = this.object.servicios['archivosAdjuntos']['propiedades'];
    const requestToSend = [];

    let arrayFiles = $('input[type="file"]');

    for(var i = 0; i < arrayFiles.length; i ++){
      console.log("arrayFiles", arrayFiles[i].id);
      let name = (arrayFiles[i].id).replace('fileTerminos-','');
      let file = (arrayFiles[i] as HTMLInputElement).files[0];
      if(!file){
        //show error
        this.errorGeneral = true; 
        this.spinner.hide();
        return;
      }
      this.errorGeneral = false; 
      const fileBase64 = await this.convertBase64(file);
      var infoFile = {
        name: name,
        content: fileBase64
      }
      requestToSend.push(infoFile);
    }
    console.log("requestToSend", requestToSend);

    this.saveArchivos(requestToSend);

  }

  convertBase64(file){
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      try {
        reader.readAsDataURL(file);
        reader.onloadend = () => {
          resolve(reader.result);
        };
      } catch (err) {
        reject(err);
      }
    });
  }



  async saveArchivos(archivos) {
    this.showMessageError = false;
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("archivosAdjuntos", device, this.intentsArchivo, "NO", false);
    console.log("El valor actual de analytics es:", analytics);

    let response = await this.middle.updateDataUser({archivos}, this.id);
    if (response !== 'OK') {
      this.showMessageError = true;
      this.spinner.hide();
      return;
    }
    await this.middle.updateDataUser(analytics, this.id);
    const object = this.session.getObjectSession();
    object.archivosAdjuntos = true;
    this.session.updateModel(object);
    this.continue();
    
  }

  getDatos(){
    console.log(this.object.servicios);
    let name; 
    //this.object = this.session.getObjectSession();
    if(this.object.servicios['archivosAdjuntos']['propiedades']){
      for(var i = 0; i < this.object.servicios['archivosAdjuntos']['propiedades'].length; i++){
        console.log("archivo",this.object.servicios['archivosAdjuntos']['propiedades'][i]);
        name = this.object.servicios['archivosAdjuntos']['propiedades'][i];
        const nameCleaned = name.replace(/\s/g, '-');
        console.log("nombre reemplazado",nameCleaned);
        this.datos.push({id:nameCleaned, name:name}) 
       // this.datos.push(nameCleaned);
      }
   // this.datos = this.object.servicios['archivosAdjuntos']['propiedades'];
    console.log("los datos son",this.datos);
      
    }
    
  }


  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('archivosAdjuntos',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  async processFile(id, file){
    this.showMessageErrorFormat = false;
    console.log("id", id);
    console.log("file", file);
    var idData = "#fileTerminos-"+id;
    let myFile = $(idData).prop('files')[0];
    let nameInput = "name-file-"+id;
    if(!myFile) {
      document.getElementById(nameInput).innerHTML= 'Elige un archivo';
      return;
    }
    if( !myFile.name.toString().toUpperCase().includes('.PDF')) {
      document.getElementById(nameInput).innerHTML= 'Elige un archivo';
      this.showMessageErrorFormat = true;
      return;
    }
    console.log("nameInput", nameInput);
    document.getElementById(nameInput).innerHTML= myFile.name;
    console.log("myFile", myFile);


  }

  uploadFile(){
    var classArchivo = ".";
    console.log("cambio el input", $('.input-file'));
    $(classArchivo).each(function() {
      var $input = $(this),
          $label = $input.next('.js-labelFile'),
          labelVal = $label.html();
      
     $input.on('change', function(element) {
      var valueElement  = element.target as HTMLInputElement;
      console.log("El nombre del archivo es", valueElement);
      var ext = valueElement.value.match(/\.([^\.]+)$/)[1];
        switch (ext) {
          case 'pdf':
            if (valueElement.value) fileName = valueElement.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
            break;
          default:
            var fileName = '';
        } 
        
     });
    });
  }


  showFilesInputs(){

  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

}
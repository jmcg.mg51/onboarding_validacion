import { Component, NgZone, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from '../../model/sesion/SessionPojo';
import { SessionService } from '../../services/session/session.service';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { isBrowserOk } from '../../services/general/services-general.service';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesGeneralService, isMobile } from '../../services/general/services-general.service';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { browser } from 'protractor';

declare const deviceInfo: any;
@Component({
  selector: 'app-global-instruction',
  templateUrl: './global-instruction.component.html',
  styleUrls: ['./global-instruction.component.css']
})
export class GlobalInstructionComponent implements OnInit {
 
  constructor(public router: Router, private actRoute: ActivatedRoute, public ngZone: NgZone,
    private session: SessionService,private workFlowService :WorkFlowService, private middle: MiddleMongoService,
    private spinner: NgxSpinnerService, private analiticService: AnaliticService) { }

  object: sesionModel;
  id: any;
  type: string;
  merchantCode: string;
  colorButtons: string;
  colorLabels: string; 
  configColorButton: string; 
  configColorLabel: string;
  defaultColorButton: "black";
  defaultColorLabel: "white";
  objectOffer;
  imageService: string;
  imageConfig: string; 
  copiar:string = '      ';
  url;
  isMobileBool: boolean;
  sessionGetAlert: string;
  textTerminosFico;
  nivelFicoScore;
  gpsService: boolean;
  showGPS: boolean;
  responseGPS;

  async ngOnInit() {
    await this.spinner.show();
    var responseNavigator = this.validateDevive();
    this.isMobileBool = isMobile(navigator.userAgent);
    this.sessionGetAlert = sessionStorage.getItem('alertBrowser');
    sessionStorage.setItem("urlTerms", window.location.href);

   

  if(responseNavigator){
    console.log('entre al response navigator');
    this.actRoute.params.subscribe(params => {
      this.id = params['id'];
    });

    if(this.actRoute.snapshot.queryParamMap.get('ti')){ 
      sessionStorage.setItem('ti', JSON.stringify(this.actRoute.snapshot.queryParamMap.get('ti')));
    }
    var baseUrl = document.location.origin;
    this.url = baseUrl + '#' + Rutas.globalInstruction + `${this.id}`;  

    // if(this.isMobileBool && this.sessionGetAlert !== 'true' ){
    //   this.abrirModal();
    // }else{
    // }


    await this.saveSessionStorage();
    //await this.spinner.hide();
    console.log("el valor de gps es", this.gpsService);
    if(this.gpsService === true){
      console.log("el servicio de GPS esta activo");
      await this.getLocationByGPS();

    }else{
      console.log("el servicio de GPS NO esta activo");
     await this.alredySessionExist();
    }
    
  //  await this.alredySessionExist();
    this.loadScript();
    this.spinner.hide();
    

  }else{
    this.router.navigate([Rutas.deviceConnection]);
  }
    
    if(!isBrowserOk(navigator.userAgent)){
      document.write("<p><b>Esta aplicación no es compatible este navegador<\/b>");
      document.write("<p><b>"+navigator.userAgent+"<\/b>");
      return
    }
    

    
  }

  async alredySessionExist() {
    
    this.object = this.session.getObjectSession();
    
      if (this.object !== undefined && this.object){
        if(this.object.correo===true || this.object.datosFiscales===true || this.object.telefono===true || this.object.daon.selfie===true ){
          this.session.updateModel(this.object);
          this.ngZone.run(() => this.router.navigate([this.workFlowService.redirectFlow('telefono',this.object.servicios)])).then();
        }else{ 
          this.session.updateModel(this.object);
          this.ngZone.run(() => this.router.navigate([this.workFlowService.redirectFlow('globalInstruction',this.object.servicios)])).then();
        }
      }else{
        this.router.navigate([Rutas.error]);
      }


  }

  async saveSessionStorage() {
    var dataUserResponse = await this.middle.getDataUser(this.id);
    console.log("saveSessionStorage", dataUserResponse);
    if(dataUserResponse !== undefined) {
      await this.getOffer(dataUserResponse["nombreOferta"], dataUserResponse["merchantId"]);
      this.workFlowRFC(dataUserResponse);
      //console.log("la oferta es:", offer);
      dataUserResponse._id = this.id;
      console.log('el data user response es: ' , dataUserResponse);
      await this.session.updateModel(dataUserResponse);
      if(dataUserResponse.code)
        sessionStorage.setItem('emailCode', dataUserResponse.code);
      if(dataUserResponse.tipoPersona && dataUserResponse.tipoPersona.tipoPersona) {
        if(dataUserResponse.tipoPersona.tipoPersona == 'fisica') {
          sessionStorage.setItem('typePerson', '1');
        } else {
          sessionStorage.setItem('typePerson', '2');
        }
      }
    } else {
      this.spinner.hide();
      this.router.navigate([Rutas.error]);
    }
  }

  

  async getLocationByGPS(){
    if (navigator.geolocation) {

     // navigator.permissions.revoke({name:'geolocation'}).then(function(result) {
     // });
     this.responseGPS = await navigator.geolocation.getCurrentPosition(async (position) => {
        const gps = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
      };
        console.log("la ubiación es:", gps);
        this.showGPS = true;
        let response = await this.saveData(gps);
        if(response == "OK"){
          await this.alredySessionExist();
        }
      }, (err) => {
        console.log('ERROR LOCALIZACION (' + err.code + '): ' + err.message);
        this.showGPS = true;
      }, {timeout:5000});
      
     }else{
       this.responseGPS = "error";
     }
    
  }


  async saveData(gps){
    console.log("Entre a guardar la información");
    let responseUpdate = await this.middle.updateDataUser({gps}, this.id);
    console.log("response saveData", responseUpdate);
    return responseUpdate;
  }

  public loadScript() {
    let body = <HTMLDivElement> document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/SecureOnBoarding.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
}

async getOffer(nombreOferta:string, merchantId: string){
 console.log('track y oferta : ' , nombreOferta, merchantId);
  this.objectOffer = await this.middle.getInfoOferta(nombreOferta, merchantId);
  console.log("la oferta es:", this.objectOffer);

  if(this.objectOffer && this.objectOffer.servicios && this.objectOffer.servicios.oferta && this.objectOffer.servicios.oferta.includes('Bot')) {
    console.log('si tengo bot');
    sessionStorage.setItem('bot', 'true');
  } else {
    console.log('no tengo bot :(');
  }

  this.configColorButton = this.objectOffer["configuration"] !== undefined?
    this.objectOffer["configuration"].colorButton : this.defaultColorButton;

  this.configColorLabel = this.objectOffer["configuration"] !== undefined?
    this.objectOffer["configuration"].colorLabel : this.defaultColorLabel;
  this.imageService = this.objectOffer["urlIcon"];

  if(this.objectOffer["documentoTerminos"]){
    this.textTerminosFico = this.objectOffer["documentoTerminos"];
    sessionStorage.setItem("textoTerminos", this.textTerminosFico);
  }

  if(this.objectOffer["nivelFicoScore"]){
    this.nivelFicoScore = this.objectOffer["nivelFicoScore"];
    sessionStorage.setItem("nivelFicoScore", this.nivelFicoScore);
  }

  if(this.objectOffer["nombreCliente"]){
    var nombreComercio = this.objectOffer["nombreCliente"];
    sessionStorage.setItem("nombreComercio", nombreComercio);
  }

  if(this.objectOffer["servicios"]){
    const servicios = this.objectOffer["servicios"];
    var propiedades = servicios.oferta;
    console.log("propiedades if servicios", propiedades);
    var identificaciones = servicios.propiedades;
    for(var i = 0; i<propiedades.length ; i++){
      console.log("PROPIEDADES", propiedades[i]);
      if(propiedades[i] === "Skip"){
        sessionStorage.setItem("skipStep", "true");
      }
      if(propiedades[i] === "Gps"){
        console.log("GPS IS ACTIVE");
        this.gpsService = true; 
        console.log("GPS IS ACTIVE VALOR", this.gpsService);
      }

      if(propiedades[i] === "Selfie"){
        sessionStorage.setItem("Selfie", "true");
      }
      if(propiedades[i].toUpperCase() === "ENROLL"){
        sessionStorage.setItem("Enroll", "true");
      }
      // if(propiedades[i] === "Prueba De Vida"){
      //   sessionStorage.setItem("PDV", "true");
      // }
      if(propiedades[i] === "Documento"){
        sessionStorage.setItem("Documento", "true");

        console.log("el numero de documentos seleccionados es:", identificaciones.length);
        for(var k = 0; k < identificaciones.length; k ++){
          if(identificaciones[k] === "CURP"){
            identificaciones.length = identificaciones.length - 1;
          }
          
        }
        //this.type.includes('INE') && this.type.includes('PASSPORT')
        console.log("documentos:",identificaciones[0] );
        if(identificaciones[0].includes('INE') && identificaciones[0].includes('PASAPORTE')){
          console.log("no hago nada");
        }else{
          console.log("numero de identificaciones:", identificaciones.length );
          if(identificaciones.length < 2){
            for(var j = 0; j < identificaciones.length; j ++){
              if(identificaciones[j] === "INE"){
                sessionStorage.setItem("serviceId", 'VID')
              }else{
                sessionStorage.setItem("serviceId", 'PP');
              }
            }
          // propiedades[i].propiedades === "INE" ? sessionStorage.setItem("serviceId", 'VID') : sessionStorage.setItem("serviceId", 'PP');
          }
        }
        
      }

      if(propiedades[i] === "Rfc"){
        console.log("RFC propiedades", propiedades[i]);
      }
      
    }

    
  }

  this.setServiceId();
  sessionStorage.setItem("colorButton", this.configColorButton);
  sessionStorage.setItem("colorLabel", this.configColorLabel);
  sessionStorage.setItem("imageConfig", this.imageService);
  
}

reload(){
  location.reload();
}

public getButtonColorSystem(){
  this.colorButtons = sessionStorage.getItem("colorButton");
  

  return this.colorButtons
}

public getLabelColorSystem(){
  this.colorLabels = sessionStorage.getItem("colorLabel");

  return this.colorLabels;
}

public getImageConfig(){
  this.imageConfig = sessionStorage.getItem("imageConfig");
  return this.imageConfig;

}

validateDevive(){
  let device = deviceInfo();
  switch (device['os.name']) {
    case 'Macintosh':
      return true; 
      break;
    case 'Windows':
      return true; 
      break;
    case 'Android':
      if(device['browser.name'] === "Firefox" || device['browser.name'] === "Chrome" || device['browser.name'] === "Opera" 
      || device['browser.name'] === "Safari" ){
        if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
          return true; 
        }else{
          return false;
        }
        
      }else{
        return false; 
      }
      break;
    case 'iPad':
      if( device['browser.name'] === "Safari" ){
        if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
          return true; 
        }else{
          return false;
        }
      }else{
        //Redirect a página de error
        return false;
      }
      break;
    case 'iPhone':
      if( device['browser.name'] === "Safari" ){
        console.log("media device", navigator);
        if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
          return true; 
        }else{
          /** CAMBIAR A FALSE */
          return false;
        }
      }else{
        return false; 
      }
      break;
    default:
      return false; 
      break;
  }



}

copyLink(){
  var aux = document.createElement("input");
  var value = $("#link").val().toString();
  aux.setAttribute("value", value);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  this.copiar="Copiado!";
  
  setTimeout(() => {
    this.copiar=" ";
  }, 2000);
}


async abrirModal() {
  const modal = document.getElementById('modalAlertBrowser');
  modal.style.display = 'block';
  sessionStorage.setItem('alertBrowser', 'true');
}
async cerrarModal() {
  const modal = document.getElementById('modalAlertBrowser');
  modal.style.display = 'none';

  this.spinner.show();
    await this.saveSessionStorage()
    await this.alredySessionExist();
    this.loadScript();
    this.spinner.hide();

}

async setServiceId() {
  if(sessionStorage.getItem("Documento") && sessionStorage.getItem("Selfie") && sessionStorage.getItem("Enroll") ){
    console.log("entro a selfie , documento y enroll");
    sessionStorage.setItem("serviceNumber", '50');
    sessionStorage.setItem('serviceIdMissionName','Enroll');
    return;
  }
  if(sessionStorage.getItem("Documento") && sessionStorage.getItem("Selfie")){
    console.log("entro a selfie y documento");
    sessionStorage.setItem("serviceNumber", '10');
    sessionStorage.setItem('serviceIdMissionName','Validate-id-match-face');
    return;
  }

  if(sessionStorage.getItem("Documento")){
    sessionStorage.setItem("serviceNumber", '20');
    sessionStorage.setItem('serviceIdMissionName','Validate-id');
    return; 
  }

}

async workFlowRFC(offer){
  if(offer.servicios && offer.servicios.hasOwnProperty('rfc')){
    console.log("SI TENGO RFC");
    if(offer.servicios.rfc.hasOwnProperty('propiedades')){
      console.log("SI TENGO PROPIEDADES DEL RFC");
      let propiedadesRFC = offer.servicios.rfc.propiedades
      console.log("propiedes del RFC", propiedadesRFC);
      for(var r = 0; r < propiedadesRFC.length; r++){
          if(propiedadesRFC[r] === "Fisica"){
            sessionStorage.setItem("havePhysicalRFC", "true");
          }
          if(propiedadesRFC[r] === "Moral"){
            sessionStorage.setItem("haveMoralRFC", "true");
          }
      }
    }  
  }
}

}

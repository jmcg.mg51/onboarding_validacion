import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../../environments/environment';
import {GlobalInstructionComponent} from '../../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-autorizacion',
  templateUrl: './autorizacion.component.html',
  styleUrls: ['./autorizacion.component.css']
})
export class AutorizacionComponent implements OnInit {

    //Mark: variables of components
    nip;
    aceptarCheck: boolean;
    errorGeneral: boolean;
    chosenYearDate: Date;
    btnContinuar: boolean;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsAnalisisFinancieroAutorizacion;
  nombreOtorgante: string;

  async ngOnInit() {
    this.spinner.show;
    this.errorGeneral = false;
    this.btnContinuar = true;

    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    console.log('this.object: ' , this.object);
    if(sessionStorage.getItem('intentsReporteCrediticio')){
      this.intentsAnalisisFinancieroAutorizacion = sessionStorage.getItem('intentsReporteCrediticio');
    }else{
      sessionStorage.setItem('intentsReporteCrediticio', '0');
      console.log("El valor de intents es:",sessionStorage.getItem('intentsReporteCrediticio'));
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("reporteCrediticio", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsAnalisisFinancieroAutorizacion = sessionStorage.getItem('intentsReporteCrediticio');
    }
    if(sessionStorage.getItem("nombreComercio")){
      this.nombreOtorgante = sessionStorage.getItem("nombreComercio");
    }else{
      this.nombreOtorgante = "(Nombre del otorgante del credito)";
    }
    const fp = await FP.load({token: environment.fingerJsToken, endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    console.log('already: ' , this.object);
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    }
    return true;
    // else {
    //   if(this.object.reporteCrediticio===true){
    //     this.router.navigate([this.workFlowService.redirectFlow('reporteCrediticio',this.object.servicios)]);   
    //     return false;
    //   } else {
    //     return true;
    //   }
    // }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsAnalisisFinancieroAutorizacion =  parseInt(this.intentsAnalisisFinancieroAutorizacion) + 1;
    sessionStorage.setItem('intentsReporteCrediticio', this.intentsAnalisisFinancieroAutorizacion);
    this.nip = $("#nipInput").val();

    if(this.nip === "" || this.aceptarCheck === false){
      this.errorGeneral = true; 
      this.spinner.hide();
    }else{
      const autorizacion = {
        acepto: this.aceptarCheck,
        nip: this.nip
      }

      console.log(autorizacion);
      await this.saveAutorizacion(autorizacion);
      this.spinner.hide();
    }

    
    //this.CurpValidate(this.curpInput);
  }



  async saveAutorizacion(autorizacion){

    const result = await this.middle.validaCodigoPIN(this.id, autorizacion.nip);
    if(result === 200){
      this.errorGeneral = false;
      sessionStorage.setItem('reporteCrediticio', "si");
    //sessionStorage.setItem('patron', this.razonPatron);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("reporteCrediticio", device, this.intentsAnalisisFinancieroAutorizacion, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
      await this.middle.updateDataUser({autorizacion}, this.id);

      const object = this.session.getObjectSession();
      object.analisisFinancieroAutorizacion = true;
      this.session.updateModel(object);

      this.continue();
    }else{
      this.errorGeneral = true;
      return;
    }
    
    

    
  }

  ckeckChangeStatus(){
    var check = document.getElementById("changeStatus") as HTMLInputElement;
    if(check.checked){
      this.aceptarCheck = true;
      this.btnContinuar = false;
    }else{
      this.aceptarCheck = false; 
      this.btnContinuar = true;
    }
  }

  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('reporteCrediticio',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
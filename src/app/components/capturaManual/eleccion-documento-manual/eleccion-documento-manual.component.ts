import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesGeneralService, isMobile } from '../../../services/general/services-general.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { environment } from '../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import { GlobalInstructionComponent } from '../../global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';


@Component({
  selector: 'app-eleccion-documento-manual',
  templateUrl: './eleccion-documento-manual.component.html',
  styleUrls: ['./eleccion-documento-manual.component.css']
})

export class EleccionDocumentoManualComponent implements OnInit {

  constructor(private workFlowService: WorkFlowService, private serviciogeneralService: ServicesGeneralService, private spinner: NgxSpinnerService, public router: Router, private session: SessionService, public servicesGeneralService: ServicesGeneralService,
    private actRoute: ActivatedRoute, private global: GlobalInstructionComponent,
    private analyticService: AnaliticService, private middle: MiddleMongoService) { }

  filtersLoaded: Promise<boolean>;
  typeIdentity: string;
  error: string;
  id: string;
  showINE: boolean;
  showPass: boolean;
  type: string;
  object: sesionModel;
  isMobileBool: boolean;
  sessionGetAlert: string;
  url;
  copiar: string = '      ';
  intentsDocument;

  async ngOnInit() {
      await this.spinner.show();
      if (!(await this.alredySessionExist())) { return; }
      console.log("obj= ", this.object);
      this.typesIdentity();
      this.id = this.object._id;

      if (sessionStorage.getItem('intentsDocument')) {
        this.intentsDocument = sessionStorage.getItem('intentsDocument');
      } else {
        sessionStorage.setItem('intentsDocument', '0');
        const device = this.analyticService.getTypeDevice();
        const initAnalytics = this.analyticService.initAnalytics('documento', device)
        console.log("El id es:", this.id);
        console.log("Lo que voy a guardar es:", initAnalytics);
        var result = await this.middle.updateDataUser(initAnalytics, this.id);
        console.log("El resultado es:", result);
        this.intentsDocument = sessionStorage.getItem('intentsDocument');
      }
      var baseUrl = document.location.origin;
      console.log(baseUrl);
      console.log(this.id);
      this.url = baseUrl + '#' + Rutas.globalInstruction + `${this.id}`;

      const fp = await FP.load({ token: environment.fingerJsToken, 
 endpoint: environment.fpDomain });
      fp.get({ tag: { 'tag': this.id } });
      this.error = sessionStorage.getItem('errorDocument');
      this.filtersLoaded = Promise.resolve(true);
      $("#INEOption").trigger("click");

      await this.spinner.hide();

      $("#INEOption").hover(function () {
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        var PassaportOption = document.getElementById("PassaportOption") as HTMLInputElement;
        PassaportOption.style.backgroundColor = "white";
        PassaportOption.style.color = "#6c757d";
      });

      $("#PassaportOption").hover(function () {
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        var INEtOption = document.getElementById("INEOption") as HTMLInputElement;
        INEtOption.style.backgroundColor = "white";
        INEtOption.style.color = "#6c757d";

      });

  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      return true;
    }
  }

  sendDoc(ti_) {
    if (ti_ === '') {
      this.error = 'Seleccione un tipo de documento';
    }
    this.typeIdentity = this.servicesGeneralService.INE;
    if(ti_ === 'PASSPORT' ) {
      this.typeIdentity = this.servicesGeneralService.PASSPORT;
    }
    console.log('ti= ' + this.typeIdentity);
    this.servicesGeneralService.setTipoIdentificacion(this.typeIdentity);
    sessionStorage.removeItem('errorDocument');
    this.router.navigate([Rutas.instruccionesManual]);
  }

  typesIdentity() {
    if (this.object.servicios['documentoManual']['propiedades'] && this.object.servicios['documentoManual']['propiedades'].length > 0) {
      this.type = this.object.servicios['documentoManual']['propiedades'];
      console.log("=== " + this.type);
      if (this.type.includes('INE') && this.type.includes('PASSPORT')) {
        this.showINE = true;
        this.showPass = true;
        if (sessionStorage.getItem('ti')) {
          if (sessionStorage.getItem('ti').includes('ID_CARD')) {
            this.servicesGeneralService.setTipoIdentificacion(this.servicesGeneralService.INE);
          } else if (sessionStorage.getItem('ti').includes('PASSPORT')) {
            this.servicesGeneralService.setTipoIdentificacion(this.servicesGeneralService.PASSPORT);
          }
          this.router.navigate([Rutas.instruccionesManual]);
        }
      } else if (this.type.includes('INE')) {
        this.servicesGeneralService.setTipoIdentificacion(this.servicesGeneralService.INE);
        this.router.navigate([Rutas.instruccionesManual]);
      } else if (this.type.includes('PASSPORT')) {
        this.servicesGeneralService.setTipoIdentificacion(this.servicesGeneralService.PASSPORT);
        this.router.navigate([Rutas.instruccionesManual]);
      }
    } else {
      // this.servicesGeneralService.setTipoIdentificacion(this.servicesGeneralService.INE);
      this.router.navigate([Rutas.capturaDatos]);
    }

  }

  public getColorButton() {
    return this.global.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.global.getLabelColorSystem();
  }

  public getImageSystem() {
    return this.global.getImageConfig();
  }

}

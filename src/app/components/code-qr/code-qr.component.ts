import { Component, OnInit, NgModule } from '@angular/core';
import { Rutas } from 'src/app/model/RutasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, NavigationEnd, RouterModule } from '@angular/router';
import { SessionService } from '../../services/session/session.service';
import { MiddleDaonService } from '../../services/http/middle-daon.service';
import { sesionModel } from '../../model/sesion/SessionPojo';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { environment } from '../../../environments/environment';
import { ɵAnimationGroupPlayer } from '@angular/animations';
import { Session } from 'protractor';
import { ServicesGeneralService} from 'src/app/services/general/services-general.service';
import {TermsComponent} from '../terms/terms.component';

@Component({
  selector: 'app-code-qr',
  templateUrl: './code-qr.component.html',
  styleUrls: ['./code-qr.component.css']

})

export class CodeQrComponent implements OnInit {

  title = 'Escanea con tu celular';
  descr = 'Para continuar en tu celular escanea el código QR o recibe un link en tu e-mail.';
  btnTitleContinuar = 'Continuar';
  btnTitleSendLink = 'Enviar link a mi correo';
  public code: string = null;
  copiar:string = '      ';
  enviar:string = '      ';
  data;
  constructor(private middle: MiddleMongoService, private serviciogeneralService: ServicesGeneralService,
    private spinner: NgxSpinnerService, public router: Router,
              private session: SessionService, private actRoute: ActivatedRoute, private middleDaon: MiddleDaonService,
              private middleMongo: MiddleMongoService, private term: TermsComponent ) { this.code = "preview"}

  filtersLoaded: Promise<boolean>;
  errorMensaje: string;
  id: string;


  async ngOnInit() {
      await this.spinner.show();
      var movilFlag = sessionStorage.getItem('movilFlag');
      if(movilFlag){
        this.router.navigate([Rutas.fin]);
      }else{
        var baseUrl = document.location.origin;
        console.log(baseUrl);
        var idSessionStorage = sessionStorage.getItem('currentSessionDaon');
        if (idSessionStorage){
          this.data = JSON.parse(idSessionStorage);
          if (this.data) {
              console.log("ti===== ",sessionStorage.getItem('ti'));
              this.id = this.data._id
              console.log(this.id);
              this.serviciogeneralService.settI('ID_CARD');
              this.serviciogeneralService.setFrontAndBack('front');
              var url = baseUrl + '#' + Rutas.globalInstruction + `${this.id}`;
              this.code =  url+"?ti="+sessionStorage.getItem('ti');

          }
        }
      }
      await this.spinner.hide();
  }

  async alredySessionExist() {
    const object = this.session.getObjectSession();
    console.log('sessionDatosFiscales= ', object);
    if (object === null || object === undefined) {
      this.router.navigate([Rutas.terminos + `/${this.id}`]);
      return false;
    } else {
      if (object._id !== this.id) {
        this.router.navigate([Rutas.error]);
        return false;
      } else if (object.datosFiscales) {
        this.router.navigate([Rutas.correo + `${this.id}`]);
        return false;
      } else {
        return true;
      }
    }
  }


  async enter() {
    await this.spinner.show();
    sessionStorage.setItem('movilFlag','true');
    this.router.navigate([Rutas.qrFinal]);
    await this.spinner.hide();
  }

  public getColorButton(){

    return this.term.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.term.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.term.getImageConfig();
  }

  async copyLink(){
    var aux = document.createElement("input");
    var value = $("#link").val().toString();
    aux.setAttribute("value", value);
    document.body.appendChild(aux);
    console.log(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
    this.copiar="Copiado!";
    
    setTimeout(() => {
      this.copiar=" ";
    }, 2000);

    
  }

  async sendMail(){
    const modal = document.getElementById('modalSendLink');
    modal.style.display = 'none';
    
    console.log("this.date = ", sessionStorage.getItem("email"));
    let correo = "";
    if(sessionStorage.getItem("email")){
      correo = this.hideEmail(sessionStorage.getItem("email"));
    }
    this.enviar="Hemos enviado el correo a " + correo;
    var dataLink = await this.middle.getLinkMail(this.id,sessionStorage.getItem('ti'));
    if(dataLink.status && dataLink.status===404){
      this.enviar="error al enviar correo!";
    }
    modal.style.display = 'block';

  }

  hideEmail(textCorreo: string) {
    if (textCorreo) {
      const textCorreoList = textCorreo.split('@');
      let text = textCorreoList[0].split('')[0] + '***' + '@' + textCorreoList[1].split('.')[0].split('')[0] + '***';
      for (let i = 1; i < textCorreoList[1].split('.').length; i++) {
        text = text + '.' + textCorreoList[1].split('.')[i];
      }
      return text;
    }
    return '';
  }

  async cerrarModal() {
    const modal = document.getElementById('modalSendLink');
    modal.style.display = 'none';
  }
  
}


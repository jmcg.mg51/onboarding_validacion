import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-seguro-social',
  templateUrl: './seguro-social.component.html',
  styleUrls: ['./seguro-social.component.css']
})
export class SeguroSocialComponent implements OnInit {

    //Mark: variables of components
    razonPatron;
    errorGeneral: boolean;
    asalarido: boolean;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsSeguro;

  async ngOnInit() {
    this.spinner.show;
    this.asalarido = false; 
    
    if (!(await this.alredySessionExist())) { return; }
   
   
    this.id = this.object._id;
    if(sessionStorage.getItem('intentsSeguro')){
      this.intentsSeguro = sessionStorage.getItem('intentsSeguro');
    }else{
      sessionStorage.setItem('intentsSeguro', '0') 
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("seguroSocial", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsSeguro = sessionStorage.getItem('intentsSeguro');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
    document.getElementById('razonPatron').focus();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.seguroSocial===true){
        this.router.navigate([this.workFlowService.redirectFlow('seguroSocial',this.object.servicios)]); 
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsSeguro =  parseInt(this.intentsSeguro) + 1;
    sessionStorage.setItem('intentsSeguro', this.intentsSeguro);
    this.razonPatron = $("#razonPatron").val();


    if(this.razonPatron === "" ){
      this.errorGeneral = true; 
      this.spinner.hide();
    }else{
      const seguro = {
        patron: this.razonPatron,
        asalariado: this.asalarido
      }

      console.log(seguro);
      await this.saveSeguro(seguro);
      this.spinner.hide();
    }

    
    //this.CurpValidate(this.curpInput);
  }



  async saveSeguro(seguro){
    
    sessionStorage.setItem('seguroSocial', "si");
    sessionStorage.setItem('patron', this.razonPatron);


    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("seguroSocial", device, this.intentsSeguro, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
    await this.middle.updateDataUser({seguro}, this.id);

    const object = this.session.getObjectSession();
    object.seguroSocial = true;
    this.session.updateModel(object);

    this.continue();

    
  }

  ckeckChangeStatus(){
    var check = document.getElementById("changeStatus") as HTMLInputElement;
    if(check.checked){
      this.asalarido = true;
    }else{
      this.asalarido = false; 
    }
  }

  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('seguroSocial',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
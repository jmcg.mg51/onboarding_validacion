import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../../environments/environment';
import {GlobalInstructionComponent} from '../../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { terminosDeUso, politicaDePrivacidad } from '../../../model/documentos/aceptacion';
import htmlToPdfmake from "html-to-pdfmake";
import { clearScreenDown } from 'readline';
var pdfMake = require("pdfmake/build/pdfmake");
var pdfFonts = require("pdfmake/build/vfs_fonts");
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-terminos-condiciones',
  templateUrl: './terminos-condiciones.component.html',
  styleUrls: ['./terminos-condiciones.component.css']
})
export class TerminosCondicionesSignatureComponent implements OnInit {

    //Mark: variables of components
    nip;
    aceptarCheck: boolean;
    errorGeneral: boolean;
    chosenYearDate: Date;
    btnContinuar: boolean;
    terminosBase64;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService, private modalService: NgbModal) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsAnalisisFinancieroTerminos;
  textoModal;
  nombreOtorgante: string;
  textoModalPolitica;
  tituloModal;
  


  async ngOnInit() {
    this.spinner.show;
    this.errorGeneral = false;
    this.btnContinuar = true;
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id; 

    if(sessionStorage.getItem('intentsTermsSignature')){
      this.intentsAnalisisFinancieroTerminos = sessionStorage.getItem('intentsTermsSignature');
    }else{
      sessionStorage.setItem('intentsTermsSignature', '0');
      console.log("El valor de intents es:",sessionStorage.getItem('intentsTermsSignature'));
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("termsSignature", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsAnalisisFinancieroTerminos = sessionStorage.getItem('intentsTermsSignature');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});

    await this.fullDataContract();
    // if(sessionStorage.getItem("contratante")){
    //   var textTerminos = sessionStorage.getItem("textoTerminos");
    //   var contratante = sessionStorage.getItem("contratante");
    //   this.textoModal = textTerminos.replace(/#NOMBRE#/g, contratante);

    //   //TODO {demás variables a personalizar}
    //   if(sessionStorage.getItem("nombreComercio")){
    //     var comercio = sessionStorage.getItem("nombreComercio");
    //     this.textoModal = this.textoModal.replace(/#COMERCIO#/g, comercio);
    //   }

    //   var valuePDF = htmlToPdfmake(this.textoModal);
    //   var htmlStructure = {content:valuePDF};
    //   this.terminosBase64 = await this.convertHTMLToBase64(htmlStructure);
    //   console.log("terminosBase64", this.terminosBase64);

    // }else{
    //   this.textoModal = sessionStorage.getItem("textoTerminos");
    //   var valuePDF = htmlToPdfmake(this.textoModal);
    //   var htmlStructure = {content:valuePDF};
    //   this.terminosBase64 = await this.convertHTMLToBase64(htmlStructure);
    //   console.log("terminosBase64", this.terminosBase64);
    // }

    // if(sessionStorage.getItem("nombreComercio")){
    //   this.nombreOtorgante = sessionStorage.getItem("nombreComercio");
    // }else{
    //   this.nombreOtorgante = "(Nombre del otorgante del credito)";
    // }

    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.nip===true){
        this.router.navigate([this.workFlowService.redirectFlow('nip',this.object.servicios)]);   
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsAnalisisFinancieroTerminos =  parseInt(this.intentsAnalisisFinancieroTerminos) + 1;
    sessionStorage.setItem('intentsTermsSignature', this.intentsAnalisisFinancieroTerminos);
    this.nip = $("#nipInput").val();
    var nivel = sessionStorage.getItem("intentsTermsSignature");

    if(this.nip === "" || this.aceptarCheck === false){
      this.errorGeneral = true; 
      this.spinner.hide();
    }else{
      const termsSignature = {
        acepto: this.aceptarCheck,
        nip: this.nip,
        terminos: this.terminosBase64
      }

      console.log(termsSignature);
      await this.saveTerminos(termsSignature);
      this.spinner.hide();
    }

    
    //this.CurpValidate(this.curpInput);
  }


  async fullDataContract() {
    var textTerminos = sessionStorage.getItem("textoTerminos");
    textTerminos = textTerminos.replace(/#NOMBRE#/g, sessionStorage.getItem("contratante") || '#NOMBRE#');
    // this.textoModal = textTerminos.replace(/#NOMBRE#/g, sessionStorage.getItem("contratante") || '#NOMBRE#');
    this.textoModal = textTerminos.replace(/#COMERCIO#/g, sessionStorage.getItem("nombreComercio") || '#COMERCIO#');
    let jsonAdicionales = undefined;
    try {
      jsonAdicionales = JSON.parse( atob(sessionStorage.getItem('valoresAdicionalesContrato')));
    } catch (error) {
      jsonAdicionales = undefined;
    }
    console.log('el valor del json es: ' , jsonAdicionales);
    if(jsonAdicionales) {
      for(let value in jsonAdicionales) {
        let regex = new RegExp(`#${value}#`, 'g');
        console.log(regex);
        this.textoModal = this.textoModal.replace(new RegExp(`#${value}#`, 'g'), jsonAdicionales[value]);
      }
    }
    var valuePDF = htmlToPdfmake(this.textoModal);
    var htmlStructure = {content:valuePDF};
    this.terminosBase64 = await this.convertHTMLToBase64(htmlStructure);
    console.log("terminosBase64", this.terminosBase64);
    this.nombreOtorgante = sessionStorage.getItem("nombreComercio") || '(Nombre del otorgante del credito)';
  }


  async saveTerminos(termsSignature){

    const result = await this.middle.validaCodigoPINsignature(this.id, termsSignature.nip);
    if(result === 200){
      this.errorGeneral = false;
      sessionStorage.setItem('termsSignature', "si");
      //sessionStorage.setItem('patron', this.razonPatron);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("termsSignature", device, this.intentsAnalisisFinancieroTerminos, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);
      await this.middle.updateDataUser({termsSignature}, this.id);

      const object = this.session.getObjectSession();
      object.TerminosCondicionesSignatureComponent = true;
      this.session.updateModel(object);

      this.continue();
    }else{
      this.errorGeneral = true;
      return;
    }
    
  }

  ckeckChangeStatus(){
    var check = document.getElementById("changeStatus") as HTMLInputElement;
    if(check.checked){
      this.aceptarCheck = true;
      this.btnContinuar = false;
    }else{
      this.aceptarCheck = false; 
      this.btnContinuar = true;
    }
  }

  open() {
    
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'block';
    //this.modalService.open(content);
  }

  cerrarModal(){
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'none';
  }

  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('nip',this.object.servicios)]); 
  }



async convertHTMLToBase64(pdfStructure){
   let pdfBase64;
    var pdfDocGenerator = await pdfMake.createPdf(pdfStructure, {
      exampleLayout: {
        hLineColor: function (rowIndex:any, node:any, colIndex:any) {
          if (rowIndex === node.table.body.length) return 'blue';
          return rowIndex <= 1 ? 'red' : '#dddddd';
        },
        vLineColor: function (colIndex:any, node:any, rowIndex:any) {
          if (rowIndex === 0) return 'red';
          return rowIndex > 0 && (colIndex === 0 || colIndex === node.table.body[0].length) ? 'blue' : 'black';
        }  
      }
    });

    return new Promise(function(resolve, reject) {
      pdfDocGenerator.getBase64(function (pdfBase64) {
        resolve(pdfBase64);
      });
    });

  }

  openPolitica(content){
    this.textoModalPolitica = politicaDePrivacidad;
    this.tituloModal = 'Politica de Privacidad';
    this.modalService.open(content);

  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
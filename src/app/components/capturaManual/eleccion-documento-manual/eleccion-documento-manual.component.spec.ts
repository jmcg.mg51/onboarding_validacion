import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EleccionDocumentoManualComponent } from './eleccion-documento-manual.component';

describe('EleccionDocumentoManualComponent', () => {
  let component: EleccionDocumentoManualComponent;
  let fixture: ComponentFixture<EleccionDocumentoManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EleccionDocumentoManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EleccionDocumentoManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

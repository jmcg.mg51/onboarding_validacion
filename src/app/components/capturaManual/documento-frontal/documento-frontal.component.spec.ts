import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoFrontalComponent } from './documento-frontal.component';

describe('DocumentoFrontalComponent', () => {
  let component: DocumentoFrontalComponent;
  let fixture: ComponentFixture<DocumentoFrontalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoFrontalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoFrontalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Output, EventEmitter, Input, Inject, ElementRef, ViewChild } from '@angular/core';
import { MiddleVerificacionService } from 'src/app/services/http/middle-verificacion.service';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import { sesionModel } from '../../../model/sesion/SessionPojo';
import { SessionService } from '../../../services/session/session.service';
import { Rutas } from '../../../model/RutasUtil';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesGeneralService, isMobile } from '../../../services/general/services-general.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../../environments/environment';
import { TermsComponent } from '../../terms/terms.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import Botd from '@fpjs-incubator/botd-agent';

@Component({
  providers: [TermsComponent],
  selector: 'app-correo',
  templateUrl: './correo.component.html',
  styleUrls: ['./correo.component.css']
})


export class CorreoComponent implements OnInit {

  constructor(private workFlowService: WorkFlowService, public serviciogeneralService: ServicesGeneralService,
    private actRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private router: Router, private middleVerifica: MiddleVerificacionService, private middleDaon: MiddleDaonService,
    private session: SessionService, private middle: MiddleMongoService,
    private term: TermsComponent, private analiticService: AnaliticService) { }

  filtersLoaded: Promise<boolean>;
  codigoText = '';
  error = '';
  id: any;
  object: sesionModel;
  a = ''; b = ''; c = ''; d = '';
  correo = '';
  intents;
  @ViewChild('box1', { read: false, static: false }) box1: ElementRef;
  @ViewChild('validarBoton', { read: false, static: false }) validarBoton: ElementRef;

  async ngOnInit() {
    if (sessionStorage.getItem('intentsEmail')) {
      this.intents = sessionStorage.getItem('intentsEmail');
    } else {
      sessionStorage.setItem('intentsEmail', '0')
      this.intents = sessionStorage.getItem('intentsEmail');
    }
    this.correo = this.hideEmail(this.serviciogeneralService.getCorreo());
    await this.spinner.show();
    if (this.serviciogeneralService.getCorreo() === undefined || this.serviciogeneralService.getCorreo() === '') {
      this.router.navigate([Rutas.correo]);
    }

    if (!await this.alredySessionExist()) { return; }
    this.id = this.object._id;
    const fp = await FP.load({ token: environment.fingerJsToken, 
 endpoint: environment.fpDomain });
    fp.get({ tag: { 'tag': this.id } });

    // const botdPromise = Botd.load({
    //   token: environment.BotTdToken,
    //   mode: "allData"
    // });
    // const botd = await botdPromise
    // const resultBotd = await botd.detect({ tag: this.id });
    // console.log(resultBotd);
    
    // await this.middleVerifica.sendDataWebhookFinger({ data: resultBotd, id: this.id });
    await this.spinner.hide();
    document.getElementById('box1') && document.getElementById('box1') != null ?
    document.getElementById('box1').focus() : '';
    this.filtersLoaded = Promise.resolve(true);
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if (this.object.correo === true && this.object.emailVerified === true) {
        this.router.navigate([this.workFlowService.redirectFlow('correoElectronico', this.object.servicios)]);
        return false;
      } else {
        return true;
      }
    }
  }

  hideEmail(textCorreo: string) {
    if (textCorreo) {
      const textCorreoList = textCorreo.split('@');
      let text = textCorreoList[0].split('')[0] + '***' + '@' + textCorreoList[1].split('.')[0].split('')[0] + '***';
      for (let i = 1; i < textCorreoList[1].split('.').length; i++) {
        text = text + '.' + textCorreoList[1].split('.')[i];
      }
      return text;
    }
    return '';
  }

  onSearchChange1(searchValue: string): void {
    this.codigoText = searchValue;
  }

  async validaCodigo() {
    await this.spinner.show();
    this.intents = parseInt(this.intents) + 1;
    sessionStorage.setItem('intentsEmail', this.intents);

    console.log(this.codigoText);
    const result = await this.middleVerifica.validaCodigoEmail(this.id, this.codigoText);
    if (result === 200) {
      this.error = '';
      await this.saveData();
    } else {
      this.error = 'Clave invalida, intente de nuevo';
      this.box1.nativeElement.value = '';
      this.codigoText = '';
    }
    await this.spinner.hide();
  }

  async verificaCorreo() {
    const objetoDaon = await this.middleDaon.createDaonRegister(this.serviciogeneralService.getCorreo(), this.id);
    if (objetoDaon === true) {
      await this.saveData();
    } else {
      this.router.navigate([Rutas.error]);
    }
    await this.spinner.hide();
  }

  async saveData() {
    this.object.emailVerified = true;
    this.object.correo = true;
    this.session.updateModel(this.object);
    const device = this.analiticService.getTypeDevice();
    const analytics = this.analiticService.updateAnalytics("correoElectronico", device, this.intents, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    sessionStorage.setItem('emailCode', this.codigoText);
    await this.middle.updateDataUser(analytics, this.id);
    await this.middle.updateDataUser(this.object, this.id);
    sessionStorage.setItem('email', this.serviciogeneralService.getCorreo());
    this.router.navigate([this.workFlowService.redirectFlow('correoElectronico', this.object.servicios)]);
  }

  async reescribirCorreo() {
    this.router.navigate([Rutas.correo]);
  }

  async aceptar() {
    await this.spinner.show();
    const correoText = this.serviciogeneralService.getCorreo();
    console.log(correoText);
    console.log('id >>>' + this.id + ' - correoText >>> ' + correoText + ' - ' + this.object['emailVerified'])
    await this.middleVerifica.generaCodigoEmail(this.id, correoText, true);
    this.serviciogeneralService.setCorreo(correoText);
    await this.spinner.hide();
  }

  public getColorButton() {

    return this.term.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.term.getLabelColorSystem();
  }

  public getImageSystem() {
    return this.term.getImageConfig();
  }


}

import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

import { Rutas } from 'src/app/model/RutasUtil';
import { SessionService } from '../../services/session/session.service';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { MiddleDaonService } from '../../services/http/middle-daon.service';
import { terminosDeUso, politicaDePrivacidad } from '../../model/documentos/aceptacion';
import { Observable } from 'rxjs';
import { isMobile } from '../../services/general/services-general.service';


declare const deviceInfo: any;

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  id: any;
  oferta: string;
  tag: string;
  textoModal = '';
  tituloModal = '';
  apiToken: string;
  modelo: sesionModel;
  merchantCode: string;
  datosDelCliente: sesionModel;
  filtersLoaded: Promise<boolean>;
  paramsAdic: string;
  //MARK: Variables de sistema
  colorButtons: string;
  colorLabels: string;
  colorBorders: string
  configColorButton: string;
  configColorLabel: string;
  configColorBorder:string;
  defaultColorButton: "black";
  defaultColorBorder: "black";
  defaultColorLabel: "white";
  imageService: string;
  imageConfig: string;
  propiedadesServicios; 
  objectPropiedades;
  errorNetwork: boolean;
  showErrorNetwork: boolean;
  paramValue: string;
  decipherText;
  onlyMovil = false;
  public code: string = null;


  constructor(config: NgbModalConfig, private modalService: NgbModal, public router: Router, public session: SessionService,
    private http: HttpClient, private middle: MiddleMongoService, private actRoute: ActivatedRoute,
    private middleDaon: MiddleDaonService, private spinner: NgxSpinnerService) {
    config.backdrop = 'static';
    config.keyboard = false;

  }

  objectOffer: {};

  async ngOnInit() {
    this.objectPropiedades = [];
  // await this.middle.test();
  var responseNavigator = this.validateDevive();

  if(responseNavigator){
    await this.spinner.show();
    sessionStorage.clear();
    this.actRoute.params.subscribe(params => {
      this.merchantCode = params['id'];
    });
    sessionStorage.setItem('merchantCode' , this.merchantCode);
    this.actRoute.queryParams.subscribe(params => {
      console.log('los params son : ' , params);
      this.tag = params['tag'];
      this.paramsAdic = params['paramsAdic'];
      this.paramValue = decodeURIComponent(params['paramValue']);

      console.log("LOG", this.paramValue);
      
    });
    
    console.log("paramvalue", this.paramValue);
    if(this.paramValue !== 'undefined'){
      console.log("if");
      //Se llama al lambda de descifrado
      this.decipherText = await this.middle.decipherText(this.paramValue); 
      let dataDecipher = await this.isJsonString(this.decipherText.body);
      if(dataDecipher === true){
        this.decipherText = JSON.parse(this.decipherText.body);
        console.log("decipher text", this.decipherText.email);
      }
     
    }

    console.log('ya tengo el merchatn: ', this.tag);
    if (!this.merchantCode) {
      await this.goToErrorPage();
      return;
    }
    const result = this.getValuesFromMerchanCode();
    if (!result) {
      await this.goToErrorPage();
      return;
    }
    await this.getOffer(this.merchantCode);
    this.filtersLoaded = Promise.resolve(true);
    
    await this.spinner.hide();
  }else{
    sessionStorage.setItem("urlTerms", window.location.href);
    this.router.navigate([Rutas.deviceConnection]);
    
  }
  if(document.getElementById('aceptarBoton')) {
    document.getElementById('aceptarBoton').focus();
  }
  }

  getValuesFromMerchanCode() {
    
    let result = true;
    try {
      const values = atob(this.merchantCode).split(':');
      if (values.length > 1) {
        this.apiToken = values[0];
        this.oferta = values[1];
      } else {
        result = false;
      }
    } catch (e) {
      console.log('no se pudo obtener la b64');
      result = false;
    }
    return result;
  }

  
  async isJsonString(jsonText) {
    try {
        JSON.parse(jsonText);
        return true;
    } catch (e) {
        console.log("error de parseo de json");
        return false;
    }
}

  async initSessionValues() {
    this.session.cleanValues();
    sessionStorage.setItem('merchantCode' , this.merchantCode);
    sessionStorage.setItem('valoresAdicionalesContrato', this.paramsAdic);

    if(this.decipherText && this.decipherText.email &&
      this.decipherText.body != ""){
      sessionStorage.setItem("email", this.decipherText.email);
    }
    if(this.decipherText && this.decipherText.rfc && 
      this.decipherText.rfc != ""){
      sessionStorage.setItem("rfc", this.decipherText.rfc);
    }
    this.datosDelCliente = new sesionModel();
    const objectResponse = await this.middle.creaTrackId(this.oferta, this.apiToken, this.tag, this.paramsAdic);
    if (!objectResponse) {
      await this.goToErrorPage();
    }
    this.id = objectResponse['id'];
    this.datosDelCliente._id = this.id;
    this.datosDelCliente.callback = objectResponse['callback'];
  }

  async goToErrorPage() {
    await this.spinner.hide();
    this.router.navigate([Rutas.error]);
  }

  async siguiente() {
    await this.spinner.show();
    await this.initSessionValues();
    this.session.updateModel(this.datosDelCliente);
    this.datosDelCliente.terminos = true;
    this.datosDelCliente.daon = {
      daonClientHref: '',
      daonHref: '',
      selfie: false,
      identity: false,
      pruebaVida: false
    };

    await this.middle.updateTermsDataUser({ terminos: true }, this.id);
    await this.session.updateModel(this.datosDelCliente);
    console.log('aqui los datos del cliente son: ' , this.datosDelCliente);
    await this.spinner.hide();
    this.router.navigate([Rutas.globalInstruction + `/${this.id}`]);

  }

  open(content, tipo) {
    if (tipo === 'terminos') {
      this.textoModal = terminosDeUso;
      this.tituloModal = 'Terminos de Uso';
    } else {
      this.textoModal = politicaDePrivacidad;
      this.tituloModal = 'Politica de Privacidad';
    }
    this.modalService.open(content);
  }


  async getOffer(merchan: string) {
    var merchanApiToken: string;
    var merchanOferta: string;

    const values = atob(merchan).split(':');
    if (values.length > 1) {
      merchanApiToken = values[0];
      merchanOferta = values[1];
    }

    this.objectOffer = await this.middle.getInfoOferta(merchanOferta, merchanApiToken);
    if(!this.objectOffer['error']){
      this.showErrorNetwork = false; 
      this.errorNetwork = true; 
      this.configColorButton = this.objectOffer["configuration"] !== undefined ?
      this.objectOffer["configuration"].colorButton : this.configColorButton;

    this.configColorLabel = this.objectOffer["configuration"] !== undefined?
      this.objectOffer["configuration"].colorLabel : this.configColorLabel;

    const servicios = this.objectOffer["servicios"];
    if(servicios && servicios.oferta && servicios.oferta.includes('Bot')) {
      sessionStorage.setItem('bot', 'true');
    }
    if(servicios && servicios.oferta && servicios.oferta.includes('Movil') && !isMobile(navigator.userAgent)) {
      console.log('no debo permitir la visualizacion');
      this.code =  window.location.href;
      console.log('la url es: ' , this.code);
      this.onlyMovil = true;
      this.spinner.hide();
      return;
    }
    //const jsonServicios = JSON.parse(servicios);
    this.propiedadesServicios = servicios.propiedades;
    
    for(const value in this.propiedadesServicios){
      var index = parseInt(value.toString()) + 1 ;
      const nodoPropiedades = { name: `${this.propiedadesServicios[value]}`, image: `${index}`}; 
      if(index.toString() != "NaN"){
        this.objectPropiedades.push(nodoPropiedades);
      }
      

    }
    console.log("propiedades", this.objectPropiedades);
    
    this.imageService = this.objectOffer["urlIcon"];

    sessionStorage.setItem("colorButton", this.configColorButton);
    sessionStorage.setItem("colorLabel", this.configColorLabel);
    sessionStorage.setItem("imageConfig", this.imageService);
    }else{
      console.log("Ocurrio un error de conexion");
      this.errorNetwork = false;
      this.showErrorNetwork = true;
      this.spinner.hide();
      return;
    }
    


  }

  public getButtonColorSystem() {
    this.colorButtons = sessionStorage.getItem("colorButton");


    return this.colorButtons
  }

  public getLabelColorSystem() {
    this.colorLabels = sessionStorage.getItem("colorLabel");

    return this.colorLabels;
  }

  public getImageConfig() {
    this.imageConfig = sessionStorage.getItem("imageConfig");
    return this.imageConfig;

  }

  public getBorderColorSystem(){
    this.colorBorders = sessionStorage.getItem("colorButton");

    return this.colorBorders;
  }

  validateDevive(){
    let device = deviceInfo();

    switch (device['os.name']) {
      case 'Macintosh':
        return true; 
        break;
      case 'Windows':
        return true; 
        break;
      case 'Android':
        if(device['browser.name'] === "Firefox" || device['browser.name'] === "Chrome" || device['browser.name'] === "Opera" 
        || device['browser.name'] === "Safari" ){
          if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
            return true; 
          }else{
            return false;
          }
          
        }else{
          return false; 
        }
        break;
      case 'iPad':
        if( device['browser.name'] === "Safari" ){
          if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
            return true; 
          }else{
            return false;
          }
        }else{
          //Redirect a página de error
          return false;
        }
        break;
      case 'iPhone':
        if( device['browser.name'] === "Safari" ){
          console.log("media device", navigator.mediaDevices.getUserMedia());
          if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices ){
            console.log("soy true");
            return true; 
          }else{
            /** CAMBIAR EL VALOR A FALSE */
            return false;
          }
        }else{
          return false; 
        }
        break;
      default:
        return false; 
        break;
    }
  }

}

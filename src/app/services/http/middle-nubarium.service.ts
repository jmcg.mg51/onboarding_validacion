import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { LigaUtil } from 'src/app/model/LigasUtil';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';


@Injectable({
  providedIn: 'root'
})
export class MiddleNubariumService {

  constructor(private http: HttpClient) { }

  headers = new HttpHeaders({ 'Content-Type': 'application/json', Accept: 'q=0.8;application/json;q=0.9' });


  async validateDocument(data) {
    var statusCode;
    let response
    console.log('lo que voy a mandar es: ' , data);
    try {
      response = await this.http.post(LigaUtil.urlMiddleNubarium() + `/catalogo/nubarium/comprobante`, JSON.stringify(data), { headers: this.headers, })
    .toPromise();
    } catch (error) {
    } 
    
    if(response && !response["errorType"]){
      statusCode = 200;
    }else{
      statusCode = "ERROR"
    }
    return statusCode;
  }

  async catCFDI(typePerson) {
    let response = {};
    console.log('lo que voy a mandar es: ' , typePerson);
    try {
      response = await this.http.post(LigaUtil.urlMiddleNubarium() + `/catalogo/cfdi`, {typePerson}, { headers: this.headers })
    .toPromise();
    } catch (error) {
      console.log('Hubo un error al consumir el servicio de catalogos : ' , error);
    } 
    return response;
  }
     

}

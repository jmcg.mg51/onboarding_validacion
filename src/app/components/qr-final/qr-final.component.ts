import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';

@Component({
  selector: 'app-qr-final',
  templateUrl: './qr-final.component.html',
  styleUrls: ['./qr-final.component.css']
})
export class QrFinalComponent implements OnInit {

  title = 'Aún no has finalizado';
  instruction = 'Por favor continue en su dispositivo móvil';

  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;

  async ngOnInit() {    
    await this.spinner.hide();
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';
import { MiddleNubariumService } from 'src/app/services/http/middle-nubarium.service';
import { SessionService } from 'src/app/services/session/session.service';
import { WorkFlowService } from 'src/app/services/session/work-flow.service';
import { GlobalInstructionComponent } from '../global-instruction/global-instruction.component';

@Component({
  selector: 'app-cfdi',
  templateUrl: './cfdi.component.html',
  styleUrls: ['./cfdi.component.css']
})
export class CfdiComponent implements OnInit {

  constructor(private middleCat: MiddleNubariumService, private formBuilder: FormBuilder, 
    private spinner: NgxSpinnerService, private session: SessionService, public router: Router,
    private global: GlobalInstructionComponent, private middle: MiddleMongoService,
    private workFlowService : WorkFlowService) { }

  catalogoCFDI = [];
  catalgoRegimen = [];
  errorMessage = '';
  object: sesionModel;
  id: string;
  
  cfdiForm = this.formBuilder.group({
    cfdi: ['', { validators: [Validators.required] }],
    regimen: ['', { validators: [Validators.required] }]
  });

  async ngOnInit() {
    await this.spinner.show();
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    let responseCatalogo = await this.middleCat.catCFDI( sessionStorage.getItem('typePerson')  );
    console.log('la respuesta del catalogo es: ' , responseCatalogo);
    if(responseCatalogo) {
      this.catalogoCFDI = responseCatalogo['cfdi'];
      this.catalgoRegimen = responseCatalogo['regimen'];
    }
    await this.spinner.hide();
  }
  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    }
    return true;
  }

  public getColorButton() {
    return this.global.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.global.getLabelColorSystem();
  }

  async saveValues() {
    this.errorMessage = '';
    await this.spinner.show();
    let chooseCFDI = this.cfdiForm.controls.cfdi.value.toUpperCase();
    console.log(chooseCFDI);
    let chooseRegimen = this.cfdiForm.controls.regimen.value.toUpperCase();
    console.log(chooseRegimen);
    if (!this.cfdiForm.valid || !chooseCFDI || !chooseRegimen || chooseCFDI == 'NODATA' || chooseRegimen == 'NODATA') {
      this.errorMessage = 'Selecciona todos los campos';
      await this.spinner.hide();
      return;
    }
    let resultCFDI = this.catalogoCFDI.find(item => item.cdUsoCFDI === chooseCFDI);
    let resultRegimen = this.catalgoRegimen.find(item => item.cdRegimenFiscal === chooseRegimen);

    let reqCFDI = {
      usoCFDI : resultCFDI,
      regimenFiscal : resultRegimen
    };
    console.log('el reqCFDI es: ' , reqCFDI);
    await this.middle.updateDataUser({ cfdi: reqCFDI }, this.id);
    await this.spinner.hide();
    this.router.navigate([this.workFlowService.redirectFlow('cfdi',this.object.servicios)]);
  }
}

import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Rutas } from 'src/app/model/RutasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import { ErrorSelfieService } from 'src/app/services/errores/error-selfie.service';
import { SessionService } from 'src/app/services/session/session.service';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../../model/sesion/SessionPojo';
import { WorkFlowService } from '../../../../services/session/work-flow.service';
import {TermsComponent} from '../../../terms/terms.component';
import { MiddleMongoService } from '../../../../services/http/middle-mongo.service';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { MiddleIdMissionService } from '../../../../services/http/middle-idMission.service';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';

declare function initializeSdk(token, service, idtype, serviceName): any;
declare function detectFaceSdk(token, configSDK, callback): any;

@Component({
  selector: 'app-instrucciones',
  templateUrl: './instrucciones.component.html',
  styleUrls: ['./instrucciones.component.css']
})

export class InstruccionesComponent implements OnInit {

  constructor(private workFlowService :WorkFlowService, private router: Router, private session: SessionService, private actRoute: ActivatedRoute,
              public spinner: NgxSpinnerService, private errorSelfieService: ErrorSelfieService,
              private term: TermsComponent, private middle: MiddleMongoService, private analyticService: AnaliticService,
              private idMission: MiddleIdMissionService, private middleDaon: MiddleDaonService,
              public ngZone: NgZone,) {
               }

  filtersLoaded: Promise<boolean>;
  id: string;
  errorMensaje: string;
  object: sesionModel;
  intentsSelfie;
  token; 
  imageData;
  configSDK;
  showSpinner: boolean;
  spinnerModal;
  mensajeModal: string;

  async ngOnInit() {
    this.errorMensaje = sessionStorage.getItem("errorIdMission");
    await this.spinner.show();
    let serviceId = sessionStorage.getItem("serviceNumber");
    let idType =  sessionStorage.getItem("serviceId");
    let serviceName = sessionStorage.getItem('serviceIdMissionName');
        //sessionStorage.getItem("serviceId") === "ID_CARD" ? idType = "VID" : idType = "PP";
    console.log("tipo de id", idType);
    if(sessionStorage.getItem("tokenIdMission")){
        console.log("existe el token");
        this.token = sessionStorage.getItem('tokenIdMission');
      }else{
        let response = await this.idMission.getToken();
        this.token = response['data'].tokenId;
        console.log("token", this.token);
        sessionStorage.setItem('tokenIdMission', this.token);
        initializeSdk(this.token, serviceId, idType, serviceName);
      }

    // this.errorMensaje = this.errorSelfieService.returnMensaje();
    
   if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;

    let principalColor = this.getColorButton().replace('#', '');
    let secondarylColor = this.getColorLabel().replace('#', '');
    this.configSDK = {
      colorButton: principalColor,
      colorLabel: secondarylColor
      
    }

    if(sessionStorage.getItem('intentsSelfie')){
      this.intentsSelfie = sessionStorage.getItem('intentsSelfie');
    }else{
      sessionStorage.setItem('intentsSelfie', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("selfie", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsSelfie = sessionStorage.getItem('intentsSelfie'); 
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    this.filtersLoaded = Promise.resolve(true);
    await this.spinner.hide();
    this.spinnerModal = document.getElementById('modalSpinner');
    // $("#validarLabel").hide();
    document.getElementById('validarBoton').focus();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.selfie===true){
        console.log("INSTRUCCIONES==========");
        this.router.navigate([this.workFlowService.redirectFlow('selfie',this.object.servicios)]);
        return false;
      } else {
        return true;
      }
    }
  }
  
 async continuar() {
    sessionStorage.setItem("errorIdMission", '');
    this.mensajeModal = "Cargando..."
    this.spinnerModal.style.display = 'block';
    this.errorSelfieService.mensaje = '';
    this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
    sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
    await detectFaceSdk(this.token, this.configSDK, this.callback);
    this.spinnerModal.style.display = 'none';
    // setTimeout(() => {
    // this.spinnerModal.style.display = 'block';
    //   document.getElementById("continue-FaceCaptureInstruction").style.setProperty('background-color', this.configSDK.colorButton, 'important');
    //   document.getElementById("continue-FaceCaptureInstruction").style.setProperty('color', this.configSDK.colorLabel, 'important');
    //   document.getElementById("autoIDDocCapture-Back").style.setProperty('display', 'none', 'important');
      
    //   //document.getElementById("faceInstructionPageWeb").style.setProperty('background-color', this.configSDK.colorLabel, 'important');
    //   //document.getElementById("faceInstructionPageWeb").style.setProperty('background-color', '#6c757d', 'important');
    //   //document.getElementsByClassName("container-fluid customPagesWeb")[0].style.setProperty('background-color', '#fffff', 'important');
      
    //   this.spinnerModal.style.display = 'none';
    // }, 300);
    
    //this.spinner.show();
  }

  callback = async (response) => {
    // $("#validarBoton").hide();
    // $("#validarLabel").show();
    sessionStorage.setItem("errorIdMission", '');
    this.spinnerModal.style.display = 'block';
    console.log("el response del callback es:",response, this.token);
    console.log("estatus de selfie", response.Status);
      if(response && response.Status === "SUCCESS"){
        //await this.spinner.show();
        console.log("si entre al if");
      //this.saveOCR(response.FormDetails);
        sessionStorage.setItem("statusSelfie",response.Status );
        this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
        sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
        this.imageData = response.ImageData;
        sessionStorage.setItem("selfieImage", this.imageData);

        const object = this.session.getObjectSession();
        this.session.updateModel(object);
        const device = this.analyticService.getTypeDevice();
        const analytics = this.analyticService.updateAnalytics("selfie", device, this.intentsSelfie, "NO", false);
        console.log("El valor actual de analytics es:", analytics);
        await this.middle.updateDataUser(analytics, this.id);
        //Guardar los valores de tiempo
        await this.middleDaon.updateDaonDataUser(object, this.id);
        await this.idMission.saveSelfie(this.id, response.ImageData );
        this.ngZone.run(() => this.router.navigate([Rutas.chooseIdentity])).then();
        this.spinnerModal.style.display = 'none';
        return false; 
      
    }else{
      console.log("Ocurrió un error");
      this.initForm();
      this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
      sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
      this.spinnerModal.style.display = 'none';
      
    }
    this.spinnerModal.style.display = 'none';
    //this.spinner.hide();
  }

  initForm(){
    sessionStorage.removeItem('selfieImage');
    sessionStorage.removeItem('statusSelfie');
    sessionStorage.removeItem('tokenIdMission');
  }

  deleteToken(){
    sessionStorage.removeItem('tokenIdMission');
    setTimeout(function() { 
      sessionStorage.clear(); 
    }, (10 * 60 * 1000));
  }

  public getColorButton(){

    return this.term.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.term.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.term.getImageConfig();
  }


  
}

// this.ngZone.run(() => this.router.navigate([this.workFlowService.redirectFlow('pruebaDeVida',this.object.servicios)])).then();
       
import { Component, OnInit, NgZone } from '@angular/core';
import { ServicesGeneralService, isMobile } from '../../../../services/general/services-general.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import IconDefinitions from '../../../../../assets/icons/icons-svn';
import { ErrorSelfieService } from 'src/app/services/errores/error-selfie.service';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../../model/sesion/SessionPojo';
import {TermsComponent} from '../../../terms/terms.component';
import {GlobalInstructionComponent} from '../../../global-instruction/global-instruction.component';
import { MiddleIdMissionService } from '../../../../services/http/middle-idMission.service';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import { MiddleMongoService } from '../../../../services/http/middle-mongo.service';

declare function initializeSdk(token, service, idtype, serviceName): any;
declare function captureIDsdk(token, idType, configSDK, callbackFront, isMobile): any;
declare function captureIDBacksdk(token, service, idType, configSDK, callbackBack, isMobile): any
declare function submitSdk(token, serviceId, idType, trackId, email, callback, serviceName): any;




@Component({
  selector: 'app-capture-instruction',
  templateUrl: './capture-instruction.component.html',
  styleUrls: ['./capture-instruction.component.css']
})
export class CaptureInstructionComponent implements OnInit {

  constructor(public router: Router, public serviciogeneralService: ServicesGeneralService, private actRoute: ActivatedRoute,
    private session: SessionService, private spinner: NgxSpinnerService, private errorSelfieService: ErrorSelfieService,
    private term: TermsComponent, private global: GlobalInstructionComponent,
    private idMission: MiddleIdMissionService,  private middleDaon: MiddleDaonService,
    private analyticService: AnaliticService, private middle: MiddleMongoService, public ngZone: NgZone,
    ) {
      let tipoDcumento="";
      if (serviciogeneralService.gettI() !== undefined &&
      serviciogeneralService.getFrontAndBack() !== undefined) {

      sessionStorage.setItem('ti', serviciogeneralService.gettI());
      sessionStorage.setItem('fb', serviciogeneralService.getFrontAndBack());
      console.log("parte del INE: " + sessionStorage.getItem('fb'));

      
      if(serviciogeneralService.gettI()==='ID_CARD'){
        tipoDcumento="INE";
      }else if(serviciogeneralService.gettI()==='PASSPORT'){
        tipoDcumento="PASAPORTE";
      }
      if (serviciogeneralService.getFrontAndBack() === 'front') {
        this.titulo = tipoDcumento + " FRONTAL";
        this.description = 'Tómale foto al frente de tu '+tipoDcumento+' vigente.';
        if(!this.isMobileBool) {
          this.description = 'Sube una imagen del frente de tu '+tipoDcumento+' vigente.';
        }
        if(tipoDcumento === "PASAPORTE"){
          this.fotoFT = "../../../../../assets/img/svg/pasaporte.svg"
        }else{
          this.fotoFT = "../../../../../assets/img/animations/ine.svg";
        }
        if (serviciogeneralService.gettI() === 'ID_CARD') {
          this.idcard = 'id-card-front sv';
        } else {
          this.idcard = 'passport';
        }
      } else {
        this.titulo = tipoDcumento + " POSTERIOR";
        this.description = 'Tómale foto al reverso de tu '+tipoDcumento+' vigente.';
        if(!this.isMobileBool) {
          this.description = 'Sube una imagen del reverso de tu '+tipoDcumento+' vigente.';
        }
        this.fotoFT = "../../../../../assets/img/animations/ineback.svg";
        this.idcard = 'id-card-back';
      }
    } else if (sessionStorage.getItem('ti') === undefined ||
      sessionStorage.getItem('fb') === undefined) {
      this.router.navigate(['']);
    } else {
      if(sessionStorage.getItem('ti')==='ID_CARD'){
        tipoDcumento="INE";
      }else if(sessionStorage.getItem('ti')==='PASSPORT'){
        tipoDcumento="PASAPORTE";
      }
        if (sessionStorage.getItem('fb') === 'front') {
        this.titulo = tipoDcumento + " FRONTAL";
        this.description = 'Tómale foto al frente de tu '+tipoDcumento+' vigente.';
        if(!this.isMobileBool) {
          this.description = 'Sube una imagen del frente de tu '+tipoDcumento+' vigente.';
        }
        if(tipoDcumento === "PASAPORTE"){
          this.fotoFT = "../../../../../assets/img/svg/pasaporte.svg"
        }else{
          this.fotoFT = "../../../../../assets/img/animations/ine.svg";
        }
        if (sessionStorage.getItem('ti') === 'ID_CARD') {
          this.idcard = 'id-card-front ss';
        } else {
          this.idcard = 'passport';
        }
      } else {
        this.titulo = tipoDcumento + " POSTERIOR";
        this.idcard = 'id-card-back';
        this.description = 'Tómale foto al reverso de tu '+tipoDcumento+' vigente.';
        if(!this.isMobileBool) {
          this.description = 'Sube una imagen del reverso de tu '+tipoDcumento+' vigente.';
        }
        this.fotoFT = "../../../../../assets/img/animations/ineback.svg";
      }
    }

    

  }

  errorMensaje: string;
  titulo: string;
  description: string;
  id: string;
  dc: any;
  mensaje: string;
  img: any;
  fotoFT: string;
  idcard: any;
  icon: IconDefinitions;
  object: sesionModel;
  isMobileBool: boolean;
  intentsDocument;
  mensajeError:boolean;
  token: string;
  service;
  zipCode;
  colonia;
  tipoDcumento: string;
  idType;
  configSDK;
  modalSpiner;
  mensajeModal: string;
  paramIntents; 
  email;
  serviceName: string;

  async ngOnInit() {
  
    this.spinner.show();
    this.paramIntents = 3;
    this.errorMensaje = sessionStorage.getItem("errorIdMission");
    if(this.errorMensaje && this.errorMensaje != null){
      this.mensajeError = true; 
    }
    $("#captureGaleriaBack").css("display", "none");
    
    if (!(await this.alredySessionExist())) { return; }
    
    this.id = this.object._id;
    console.log("el id de captura documento es:", this.id);
    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    this.isMobileBool = isMobile(navigator.userAgent);

    if (this.isMobileBool) {
      document.getElementById('MovilContinue').style.display = 'none';
      document.getElementById('captureCameraWeb').style.display = 'block';
      document.getElementById('captureCameraWebBack').style.display = 'none';
      document.getElementById('captureGaleriaFront').style.display = 'none';
      document.getElementById('captureGaleriaBack').style.display = 'none';
      console.log("es celular " + navigator.userAgent);
    } else {
      console.log("es PC " + navigator.userAgent);
      document.getElementById('MovilContinue').style.display = 'block';
      document.getElementById('captureCameraWeb').style.display = 'none';
      document.getElementById('captureCameraWebBack').style.display = 'none';
    }

    let principalColor = this.getColorButton().replace('#', '');
    let secondarylColor = this.getColorLabel().replace('#', '');
    this.configSDK = {
      colorButton: principalColor,
      colorLabel: secondarylColor
      
    }

    this.email = sessionStorage.getItem("email");
    // mensajeError: false; 
  
    
    this.errorMensaje = this.errorSelfieService.returnMensaje();
    console.log('titulo= ' + this.titulo);
    /* this.actRoute.params.subscribe(params => {
      this.id = params['id'];
    }); */
    

   

    $("#MovilContinue").css("color","#6c757d");

    $("#galeryButton").hover(function(){
      var movilContinue = document.getElementById("MovilContinue") as HTMLInputElement;
      movilContinue.style.backgroundColor = "white"; 
      movilContinue.style.color = "#6c757d";
      });

      $("#MovilContinue").hover(function(){
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        

        });
        
        this.service = sessionStorage.getItem("serviceNumber");
        this.serviceName = sessionStorage.getItem('serviceIdMissionName');
        this.idType =  sessionStorage.getItem("serviceId"); 
        if(!this.idType || this.idType === null || this.idType === '' ) {
          let tipoId = sessionStorage.getItem('ti');
          if(tipoId && tipoId !== null && tipoId !== '') {
            if(tipoId === 'ID_CARD') {
              this.idType = 'VID';
            } else {
              this.idType = 'PP';
            }
          }
        }
        this.idType === "VID" ? this.tipoDcumento = "INE" : this.tipoDcumento = "PASAPORTE";
        //sessionStorage.getItem("serviceId") === "ID_CARD" ? idType = "VID" : idType = "PP";
        console.log("tipo de id", this.idType);

        if(sessionStorage.getItem("tokenIdMission")){
          this.token = sessionStorage.getItem('tokenIdMission');
        }else{
          let response = await this.idMission.getToken();
          this.token = response['data'].tokenId;
          console.log("token", this.token);
          sessionStorage.setItem('tokenIdMission', this.token);
          initializeSdk(this.token, this.service, this.idType, this.serviceName);
        }

        if(sessionStorage.getItem('intentsDocument')){
          this.intentsDocument = sessionStorage.getItem('intentsDocument');
        }else{
          sessionStorage.setItem('intentsDocument', '0');
          const device = this.analyticService.getTypeDevice();
          const initAnalytics = this.analyticService.initAnalytics("documento", device)
          console.log("El id es:",this.id);
          console.log("Lo que voy a guardar es:", initAnalytics);
          var result = await this.middle.updateDataUser(initAnalytics, this.id);
          console.log("El resultado es:", result);
          this.intentsDocument = sessionStorage.getItem('intentsDocument'); 
        } 

        this.modalSpiner = document.getElementById('modalSpinner');
        await this.spinner.hide();
  }

  async captureID() {
    sessionStorage.setItem("errorIdMission", '');
    this.mensajeError = false; 
    this.mensajeModal = "Cargando..."
    if(this.modalSpiner && this.modalSpiner.style) {
      this.modalSpiner.style.display = 'block';
    }
    setTimeout(() => {
      this.modalSpiner.style.display = 'none';
    }, 500);
    captureIDsdk(this.token, this.idType, this.configSDK, this.callbackFront, this.isMobileBool);
    // setTimeout(() => {
    //     document.getElementById("continue-IdCaptureIntruction").style.setProperty('background-color', this.configSDK.colorButton, 'important');
    //     document.getElementById("continue-IdCaptureIntruction").style.setProperty('color', this.configSDK.colorLabel, 'important');
    //   }, 300);

  }

  async captureIDBack() {
    sessionStorage.setItem("errorIdMission", '');
    this.mensajeError = false; 
    this.mensajeModal = "Cargando...";
    this.modalSpiner.style.display = 'block';
    setTimeout(() => {
      this.modalSpiner.style.display = 'none';
    }, 500);
    await captureIDBacksdk(this.token, this.service, this.idType, this.configSDK, this.callbackBack, this.isMobileBool);
  }


  callbackFront = async (response) => {
    sessionStorage.setItem("errorIdMission", '');
    this.mensajeError = false; 
    console.log("el response del callback front es:",response, this.token);
    this.mensajeModal = "";
    if(response['Status'] && response['Status'] === "SUCCESS") {
      sessionStorage.setItem("idFront", response.ImageData);
      if(this.tipoDcumento === "PASAPORTE"){
        this.modalSpiner.style.display = 'none';
        this.mensajeModal = "Cargando...";
        this.modalSpiner.style.display = 'block';
        await submitSdk(this.token, this.service, this.idType, this.id, this.email, this.callback, this.serviceName);
        // this.modalSpiner.style.display = 'none';
        // this.router.navigate([Rutas.ocrValidation]);
      }else{
        this.modalSpiner.style.display = 'none';
        this.mensajeModal = "";
        this.modalSpiner.style.display = 'block';
        this.titulo = this.tipoDcumento + " POSTERIOR";
        this.idcard = 'id-card-back';
        this.description = 'Tómale foto al reverso de tu  '+this.tipoDcumento+' vigente.';
        if(!this.isMobileBool) {
          this.description = 'Sube una imagen del reverso de tu '+this.tipoDcumento+' vigente.';
        }
        this.fotoFT = "../../../../../assets/img/animations/ineback.svg";
        this.actionsButtonsBack();
        setTimeout(() => {
          this.modalSpiner.style.display = 'none';
        }, 1000);
        
      }

    }else if(response['Status'] === "FAIL"){
      if(response['StatusMessage'] && response['StatusMessage'] === "Network Issue! Please try again."){
        return;
      }
      console.log("error FAIL");
      this.intentsDocument = parseInt(this.intentsDocument) + 1;
      sessionStorage.setItem('intentsDocument', this.intentsDocument);
     // this.modalSpiner.style.display = 'none';
      console.log("response error", response);
      if(response.data && response.data.statusCode === "6003"){
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission","Ocurrio un error, favor de volver a intentar");
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("Selfie", "false");
          this.router.navigate([Rutas.selfie]);
          sessionStorage.setItem("errorIdMission", 'Error');
          return;
        }else{
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          window.location.reload();
        }
        this.modalSpiner.style.display = 'none';
      }
      // this.mensajeError = true;
      if(this.object.servicios['selfie']){
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission", 'Error');
        sessionStorage.removeItem("Selfie");
        sessionStorage.setItem("Selfie", "false");
        this.modalSpiner.style.display = 'none';
        this.router.navigate([Rutas.instrucciones]);
        return;
      }else{
        this.modalSpiner.style.display = 'none';
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission", 'Error');
        window.location.reload();
      }
     // this.router.navigate([Rutas.selfie]);
    }

  }

  callbackBack= async (response) => {
    sessionStorage.setItem("errorIdMission", '');
    this.mensajeError = false; 
    //this.mensajeModal = "";
    //this.modalSpiner.style.display = 'none';
    this.mensajeModal = "Cargando...";
    console.log("el response del callback back es:",response, this.token);
    
    this.modalSpiner.style.display = 'block';
    if(response['Status'] && response['Status'] === "SUCCESS") {
      sessionStorage.setItem("idBack", response.ImageData);
      await submitSdk(this.token, this.service, this.idType,this.id, this.email, this.callback, this.serviceName);
    }
  }

  callback = async (response) => {
    console.log("el response del submit back es:",response, this.token);
    
    if(response && response['Status'] && response['Status'].Status_Code === "000") {
      if( !response.FormDetails ) {
        // this.mensajeError = true; 
        this.countAddAndSessionRemove();
        this.initViewDocument();
        this.modalSpiner.style.display = 'none';
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("Selfie", "false");
          this.router.navigate([Rutas.instrucciones]);
        }else{
          sessionStorage.removeItem("tokenIdMission");
          this.router.navigate([Rutas.documentInstruction]);
          //this.ngOnInit();
        }
        return;
      }
      sessionStorage.setItem("statusDocument", response.FormDetails.state)
      await this.saveOCR(response.FormDetails);
      await this.saveRulesIDMission(response.FormDetails);
      this.validateOCR();
      this.modalSpiner.style.display = 'none';
      return;
      //this.router.navigate([Rutas.ocrValidation]);
      //this.redirect();
    } else if(!response || response['Status'] === "FAIL") {
      if(sessionStorage.getItem("intentsIdMission") && sessionStorage.getItem("intentsIdMission") !== null) {
        console.log("IF del fail");
        var intentsIdMission = parseInt(sessionStorage.getItem("intentsIdMission"));
        intentsIdMission += 1 ;
        sessionStorage.setItem("intentsIdMission", intentsIdMission.toString());
        if(intentsIdMission === this.paramIntents){
          // setTimeout(() => {
          //   this.mensajeModal = "Has excedido el número de intentos, por favor actualiza tu información manualmente";
          //   this.modalSpiner = true;

          // }, 2000);
          this.router.navigate([Rutas.updateValues]);
          return;
        }else{
          if(this.object.servicios['selfie']){
            sessionStorage.removeItem("tokenIdMission");
            sessionStorage.removeItem("Selfie");
            sessionStorage.setItem("Selfie", "false");
            this.router.navigate([Rutas.instrucciones]);
          }else{
            sessionStorage.removeItem("tokenIdMission");
            // this.router.navigate([Rutas.documentInstruction]);
            window.location.reload();
            //this.ngOnInit();
          }
          return;
        }

      } else {
        sessionStorage.setItem("intentsIdMission", "1");
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("errorIdMission", 'Error');
          sessionStorage.setItem("Selfie", "false");
          this.router.navigate([Rutas.instrucciones]);
          return;
        } else {
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          // this.router.navigate([Rutas.documentInstruction]);
          window.location.reload();
          //this.ngOnInit();
        }
        sessionStorage.setItem("errorIdMission", 'Error');
        window.location.reload();
        // this.router.navigate([Rutas.documentInstruction]);
       // return;
      }

      
      if(response['StatusMessage'] && response['StatusMessage'] === "Network Issue! Please try again.") {
        this.mensajeModal = "Ocurrio un error";
        sessionStorage.removeItem("tokenIdMission");
        //sessionStorage.setItem("errorIdMission","Ocurrio un error, favor de volver a intentar");
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("Selfie", "false");
          this.router.navigate([Rutas.instrucciones]);
          return;
        }else{
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          // this.router.navigate([Rutas.documentInstruction]);
          window.location.reload();
          //this.ngOnInit();
        }
        return;
      }
      console.log("error FAIL");
      this.intentsDocument = parseInt(this.intentsDocument) + 1;
      sessionStorage.setItem('intentsDocument', this.intentsDocument);
      this.modalSpiner.style.display = 'none';
      console.log("response error", response);
      // if(response.data && response.data.statusCode === "6003"){
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission","Ocurrio un error, favor de volver a intentar");
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("Selfie", "false");
          this.router.navigate([Rutas.instrucciones]);
          return;
        }else{
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          // this.router.navigate([Rutas.documentInstruction]);
          window.location.reload();
         // this.ngOnInit();
        }
        
      // }
      // this.mensajeError = true; 
     // this.router.navigate([Rutas.selfie]);
    }
   // this.modalSpiner.style.display = 'block';
  }


  async saveOCR(formDetails){
    let infoExtractedData = formDetails['extractedIdData'];
    let infoPersonalData = formDetails['extractedPersonalData'];

    // var infoOcr = formDetails['Identity_Validation_and_Face_Matching'].IDmission_Image_Processing.ID_Image_Processing;
    // console.log("infoOCR", infoOcr); 
    if(!infoExtractedData || !infoPersonalData) {
      setTimeout(() => {
        this.mensajeModal = "Ocurrio un error";
        this.modalSpiner = true;
      }, 2000);
      this.countAddAndSessionRemove();
      if(this.object.servicios['selfie']){
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission", 'Error');
        sessionStorage.removeItem("Selfie");
        sessionStorage.setItem("Selfie", "false");
       // this.modalSpiner = false;
        this.router.navigate([Rutas.instrucciones]);
        return;
      }else{
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission", 'Error');
        // this.router.navigate([Rutas.documentInstruction]);
        window.location.reload();
        //this.ngOnInit();
      }
    }

    let addressLine2 = infoPersonalData.addressLine2;

    [this.zipCode, this.colonia] = this.getZipCodeAndColony(addressLine2);
    // if(this.validatorField( infoOcr.Postal_Code) !== '') {
    //   this.zipCode = this.validatorField(infoOcr.Postal_Code);
    // }
    let estado = '';
    let municipio = '';
    [estado, municipio] = this.getEdoAndMunicipy(infoPersonalData.addressLine3);
    const reqAddress = {
      calle: infoPersonalData.addressLine1,
      colonia: this.colonia,
      zip: this.zipCode,
      city: municipio,
      edo: estado,
      municipy: municipio
    };

    // let lastName2 = infoOcr.LastName2_Doc != "" ? this.validatorField(infoOcr.LastName2_Doc) : this.validatorField(infoOcr.Last_Name2);
    const reqName = {
      nombres: infoPersonalData.firstName_doc,
      aPaterno: infoPersonalData.lastName ,
      aMaterno: infoPersonalData.lastName2_doc,
      nacionalidad : infoExtractedData.nationality,
      genero : infoPersonalData.gender,
      fechaDeNacimiento : infoPersonalData.dob, 
      vigencia : infoExtractedData.idExpirationDate,
      // edad : this.validatorField(infoOcr.CalculatedAGE),
      curp : infoExtractedData.idNumber1
    };

    if(infoExtractedData.idNumber  && infoExtractedData.idNumber != '' && infoExtractedData.idNumber2 &&infoExtractedData.idNumber2 != '') {
      sessionStorage.setItem('cic', infoExtractedData.idNumber);
      sessionStorage.setItem('identificadorCiudadano', infoExtractedData.idNumber2.substring(4));
    }

    sessionStorage.setItem("reqAddress", JSON.stringify(reqAddress));
    sessionStorage.setItem("reqName", JSON.stringify(reqName));

    sessionStorage.setItem('nombres', reqName.nombres ? reqName.nombres : '');
    sessionStorage.setItem('aPaterno', reqName.aPaterno ? reqName.aPaterno : '');
    sessionStorage.setItem('aMaterno', reqName.aMaterno ? reqName.aMaterno : '');
    sessionStorage.setItem('curp', reqName.curp ? reqName.curp : '');
    sessionStorage.setItem('calle', reqAddress.calle ? reqAddress.calle : '');
    sessionStorage.setItem('zip', this.zipCode ? this.zipCode : '');
    sessionStorage.setItem('city', reqAddress.city ? reqAddress.city : '');
    sessionStorage.setItem('edo', reqAddress.edo ? reqAddress.edo : '');
    sessionStorage.setItem('colonia', this.colonia ? this.colonia : '');
    sessionStorage.setItem('municipio', reqAddress.municipy ? reqAddress.municipy : '');
    sessionStorage.setItem('numero' , 'S/N');
  }

  saveRulesIDMission(formDetails){
    if(formDetails) {
      const rulesFace = {
        eyeConvering: formDetails['eyeCovering'],
        faceMask: formDetails['faceMask'],
        faceMaskScore : formDetails['faceMaskScore'],
        faceVerificationResult : formDetails['faceVerificationResult'],
        faceHeadCovering : formDetails['headCovering'],
        liveFace : formDetails['liveFace'],
        verificationResult : formDetails['verificationResult']
      }
      sessionStorage.setItem("rules", JSON.stringify(rulesFace));
    }else{
      sessionStorage.setItem("rules", "no se pudo recuperar las reglas");
    }

    if( formDetails ) {
      // let dataDocumentOCR = formDetails['Identity_Validation_and_Face_Matching'].IDmission_Image_Processing.ID_Image_Processing;
      const rulesDocumentOCR = {
        // RealnessCheck_LABEL_ID_BACK: this.validatorField(dataDocumentOCR.RealnessCheck_LABEL_ID_BACK),
        // RealnessCheck_LABEL_ID_FRONT: this.validatorField(dataDocumentOCR.RealnessCheck_LABEL_ID_FRONT),
        verificationResult:formDetails.verificationResult
      }

      sessionStorage.setItem("rulesDocumentoOCR", JSON.stringify(rulesDocumentOCR));
    }

  }

  getZipCodeAndColony(addressLine2){
    let zipCode = '';
    let colony = '';
    if(addressLine2) {
      let idx = addressLine2.search(/\d{5}/);
      if (addressLine2 !== '' && idx >= 0) {
        zipCode = addressLine2.substr(idx, idx + 5);
        colony = addressLine2.substr(0, addressLine2.length -5);
        console.log("zip,", zipCode);
        console.log("colony,", colony);
      }
    }
    return [zipCode , colony];
  }

  getEdoAndMunicipy(addressLine3) {
    let edo = addressLine3;
    let municipy = addressLine3;
    if(!addressLine3) {
      return ['',''];
    }
    let resultados = addressLine3.split(',');
    console.log('los resultados son: ' , resultados);
    if(resultados.length > 1) {
      edo = resultados[1];
      municipy = resultados[0];
    }
    //if(edo && edo.length > 3 ) {
    //  edo = edo.substring(edo.length - 3, edo.length);
    //  console.log('el edo fue: ' , edo);
    //}
    if(edo === 'DMX' || edo === '.F.') {
      edo = 'CDMX';
    }
    if(edo === 'ORO' || edo === 'QUE') {
      edo = 'QRO';
    }
    if(edo === 'CAM' || edo === 'AMP') {
      edo = 'CAMP';
    }
    return [edo, municipy];
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if (this.object.daon.identity === true) {
        this.router.navigate([Rutas.livenessInstruction]);
        return false;
      } else {
        return true;
      }
    }
  }


  back() {
    this.router.navigate([Rutas.selfieVerification]);
  }

  captueWCamera() {
    this.intentsDocument =  parseInt(this.intentsDocument) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);
    this.router.navigate([Rutas.documentCapture]);
  }

  movilContinue() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'block';
  }



  async validationFlow(response){

    if(response['Status'] && response['Status'] === "SUCCESS"){
      sessionStorage.setItem("idFront", response.ImageData);
      //Document type
      if(this.tipoDcumento === "PASAPORTE"){
        console.log("el tipo de identificadion ens:", this.idType);
        await submitSdk(this.token, this.service, this.idType, this.id, this.email,  this.callback, this.serviceName);

        //this.modalSpiner.style.display = 'none';
        //this.router.navigate([Rutas.ocrValidation]);
      }else{
        this.titulo = this.tipoDcumento + " POSTERIOR";
        this.idcard = 'id-card-back';
        this.description = 'Tómale foto al reverso de tu '+this.tipoDcumento+' vigente.';
        if(!this.isMobileBool) {
          this.description = 'Sube una imagen del reverso de tu '+this.tipoDcumento+' vigente.';
        }
        this.fotoFT = "../../../../../assets/img/animations/ineback.svg";
        this.actionsButtonsBack();

        this.modalSpiner.style.display = 'none';
      }

      
    }


    if(response['Status'] && response['Status'] === "SUCCESS"){
      if(!response || !response.FormDetails || 
        !response.FormDetails['Identity_Validation_and_Face_Matching'].IDmission_Image_Processing.ID_Image_Processing){
          // this.mensajeError = true;
          this.countAddAndSessionRemove();
          this.initViewDocument();
          this.modalSpiner.style.display = 'none';
          if(this.object.servicios['selfie']){
            sessionStorage.removeItem("tokenIdMission");
            sessionStorage.removeItem("Selfie");
            sessionStorage.setItem("Selfie", "false");
            this.router.navigate([Rutas.instrucciones]);
          }else{
            sessionStorage.removeItem("tokenIdMission");
            this.router.navigate([Rutas.documentInstruction]);
            //this.ngOnInit();
          }
          return;
      }else{
        await this.saveOCR(response.FormDetails);
        this.validateOCR();
        this.modalSpiner.style.display = 'none';
        
      }

    }else if(response['Status'] === "FAIL"){
      this.intentsDocument = parseInt(this.intentsDocument) + 1;
      sessionStorage.setItem('intentsDocument', this.intentsDocument);
      if(response['StatusMessage'] && response['StatusMessage'] === "Network Issue! Please try again."){
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission","Ocurrio un error, favor de volver a intentar");
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("Selfie", "false");
          this.modalSpiner.style.display = 'none';
          this.router.navigate([Rutas.instrucciones]);
          return;
        }
      }
      
      if(response.data && response.data.statusCode === "6003"){
        sessionStorage.removeItem("tokenIdMission");
        sessionStorage.setItem("errorIdMission","Ocurrio un error, favor de volver a intentar");
        if(this.object.servicios['selfie']){
          sessionStorage.removeItem("tokenIdMission");
          sessionStorage.setItem("errorIdMission", 'Error');
          sessionStorage.removeItem("Selfie");
          sessionStorage.setItem("Selfie", "false");
          this.modalSpiner.style.display = 'none';
          this.router.navigate([Rutas.instrucciones]);
        }else{
          sessionStorage.removeItem("tokenIdMission");
          this.router.navigate([Rutas.documentInstruction]);
        }
        
      }
      // this.mensajeError = true; 
     // this.router.navigate([Rutas.selfie]);
    }
  }

  async validateOCR(){
    if(sessionStorage.getItem("nombres") === "" || sessionStorage.getItem("aPaterno") === "" ||
    sessionStorage.getItem("aMaterno") === "" || sessionStorage.getItem("curp") === "" ||
    sessionStorage.getItem("calle") === "" || sessionStorage.getItem("zip") === "" ||
    sessionStorage.getItem("city") === "" || sessionStorage.getItem("edo") === "" ||
    sessionStorage.getItem("colonia") === ""  ){
      this.router.navigate([Rutas.updateValues]);
    }else{
      this.ngZone.run(() => this.router.navigate([Rutas.ocrValidation])).then();
      //this.router.navigate([Rutas.ocrValidation]);
    }

  }

  countAddAndSessionRemove(){
    this.intentsDocument = parseInt(this.intentsDocument) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);
    sessionStorage.removeItem("Selfie");
    sessionStorage.removeItem("idFront");
    sessionStorage.removeItem("statusSelfie"); 
    sessionStorage.removeItem("selfieImage");
    sessionStorage.setItem("Selfie", "false");

    /** OCR */

    sessionStorage.removeItem('curp');
    sessionStorage.removeItem('calle');
    sessionStorage.removeItem('numero');
    sessionStorage.removeItem('colonia');
    sessionStorage.removeItem('zip');
    sessionStorage.removeItem('city');
    sessionStorage.removeItem('edo');
    sessionStorage.removeItem('municipio');

    sessionStorage.removeItem('nombres');
    sessionStorage.removeItem('aPaterno');
    sessionStorage.removeItem('aMaterno');

  }

  initViewDocument(){
    this.titulo = this.tipoDcumento + " FRONTAL";
    this.idcard = 'id-card-front';
    this.description = 'Tómale foto o sube tu '+this.tipoDcumento+' por la parte frontal en JPG o JPEG.';
    this.actionsButtonsFront();
  }

  actionsButtonsFront(){
    if (this.isMobileBool) {
      (document.getElementById("captureGaleriaFront") as HTMLElement).style.display = "block";
      (document.getElementById("captureCameraWeb") as HTMLElement).style.display = "block";

      (document.getElementById("captureGaleriaBack") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWebBack") as HTMLElement).style.display = "none";
      document.getElementById('MovilContinue').style.display = 'none';
      console.log("es celular " + navigator.userAgent);
    } else {
      console.log("es PC " + navigator.userAgent);
      (document.getElementById("captureGaleriaFront") as HTMLElement).style.display = "block";
      document.getElementById('MovilContinue').style.display = 'block';
      (document.getElementById("captureGaleriaBack") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWebBack") as HTMLElement).style.display = "none";
      

    }
  } 

  actionsButtonsBack(){
    if (this.isMobileBool) {
      (document.getElementById("captureGaleriaFront") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWeb") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWeb") as HTMLElement).style.display = "none";
      document.getElementById('MovilContinue').style.display = 'none';

      (document.getElementById("captureGaleriaBack") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWebBack") as HTMLElement).style.display = "block";
      console.log("es celular " + navigator.userAgent);
    } else {
      console.log("es PC " + navigator.userAgent);
      document.getElementById('MovilContinue').style.display = 'block';
      (document.getElementById("captureGaleriaBack") as HTMLElement).style.display = "block";

      (document.getElementById("captureGaleriaFront") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWeb") as HTMLElement).style.display = "none";
      (document.getElementById("captureCameraWebBack") as HTMLElement).style.display = "none";
    }
  } 

  async aceptar() {
    await this.spinner.show();
    this.router.navigate([Rutas.codeQr]);
    await this.spinner.hide();
  }

  async cerrarModal() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'none';
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

}

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ServicesGeneralService, isMobile } from '../../../services/general/services-general.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import IconDefinitions from '../../../../assets/icons/icons-svn';
import { ErrorSelfieService } from 'src/app/services/errores/error-selfie.service';
import { environment } from '../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../model/sesion/SessionPojo';
import { TermsComponent } from '../../terms/terms.component';
import { GlobalInstructionComponent } from '../../global-instruction/global-instruction.component';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';

@Component({
  selector: 'app-instrucciones-reverso-manual',
  templateUrl: './instrucciones-reverso-manual.component.html',
  styleUrls: ['./instrucciones-reverso-manual.component.css']
})
export class InstruccionesReversoManualComponent implements OnInit {

  errorMensaje: string;
  titulo: string;
  description: string;
  id: string;
  dc: any;
  mensaje: string;
  img: any;
  fotoFT: string;
  idcard: any;
  icon: IconDefinitions;
  object: sesionModel;
  isMobileBool: boolean;
  intentsDocument;
  mensajeError: boolean;
  file: File;
  b64;
  tipoDcumento: string;
  cameraCapture: boolean;
  captureFoto: boolean;
  showGaleryOption = false;
  mensajeFormato: boolean;


  constructor(public router: Router, public serviciogeneralService: ServicesGeneralService, private actRoute: ActivatedRoute,
    private session: SessionService, private spinner: NgxSpinnerService, private errorSelfieService: ErrorSelfieService,
    private term: TermsComponent, private global: GlobalInstructionComponent, private middleDaon: MiddleDaonService) 
    {}

  async ngOnInit() {

    let tipoDcumento = "INE";
    console.log(this.serviciogeneralService.getTipoIdentificacion());
    console.log(sessionStorage.getItem('ti'));
    if (this.serviciogeneralService.getTipoIdentificacion() !== undefined) {
      sessionStorage.setItem('ti', this.serviciogeneralService.getTipoIdentificacion());
      if (this.serviciogeneralService.getTipoIdentificacion() === 'PASSPORT') {
        tipoDcumento = "PASAPORTE";
      }
    } else {
      if (sessionStorage.getItem('ti') === 'ID_CARD') {
        tipoDcumento = "INE";
      } else if (sessionStorage.getItem('ti') === 'PASSPORT') {
        tipoDcumento = "PASAPORTE";
      }
    }

    this.titulo = tipoDcumento === 'PASAPORTE' ? tipoDcumento : tipoDcumento + " POSTERIOR";
    this.description = 'Tómale foto o sube tu ' + tipoDcumento + ' por la parte posterior en formato JPG o JPEG';
    if (tipoDcumento === "PASAPORTE") {
      this.fotoFT = "../../../../../assets/img/svg/pasaporte.svg"
    } else {
      this.fotoFT = "../../../../../assets/img/animations/ine.svg";
    }


    if (sessionStorage.getItem('intentsDocument')) {
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    } else {
      sessionStorage.setItem('intentsDocument', '0')
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    }
    await this.spinner.show();
    this.errorMensaje = this.errorSelfieService.returnMensaje();
    console.log('titulo= ' + this.titulo);
    this.actRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    const fp = await FP.load({ token: environment.fingerJsToken, 
 endpoint: environment.fpDomain });
    fp.get({ tag: { 'tag': this.id } });
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    this.showButtons();
    await this.spinner.hide();



  }

  async acceptImage() {
    this.spinner.show();
    let nombreDelDocumento = 'Voting Card Back';
    if(this.tipoDcumento === "PASAPORTE") {
      nombreDelDocumento = 'Passport';
    }
    const jsonSendFaceDaon = {
      data: this.b64,
      type: nombreDelDocumento,
      capturaManual: true
    };
    const resultCode = await this.middleDaon.sendInfoDaon(jsonSendFaceDaon, this.id, 'document');
    this.spinner.hide();
    if (resultCode !== 200) {
      this.intentsDocument =  parseInt(this.intentsDocument) + 1;
      sessionStorage.setItem('intentsDocument', this.intentsDocument);
      console.log('ocurrio un error, favor de reintentar');
      this.errorSelfieService.mensaje = 'Error, favor de volver a intentar';
      return;
    }


    if(this.tipoDcumento === 'INE') {
      //Save image
      console.log("la imagen", this.b64);
      this.router.navigate([Rutas.instruccionesReversoManual]);
    } else {
      this.router.navigate([Rutas.capturaDatos]);
    }
  }

  showButtons() {
    this.isMobileBool = isMobile(navigator.userAgent);
    document.getElementById('MovilContinue').style.display = 'none';
    if (this.isMobileBool) {
      document.getElementById('MovilContinue').style.display = 'nome';
      document.getElementById('captureCameraWeb').style.display = 'block';
      console.log("es celular " + navigator.userAgent);
    } else {
      console.log("es PC " + navigator.userAgent);
      document.getElementById('MovilContinue').style.display = 'block';
      document.getElementById('captureCameraWeb').style.display = 'none';

    }

    $("#MovilContinue").css("color", "#6c757d");

    $("#galeryButton").hover(function () {
      var movilContinue = document.getElementById("MovilContinue") as HTMLInputElement;
      movilContinue.style.backgroundColor = "white";
      movilContinue.style.color = "#6c757d";
    });

    $("#MovilContinue").hover(function () {
      var colorHover = sessionStorage.getItem("colorButton");
      var colorLabelHover = sessionStorage.getItem("colorLabel");
      $(this).css("background-color", colorHover);
      $(this).css("color", colorLabelHover);
    });
  }

  async stopCameraJs() {
    const video = document.querySelector('video');
    if(!video) {
      return ;
    }
// A video's MediaStream object is available through its srcObject attribute
const mediaStream = video.srcObject;
    if(!mediaStream) {
      return;
    }
// Through the MediaStream, you can get the MediaStreamTracks with getTracks():
const tracks = (<MediaStream>mediaStream).getTracks();

// Tracks are returned as an array, so if you know you only have one, you can stop it with: 
    if( tracks && Array.isArray(tracks) ) {
      tracks.forEach(item => {item.stop()});
    }
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if (this.object.daon.identity === true) {
        this.router.navigate([Rutas.livenessInstruction]);
        return false;
      } else {
        return true;
      }
    }
  }

  async deleteData() {
    this.file = undefined;
    this.b64 = undefined;
    await this.delay(100);
    this.showButtons();
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async captueWCamera() {
    this.spinner.show();
    this.intentsDocument = parseInt(this.intentsDocument) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);
    this.captureFoto = true;
    const constraints = {
      video: {
        facingMode: {
          exact: 'environment'
        }
      },
      };
    console.log("hasta aqui estoy bien");
      try {
        const stream = await navigator.mediaDevices.getUserMedia(constraints);
        console.log('pase el stream');
        var $video = document.getElementById("video") as HTMLMediaElement;
        $video.srcObject = stream;
        $video.play();
      } catch (error) {
        console.log(error);
      }

    
    this.spinner.hide();

    // this.router.navigate([Rutas.documentCapture]);
  }

  movilContinue() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'block';
  }

  async processFile(imageInput: any) {
    await this.spinner.show();
    this.intentsDocument = parseInt(this.intentsDocument) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);

    try {
      this.mensaje = '';
      this.file = imageInput.files[0];
      console.log("type file", this.file.type);
      if(this.file.type === "image/jpeg" || this.file.type === "jpg"){
        this.mensajeFormato = false;
        console.log(this.file);
        this.b64 = await this.readAsDataURL(this.file);
        console.log(this.b64);
        
      }else{
        console.log('formato no aceptado: ');
        this.mensajeFormato = true;
      }

    } catch (e) {
      console.log('Error en el servicio intente de nuevo: ', e);
      this.mensaje = 'Error en el servicio intente de nuevo ';
    }
    await this.spinner.hide();
  }

  readAsDataURL(file: File) {
    return new Promise((resolve, reject) => {
      const fr = new FileReader();
      fr.onerror = reject;
      fr.onload = () => {
        resolve(fr.result);
      }
      fr.readAsDataURL(file);
    });
  }

  async aceptar() {
    await this.spinner.show();
    this.router.navigate([Rutas.codeQr]);
    await this.spinner.hide();
  }

  async enter() {
    this.spinner.show();
    var canvas = document.getElementById('canvas') as HTMLCanvasElement; 
    var video = document.getElementById('video') as HTMLVideoElement;
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height); 
    let image = canvas.toDataURL();
    await this.stopCameraJs();
    console.log(image);
    this.b64 = image;
    this.captureFoto = false;
    this.spinner.hide();
  }

  async cerrarModal() {
    const modal = document.getElementById('modalConfirm');
    modal.style.display = 'none';
  }

  public getColorButton() {
    return this.global.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.global.getLabelColorSystem();
  }

  public getImageSystem() {
    return this.global.getImageConfig();
  }
}

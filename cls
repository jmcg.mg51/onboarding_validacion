[33mcommit 39d674d3f8c3d104851129f9882a72e44f764c0d[m[33m ([m[1;36mHEAD -> [m[1;32midMission[m[33m, [m[1;31morigin/idMission[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Mar 2 10:06:16 2022 -0600

    - Se actualiza sdk

[33mcommit 3f5fb6bc51604ec58636dba574b3c14e67053f5f[m
Author: Ivan Ake <edgardoake@gmail.com>
Date:   Mon Feb 28 13:56:22 2022 -0600

    - Configuracion dinamica de servicio y tipo de identificacion (parcial)

[33mcommit 338c8352e19b7e5daf70bbaee870ccbd747cb36e[m[33m ([m[1;31morigin/idMissionPruebas[m[33m, [m[1;32midMissionPruebas[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Feb 28 11:49:24 2022 -0600

    - Cambios de sdk

[33mcommit 2d3f313f6243739604c38d36e68744c148ab1abd[m
Merge: 6e6a23f 7d226a3
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Feb 21 18:08:59 2022 -0600

    Merge branch 'idMission' of https://mitgitlab.mit.com.mx/desarrollo/terceros/onboarding/mit_onboarding_validacion into idMission

[33mcommit 6e6a23fe3e9dd128c7ff645c9717a3595bafccfe[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Feb 21 18:07:37 2022 -0600

    - se agregan variables para token

[33mcommit 7d226a3111221cb941134f1a7fa07c6bc4d82492[m
Author: Ivan Ake <edgardoake@gmail.com>
Date:   Thu Feb 17 02:03:50 2022 -0600

    - Se crea archivo para llamar a los servicios de idMission

[33mcommit 1c62b8ca4130b51476e6518e128e8017f93b2742[m
Author: Ivan Ake <edgardoake@gmail.com>
Date:   Thu Feb 17 01:44:48 2022 -0600

    - Se elimina declaracion en el sdk

[33mcommit de8c815edde64e87bb0ec83227d3836bbcfa650f[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Feb 11 14:04:20 2022 -0600

    - Recuperar valor de OCR y toma de documento

[33mcommit e9dbc824a05ca04116ab400b7628129e8864a4f5[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Feb 3 11:59:14 2022 -0600

    - integrar idMission

[33mcommit 15ec793694d3426221c07aac3d5a6d90197090f4[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Feb 3 11:58:32 2022 -0600

    - Integrar proveedor IDMission

[33mcommit 10a200e6813d791a9708d423da8a756085770554[m[33m ([m[1;31morigin/develop[m[33m, [m[1;32mdevelop[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Jan 11 11:55:30 2022 -0600

    - FIX: correcion de ofertas que no traen gps

[33mcommit 1073d54aef928a4b6d240a6cc2be88ce434ba951[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;32mmaster[m[33m)[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Wed Dec 29 17:01:59 2021 -0600

    FEAT: se cambia leyenda en la pantalla de envio de pin para ficoscore

[33mcommit 34ffb0059b917bd70ee2eb7002b8dd809783ca0d[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Wed Dec 29 10:13:29 2021 -0600

    FIX: correccion de mensajito en las pantallas de circulo de credito

[33mcommit b50aab04c73ea39acbf1ebc0279b43cd9034c5fd[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Dec 28 17:39:35 2021 -0600

    FIX: se elimina llamada a funcion desde el html

[33mcommit 39ba2b398de26c4a330bfb5688cf36ec01c9110b[m[33m ([m[1;31morigin/sprint_42[m[33m, [m[1;32msprint_42[m[33m)[m
Merge: 6a050d8 656b0e6
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Dec 27 10:53:20 2021 -0600

    Merge remote-tracking branch 'origin/develop' into sprint_42

[33mcommit 6a050d867727cc49147df339477a0409a02966ed[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Nov 12 10:08:45 2021 -0600

    mensaje en la localizacion

[33mcommit 24108c40a51dd53d2b8bf81ddffa7c8428eaebe1[m[33m ([m[1;31morigin/sprint_41[m[33m, [m[1;32msprint_41[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Oct 26 15:17:30 2021 -0500

    - Se agregan mensajes de para activar gps

[33mcommit 91b22f3dfeba233ada6c5a27af7832b51ea906e6[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Oct 20 13:00:21 2021 -0500

    - Se agrega obtención de ubicación por gps

[33mcommit 4cec43e47ef2352eb9bd0aa1c0a7d7c4b98166d4[m[33m ([m[1;31morigin/sprint_40[m[33m)[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Oct 18 10:58:24 2021 -0500

    FEAT: nueva version de FP y servicio del bot

[33mcommit 656b0e676807c080837aa64bfdfcd611f301cde6[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Oct 6 14:03:09 2021 -0500

    - Se cambia quality selfie en QA y PROD

[33mcommit e88c76d0255978e26c71a9a77908cc9fa6e6664a[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Oct 5 10:21:13 2021 -0500

    - Se cambia la url de la versión de faceQuality para dev

[33mcommit c9bdeb5097fdaa44ee5afecff2f613bb0bc57d00[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Oct 4 13:06:04 2021 -0500

    - se agrega validacion de documento horizontal en el componente documento

[33mcommit 7eb836e3d140774d71fbe4e4241db21b24310e8d[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Oct 4 12:26:32 2021 -0500

    Cambios en el texto de terminos

[33mcommit edad97b4a8b07fbe32aae7130be2d8e105c7b785[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Sep 27 11:16:51 2021 -0500

    - Se cambia la leyenda de maximo peso en los archivos adjuntos

[33mcommit b2267cde4a301a89d078080be1de79322bdc7280[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Sep 24 10:29:40 2021 -0500

    Se cambian textos de terminos de uso

[33mcommit 0851dcd5f7bde7d9e3c39f6c2d04f3988aa54920[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Thu Sep 23 10:49:17 2021 -0500

    se corrige texto de politica de privacidad

[33mcommit 9248131012be39c83ed95300efc7735b42cecc3a[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Sep 21 11:27:16 2021 -0500

    - Se elimina el mapeo de mensajes en selfie

[33mcommit 5bc1800dc91ed9cbe8b8abb66e9b5be7c5a406c6[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Sep 20 09:37:29 2021 -0500

    - Se agrega maximo de subida en los archivos pdf

[33mcommit 1584a4809b997ceb26375d0cc36af6d63f233f7a[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Sep 17 14:36:03 2021 -0500

    - Cambio de leyenda en el envio de pin en la firma digital

[33mcommit 14fa7ab135f800eface756dada820f9dd89ef3c0[m
Merge: ceddc1d fae0978
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Sep 15 11:23:24 2021 -0500

    Merge branch 'sprint_37' into develop

[33mcommit ceddc1de61ad676094ad8a8af420fdceb9d55cb9[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Sep 10 12:52:32 2021 -0500

    Se modifican tenants de DAON

[33mcommit fae0978a8a65fbd44e55f50a95f63aa46369bed9[m[33m ([m[1;31morigin/sprint_37[m[33m, [m[1;32msprint_37[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Sep 7 11:49:05 2021 -0500

    - Descomentar código de obtencion de trackId

[33mcommit cd6db81b3282e675384ed5b23d8cac07669a4d85[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Mon Sep 6 10:12:39 2021 -0500

    - Elimina el guardo de CVV en  tarjeta bancaria

[33mcommit a0289c8f49f2abb0350bf28522ea5af59c62a1c3[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Sep 1 18:39:04 2021 -0500

    - Se agrega links para visualizar politica de privacidad en firma digital
    - Se modifican los textos de aceptacion en terminos y firma digital

[33mcommit dbdcb6783f23871a8f94d2d7bb3580da2449960c[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Sep 1 17:39:10 2021 -0500

    - Se agrega nodo de tarjeta bancaria.
    - Se modifican los archivos de terminos y condiciones

[33mcommit 40aa323fbcd986838daa8a3b869f36b85341cdfa[m
Merge: 171da30 8379204
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Aug 30 10:06:07 2021 -0500

    Merge remote-tracking branch 'origin/sprint_36' into develop

[33mcommit 83792045ff8f260f15677a74e7ee543acf296bab[m[33m ([m[1;31morigin/sprint_36[m[33m, [m[1;32msprint_36[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Aug 10 12:36:57 2021 -0500

    - Reducir el número de intentos para mostrar la opción saltar paso en captura de selfie.

[33mcommit 171da3064e549e52e70b09fa1d12a710b9fbd41c[m
Merge: 0765a6d 4781999
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Tue Aug 3 16:47:03 2021 -0500

    Merge branch 'develop' of https://mitgitlab.mit.com.mx/desarrollo/terceros/onboarding/mit_onboarding_validacion into develop

[33mcommit 0765a6d140b5079a57a422b58febb8d6daaa3df9[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Tue Aug 3 16:46:29 2021 -0500

    se cambia tenant a emea

[33mcommit 4781999193661f171e16c81ac7e6f35fe96b7e23[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Jul 30 09:29:55 2021 -0500

    - Se agrega mensaje del formato permitido en el componente archivos.

[33mcommit 112349de7fa170c9c5ac1aae2c67ec25ea6d4766[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Jul 27 17:34:23 2021 -0500

    Se cambia el texto ¿De acuerdo? por ¿Estás de acuerdo? en el modal de saltar paso.

[33mcommit df9f023085fe46200d51255e7ea060043f7f671e[m[33m ([m[1;31morigin/correcciones2[m[33m, [m[1;32mcorrecciones2[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Jul 27 11:15:15 2021 -0500

    -Se eliminan guiones en el nombre de archivos y la palabra terminos en el componente de archivos adjuntos

[33mcommit 9ea68bb38970fb016901b33283d63abbd29541d3[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Jul 26 19:33:29 2021 -0500

    se hacen valdiaciones del boton cancelar

[33mcommit 6e25b03d651b75103f40325a74dc7d9ea20bdd66[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Jul 26 19:24:37 2021 -0500

    se suben correcciones para validar el archivo y su extencion

[33mcommit a8c9c26f3b90f1818e3a45271b25b0b7071f89ac[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Jul 26 18:00:57 2021 -0500

    se suben correcciones

[33mcommit 990d011ac68019890abc22e5ec04860af577c099[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Wed Jul 21 17:21:28 2021 -0500

    se modifica un stirng en un html

[33mcommit f1c973e4d92c9ce7d667dc3bbcd17945c287cb43[m[33m ([m[1;31morigin/sprint_33[m[33m, [m[1;32msprint_33[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Jul 16 23:47:57 2021 -0500

    - Correción del texto de modal skip

[33mcommit 11da6bb8ec66e2b16b2c280d1f29dc7fb452835e[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Jul 16 23:35:25 2021 -0500

    -Correción comprobante de domicilio

[33mcommit 4302d63167fa003965878844ccbc38cead4f342c[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Jul 15 10:12:59 2021 -0500

    - Corrreción de mensaje de skip en selfie

[33mcommit 687ffc6a410d5286d93788dd348efd2b31c75da9[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Thu Jul 15 10:01:23 2021 -0500

    log de prueba

[33mcommit dbcb4657ee340d2eddff3cc22875b2c87e33e0fe[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Fri Jul 9 10:02:14 2021 -0500

    se suben correcciones

[33mcommit ddec3546a66c1b7683a659c028b9acf9d20145a7[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Jul 7 11:22:21 2021 -0500

    - Se agrega nodo de datos adicionales

[33mcommit 249b3d41204754f97aadafb3a3d6512b4dfb0cd1[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Thu Jul 1 10:47:07 2021 -0500

    se actualiza codigo de firma digital y ficoScore para el contrato y la inyeccion de datos

[33mcommit f36e719341d5317265b7dc88a102652286286364[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Jun 28 18:07:37 2021 -0500

    se cambian intentos a 3

[33mcommit 8967cde5693496a7f9cb22c336ff9e348901fbec[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Jun 28 17:57:42 2021 -0500

    se cambia modal de liveinstruction

[33mcommit d4780e8c3f43ffa53b9cff0958ececbfb36ef253[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon Jun 28 17:30:26 2021 -0500

    se cambia forma de apertura en comprobante de domicilio

[33mcommit 81ca06550622a46e60890e64346104143e43a7c7[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Jun 24 23:33:51 2021 -0500

    - Se corrige camara en comprobante de domicilio

[33mcommit 9d2a5c0dce2cd55fded821124126f7574175dbb2[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Jun 24 23:31:32 2021 -0500

    - Se corrige modal de smart skip en prueba de vida

[33mcommit f4f867cbcf8eb0f4149c240142349c5979c05adb[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Jun 24 23:10:41 2021 -0500

    FIX: mostrar buton smart skip despuesde N cantidad de intentos en selfie

[33mcommit d6644ac91f17b0910af83a19badd3f721b24e02a[m[33m ([m[1;32midMission_sdk[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Jun 23 16:24:41 2021 -0500

    - Se elimina el hidden de smartskip.

[33mcommit a92807342798c5e45d0ddcbf1655f2dcaccd391e[m
Merge: 48ccf8a 99763fe
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Wed Jun 23 13:56:21 2021 -0500

    se hace merge con sprint_31

[33mcommit 48ccf8aacae3ab2483f56b151d141108acaeab72[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Wed Jun 23 13:46:24 2021 -0500

    se hacen cambios en el componente de cuenta clabe

[33mcommit 99763fec26ae2cd305d077589eedecf61ce8af56[m[33m ([m[1;31morigin/sprint_31[m[33m, [m[1;32msprint_31[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Sat Jun 12 14:13:46 2021 -0500

    - Se corrigen estilos de popup de terminos y condiciones en componente de ficoscore y firma digital

[33mcommit 865b4b448afdf867871afc78261b958912de2799[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri Jun 11 09:27:19 2021 -0500

    - Cambio de endpoints

[33mcommit 6cc6a00b95e287d5c4795e255f3db4257f0cb14c[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu Jun 10 09:22:42 2021 -0500

    Correción al reemplazar el nombre del comercio en el pdf.

[33mcommit debf293df1ff1e782b43a65c1cdadbc19a52d475[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Tue Jun 8 17:34:09 2021 -0500

    Firma electronica, envio pin y terminos y condiciones

[33mcommit 0c844e9e97f0d703ef776ecbfe4798313c19c9c2[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed Jun 2 12:08:29 2021 -0500

    - Se agregar modal para confirmar el skipSmart en nodo de selfie y prueba de vida.

[33mcommit 787c77115b7bc09f028e0b3249650eb32d4e9dcf[m[33m ([m[1;31morigin/sprint_30[m[33m, [m[1;32msprint_30[m[33m)[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri May 28 11:38:47 2021 -0500

    - Se modifica la ubicación y texto del smartSkip.

[33mcommit a9982eb31c13d76b050a8b778790dd58710eeee8[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri May 28 11:12:41 2021 -0500

    - Se actualizan los intents de prueba de vida

[33mcommit 299fdba1a4bb2c580710a14d295529e51c87ddaf[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed May 26 16:55:40 2021 -0500

    - Se corrige error de update analitica en componente que no se debe actualizar

[33mcommit 28d3cccab2d670961ace0e107ca0cf8a568cd8bf[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Wed May 26 09:50:53 2021 -0500

    - SmartSkip para el componente de selfie y prueba de vida

[33mcommit 8bc99ae01bd6a5040114e8bb5a6608e47df5afe1[m
Author: José Miguel De La Concha García <jdelaconcha@mit.local>
Date:   Mon May 24 13:57:53 2021 -0500

    se cambian valores del budget

[33mcommit 2a5b14d928d22364879034faf0c0e03024daa893[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri May 21 10:09:39 2021 -0500

    - Añadir cerrar modal en la parte posterior del modal y hacer más grande el icono de tache en el componente terminos y condiciones

[33mcommit 166a37aa97519ad7f70da6d995a7423be247b550[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Fri May 21 09:17:18 2021 -0500

    - Saltar nodo  (parcial)

[33mcommit ea07f414f1e5786242e47399477562e3542b3652[m
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu May 20 18:26:41 2021 -0500

    FIX: se añade nombre de comercio en pdf cuando INE no esta habilitado

[33mcommit 60bae5fa12e644869cb4cd5b90c496f4536b8950[m
Merge: 1b07738 17b8f3a
Author: Ivan <ivan.ake@mitec.com.mx>
Date:   Thu May 20 18:25:12 2021 -0500

    Merge branch 'develop' of https://mitgitlab.mit.com.mx/desarrollo/terceros/onboarding/mit_onboarding_validacion into develop

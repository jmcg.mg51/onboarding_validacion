import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService) { }

  error: string;

  ngOnInit() {
    this.error = sessionStorage.getItem('error');
    sessionStorage.clear();
    this.spinner.hide();
  }

}

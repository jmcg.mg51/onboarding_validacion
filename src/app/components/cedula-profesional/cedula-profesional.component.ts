import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-cedula-profesional',
  templateUrl: './cedula-profesional.component.html',
  styleUrls: ['./cedula-profesional.component.css']
})
export class CedulaProfesionalComponent implements OnInit {

    //Mark: variables of components
    carreraName;
    anoGraduacion;
    cedulaProfesionalInput;
    errorGeneral: boolean;
    chosenYearDate: Date;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsCedula;


  async ngOnInit() {
    console.log("CEDULA");
    this.spinner.show;

    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;

    if(sessionStorage.getItem('intentsCedula')){
      this.intentsCedula = sessionStorage.getItem('intentsCedula');
    }else{
      sessionStorage.setItem('intentsCedula', '0');
      console.log("El valor de intents es:",sessionStorage.getItem('intentsCedula'));
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("cedulaProfesional", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsCedula = sessionStorage.getItem('intentsCedula');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    var date = document.getElementById("anoGraduacion") as HTMLInputElement;
    date.maxLength = 4;
    await this.spinner.hide();
    document.getElementById('carreraName').focus();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.cedulaProfesional===true){
        this.router.navigate([this.workFlowService.redirectFlow('cedulaProfesional',this.object.servicios)]);   
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.spinner.show();
    this.intentsCedula =  parseInt(this.intentsCedula) + 1;
    sessionStorage.setItem('intentsCedula', this.intentsCedula);
    this.carreraName = $("#carreraName").val();
    this.anoGraduacion = $("#anoGraduacion").val();
    this.cedulaProfesionalInput = $("#cedulaProfesionalInput").val();

    if(this.carreraName === ""  || this.anoGraduacion === "" || this.cedulaProfesionalInput === "" || this.anoGraduacion < 1900){
      this.errorGeneral = true; 
      this.spinner.hide();
    }else{
      const cedula = {
        nombre: this.carreraName,
        numero: this.cedulaProfesionalInput,
        anio: this.anoGraduacion
      }

      console.log(cedula);
      await this.saveCedula(cedula);
      this.spinner.hide();
    }

    
    //this.CurpValidate(this.curpInput);
  }



  async saveCedula(cedula){
    
    sessionStorage.setItem('cedulaProfesional', "si");
    //sessionStorage.setItem('patron', this.razonPatron);
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("cedulaProfesional", device, this.intentsCedula, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
    await this.middle.updateDataUser({cedula}, this.id);

    const object = this.session.getObjectSession();
    object.cedulaProfesional = true;
    this.session.updateModel(object);

    this.continue();

    
  }


  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('cedulaProfesional',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
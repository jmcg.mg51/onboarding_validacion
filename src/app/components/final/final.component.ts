import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit {

  
  imgUrl = '../../../../../assets/img/final/22.Final.png';
  instruction = '¡Gracias por verificar tu identidad!';
  btnText = 'Finalizar';
  movilFlag: string; 
  object: sesionModel;
  routeToReturn: string;
  id: any;
  hide = false;
  title: string; 
  redirect: boolean; 

  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService, private global: GlobalInstructionComponent) {
  }

  async ngOnInit() {
    this.movilFlag = sessionStorage.getItem("movilFlag");

      this.getDataObjectSession();
      this.id = this.object._id;
      const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
      fp.get({tag: {'tag':this.id}});

    this.checkCallBack();
    await this.spinner.hide();
  }

  async getDataObjectSession() {
    this.object = this.session.getObjectSession();
    if (this.object !== undefined && this.object !== null ) {
      if (sessionStorage.getItem('reporteCrediticio') === 'no'  ) {
        return;
      } 
    this.middleDaon.getResults(this.object._id);
    }
  }

  checkCallBack(){
    const sessionDaon = JSON.parse(sessionStorage.getItem("currentSessionDaon"));
    let callback = sessionDaon["callback"];
    if(!callback || callback === '' || callback === null || callback === "undefined"){
      callback = 'https://www.mitec.com.mx';
    }
      const trackID = sessionDaon["_id"];
      console.log("El callback configurado es:", callback);

        this.title = 'Identidad verificada';
        this.redirect = true;
        setTimeout(function(){ 
          var finalCallback = callback+'?trackId='+trackID;
          window.location.href = finalCallback
        }, 5500);
        
      // }else{
      //   this.title = '¡Listo!';
      //   this.redirect = false;
      // }

    // }else{
    //   this.title = '¡Listo!';
    //   this.redirect = false;
    // }

  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}

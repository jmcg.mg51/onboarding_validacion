import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../../environments/environment';
import {GlobalInstructionComponent} from '../../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { terminosCirculo, historialCirculo } from '../../../model/documentos/aceptacion';
import htmlToPdfmake from "html-to-pdfmake";
var pdfMake = require("pdfmake/build/pdfmake");
var pdfFonts = require("pdfmake/build/vfs_fonts");
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-envio-pin',
  templateUrl: './envio-pin.component.html',
  styleUrls: ['./envio-pin.component.css']
})
export class EnvioPinComponent implements OnInit {

    //Mark: variables of components
    carreraName;
    anoGraduacion;
    cedulaProfesionalInput;
    errorGeneral: boolean;
    chosenYearDate: Date;
    emailCode: string;
    textoModal: string;
    tituloModal: string;
    terminosBase64;
    textoModalContract: string;
    flagTerms: boolean;
    flagHist: boolean;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService, private modalService: NgbModal) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsEnvioPin;
  showError: boolean;
  nombreOtorgante: string;


  async ngOnInit() {
    console.log("PIN");
    this.spinner.show;
    this.showError = false;
    if (!(await this.alredySessionExist())) { return; }
    this.emailCode = sessionStorage.getItem('emailCode');
    this.id = this.object._id;

    console.log("track", this.id);

    if(sessionStorage.getItem('ACrediticio')){
      this.intentsEnvioPin = sessionStorage.getItem('ACrediticio');
    }else{
      sessionStorage.setItem('ACrediticio', '1');
      this.intentsEnvioPin = sessionStorage.getItem('ACrediticio');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});

    const device = this.analyticService.getTypeDevice();
    const initAnalytics = this.analyticService.initAnalytics("ACrediticio", device)
    var result = await this.middle.updateDataUser(initAnalytics, this.id);

    if(sessionStorage.getItem("nombreComercio")){
      this.nombreOtorgante = sessionStorage.getItem("nombreComercio");
    }else{
      this.nombreOtorgante = "(Nombre del otorgante del credito)";
    }
    await this.fullDataContract();
    await this.spinner.hide();
  }

  open(content, tipo) {
    if (tipo === 'terminos') {
      this.textoModal = this.textoModalContract;
      this.tituloModal = 'Términos y Condiciones';
    } else {
      this.textoModal = historialCirculo;
      this.tituloModal = 'Historial Crediticio';
    }
    this.modalService.open(content);
  }

  changeStatus(tipo) {
    if (tipo === 'terminos') {
      this.flagTerms = !this.flagTerms;
    } else {
      this.flagHist = !this.flagHist;
    }
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.envioPin===true){
        this.router.navigate([this.workFlowService.redirectFlow('envioPin',this.object.servicios)]);   
        return false;
      } else {
        return true;
      }
    }
  }

  async fullDataContract() {
    var textTerminos = sessionStorage.getItem("textoTerminos");
    textTerminos = textTerminos.replace(/#NOMBRE#/g, sessionStorage.getItem("contratante") || '#NOMBRE#');
    // this.textoModal = textTerminos.replace(/#NOMBRE#/g, sessionStorage.getItem("contratante") || '#NOMBRE#');
    this.textoModalContract = textTerminos.replace(/#COMERCIO#/g, sessionStorage.getItem("nombreComercio") || '#COMERCIO#');
    let jsonAdicionales = undefined;
    try {
      jsonAdicionales = JSON.parse( atob(sessionStorage.getItem('valoresAdicionalesContrato')));
    } catch (error) {
      jsonAdicionales = undefined;
    }
    console.log('el valor del json es: ' , jsonAdicionales);
    if(jsonAdicionales) {
      for(let value in jsonAdicionales) {
        let regex = new RegExp(`#${value}#`, 'g');
        console.log(regex);
        this.textoModalContract = this.textoModalContract.replace(new RegExp(`#${value}#`, 'g'), jsonAdicionales[value]);
      }
    }
    var valuePDF = htmlToPdfmake(this.textoModalContract);
    var htmlStructure = {content:valuePDF};
    this.terminosBase64 = await this.convertHTMLToBase64(htmlStructure);
    this.nombreOtorgante = sessionStorage.getItem("nombreComercio") || '(Nombre del otorgante del credito)';
  }

  async convertHTMLToBase64(pdfStructure){
    let pdfBase64;
     var pdfDocGenerator = await pdfMake.createPdf(pdfStructure, {
       exampleLayout: {
         hLineColor: function (rowIndex:any, node:any, colIndex:any) {
           if (rowIndex === node.table.body.length) return 'blue';
           return rowIndex <= 1 ? 'red' : '#dddddd';
         },
         vLineColor: function (colIndex:any, node:any, rowIndex:any) {
           if (rowIndex === 0) return 'red';
           return rowIndex > 0 && (colIndex === 0 || colIndex === node.table.body[0].length) ? 'blue' : 'black';
         }  
       }
     });
 
     return new Promise(function(resolve, reject) {
       pdfDocGenerator.getBase64(function (pdfBase64) {
         resolve(pdfBase64);
       });
     });
 
   }

  async sendPIN(){
    // this.spinner.show();
  //   this.intentsEnvioPin =  parseInt(this.intentsEnvioPin) + 1;
  //   sessionStorage.setItem('ACrediticio', this.intentsEnvioPin);

  //   sessionStorage.setItem('envioPin', "si");
  //   //sessionStorage.setItem('patron', this.razonPatron);
  //   const device = this.analyticService.getTypeDevice();
  //   const analytics = this.analyticService.updateAnalytics("envioPin", device, this.intentsEnvioPin, "NO", false);
  //   console.log("El valor actual de analytics es:", analytics);
  //   await this.middle.updateDataUser(analytics, this.id);
  //   const response = await this.middle.enviarPIN(this.id);
    
  //   console.log("response", response);
  //  // await this.middle.updateDataUser({cedula}, this.id);
  //   if(response === 200){
  //     this.showError = false;
  //     const object = this.session.getObjectSession();
  //     object.envioPin = true;
  //     this.session.updateModel(object);
  //     this.continue();
  //     this.spinner.hide();
  //   }else{
  //     this.showError = true;
  //     this.spinner.hide();
  //     return;
  //   }
    
    
  }


  async continue() {
    await this.spinner.show();
    const autorizacion = {
      acepto: true,
      nip: this.emailCode
    }
    const ficoScore = {
      acepto: true,
      nip: this.emailCode,
      nivelFicoScore: sessionStorage.getItem("nivelFicoScore"),
      terminos: this.terminosBase64
    }
    sessionStorage.setItem('reporteCrediticio', "si");
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("ACrediticio", device, this.intentsEnvioPin, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
    await this.middle.updateDataUser({ficoScore, autorizacion}, this.id);

    const object = this.session.getObjectSession();
    object.analisisFinancieroAutorizacion = true;
    this.session.updateModel(object);
    await this.spinner.hide();
    this.router.navigate([this.workFlowService.redirectFlow('envioPin',this.object.servicios)]); 
  }

  async cancelar() {
    //Pantalla de fin
    sessionStorage.setItem('reporteCrediticio', "no");
    this.router.navigate(['/services/final']); 
  }

  public getColorButton() {
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel() {
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
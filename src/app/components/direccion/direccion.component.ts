import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../environments/environment';
import {GlobalInstructionComponent} from '../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-direccion',
  templateUrl: './direccion.component.html',
  styleUrls: ['./direccion.component.css']
})
export class DireccionComponent implements OnInit {

    //Mark: variables of components
    calleInput;
    numeroInput;
    coloniaInput;
    cpInput;
    ciudadInput;
    edoInput;
    errorGeneral: boolean;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsDirection;
  resulSearch;
  arrayStreet: any;
  showOtherColony: boolean;
  testDireccion = [
        { 
    "zipCode": "55076",
    "colony": "Las Américas",
    "municipality": "Ecatepec de Morelos",
    "state": "México"
        } 
    ];

  async ngOnInit() {
    this.spinner.show;
    
    this.calleInput =sessionStorage.getItem('calle');
    this.numeroInput =sessionStorage.getItem('numero');
    this.coloniaInput =sessionStorage.getItem('colonia');
    this.cpInput =sessionStorage.getItem('zip');
    this.ciudadInput =sessionStorage.getItem('city');
    this.edoInput =sessionStorage.getItem('edo');
    
    if (!(await this.alredySessionExist())) { return; }
  
    this.id = this.object._id;
    if(sessionStorage.getItem('intentsDirection')){
      this.intentsDirection = sessionStorage.getItem('intentsDirection');
    }else{
      sessionStorage.setItem('intentsDirection', '0') 
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("direccion", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsDirection = sessionStorage.getItem('intentsDirection');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
    document.getElementById('calleInput').focus();


 /*
    $('#cpInput').focus(function() {
      console.log("Estoy en input");
  }).blur(function(){
    var inputCP = (document.getElementById("cpInput") as HTMLInputElement).value
      if( inputCP !== ""){
       // this.searchCode();
      }
  });*/

  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.identity===true){
        this.router.navigate([Rutas.direccion]);
        return false;
      } else {
        return true;
      }
    }
  }


  async aceptar(){
    this.intentsDirection =  parseInt(this.intentsDirection) + 1;
    sessionStorage.setItem('intentsDirection', this.intentsDirection);
    this.spinner.show();
    this.calleInput = $("#calleInput").val();
    this.numeroInput = $("#numeroInput").val();
    //this.coloniaInput = $("#coloniaInput").val();
    this.cpInput = $("#cpInput").val();
    this.ciudadInput = $("#ciudadInput").val();
    this.edoInput = $("#estadoInput").val();

    if((document.getElementById("ciudadSelect") as HTMLInputElement).value === "OTRO"){
      this.coloniaInput = $("#coloniaInput").val();
    }

    if(this.calleInput === "" || this.numeroInput === "" || this.cpInput === "" || this.ciudadInput === ""
    || this.edoInput === "" ){
      this.errorGeneral = true; 
      this.spinner.hide();
      return;
    }else{
      const address = {
        calle: this.calleInput,
        numero: this.numeroInput,
        colonia: this.coloniaInput,
        zip: this.cpInput,
        city: this.ciudadInput,
        edo: this.edoInput
      }

      console.log(address);
      this.saveDirection();
      this.spinner.hide();
    }

    
    //this.CurpValidate(this.curpInput);
  }



  async saveDirection(){
    
    sessionStorage.setItem('direccion', "si");
    sessionStorage.setItem('calle', this.calleInput);
    sessionStorage.setItem('numero', this.numeroInput);
    sessionStorage.setItem('colonia', this.coloniaInput);
    sessionStorage.setItem('zip', this.cpInput);
    sessionStorage.setItem('city', this.ciudadInput);
    sessionStorage.setItem('edo', this.edoInput);

      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("direccion", device, this.intentsDirection, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.middle.updateDataUser(analytics, this.id);

      console.log(sessionStorage.getItem("colonia"));
      this.continue();

    
  }

  async searchCode($event: Event){
    const code = ($event.target as HTMLInputElement).value;
    const value = (document.getElementById("ciudadSelect") as HTMLInputElement).value;
    if(code !== ""){
      this.spinner.show();
      console.log("voy a bucar:", code);
      
      this.resulSearch = await this.middle.getInfoStreet(code);
     

      console.log(this.resulSearch);
      if(this.resulSearch === "ERROR" || this.resulSearch === undefined || this.resulSearch.errorType){
         //Eliminar 
        //this.arrayStreet = this.testDireccion;
        //console.log("array", this.arrayStreet);
        console.log("true");
        this.arrayStreet = [];
        this.showOtherColony = true;
        const select = document.getElementById("ciudadSelect") as HTMLInputElement;
        select.value = "OTRO";
        select.disabled = true;
        select.style.display = "none";
        
       // const resultadoBusqueda = this.arrayStreet.find( object1 => object1.colony === value );
       // console.log("valor", resultadoBusqueda);
       // this.coloniaInput = resultadoBusqueda["colony"];
      //  this.edoInput = resultadoBusqueda["state"];
      }else{
        console.log("validacion");
        const select = document.getElementById("ciudadSelect") as HTMLInputElement;
        select.style.display = "block";
        this.showOtherColony = false;
        this.arrayStreet = this.resulSearch;
       
      }

      //const resultadoSucursal = this.sucursales.find( object1 => object1.id === valor );
      //console.log("la sucursal a pintar es:", resultadoSucursal);
      this.spinner.hide();
    }
    


  }

  async getInfoCiudad($event: Event){
    const value = (document.getElementById("ciudadSelect") as HTMLInputElement).value;
    console.log("el value es", value);
    if(value === "OTRO"){
      this.showOtherColony =  true;
      this.ciudadInput = "";
      this.edoInput = "";
      this.coloniaInput = "";
    }else{
      this.showOtherColony = false;
      //console.log($event.currentTarget);
      for(var i = 0; i< this.arrayStreet.length; i++){
        console.log("iteracion", this.arrayStreet[i]);
        if(this.arrayStreet[i].colony === value){
          console.log()
          this.coloniaInput = this.arrayStreet[i].colony;
          this.ciudadInput = this.arrayStreet[i].municipality;
          this.edoInput = this.arrayStreet[i].state;
        }
      }
    }

    /**
     const resultadoBusqueda = this.arrayStreet.find( object1 => object1.colony === value );
        console.log("valor", resultadoBusqueda);
        this.coloniaInput = resultadoBusqueda["colony"];
        this.edoInput = resultadoBusqueda["state"];
     */

  }



  async continue(){
    this.router.navigate([Rutas.ocrValidation]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
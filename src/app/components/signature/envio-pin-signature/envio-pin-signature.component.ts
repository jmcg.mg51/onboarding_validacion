import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { environment } from '../../../../environments/environment';
import {GlobalInstructionComponent} from '../../global-instruction/global-instruction.component';
import { WorkFlowService } from '../../../services/session/work-flow.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { NgbTypeaheadWindow } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead-window';
import { AnaliticService } from 'src/app/services/session/analitic.service';

@Component({
  selector: 'app-envio-pin-signature',
  templateUrl: './envio-pin-signature.component.html',
  styleUrls: ['./envio-pin-signature.component.css']
})
export class EnvioPinSignatureComponent implements OnInit {

    //Mark: variables of components
    carreraName;
    anoGraduacion;
    cedulaProfesionalInput;
    errorGeneral: boolean;
    chosenYearDate: Date;

  //Calle numero colonia zip city edo. 
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private sesion: SessionService, private session: SessionService,
              private middleDaon: MiddleDaonService,private global: GlobalInstructionComponent,
              private workFlowService :WorkFlowService, private middle: MiddleMongoService, 
              private analyticService: AnaliticService) {
  }


  object: sesionModel;
  routeToReturn: string;
  id: any;
  ti: string;
  intentsEnvioPin;
  showError: boolean;
  nombreOtorgante: string;


  async ngOnInit() {
    console.log("PIN");
    this.spinner.show;
    this.showError = false;
    if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;

    console.log("track", this.id);

    if(sessionStorage.getItem('intentsPINSignature')){
      this.intentsEnvioPin = sessionStorage.getItem('intentsPINSignature');
    }else{
      sessionStorage.setItem('intentsPINSignature', '0');
      console.log("El valor de intents es:",sessionStorage.getItem('intentsPINSignature'));
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("envioPinSignature", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsEnvioPin = sessionStorage.getItem('intentsPINSignature');
    }

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});

    if(sessionStorage.getItem("nombreComercio")){
      this.nombreOtorgante = sessionStorage.getItem("nombreComercio");
    }else{
      this.nombreOtorgante = "(Nombre del otorgante del credito)";
    }
    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.envioPinSignature===true){
        this.router.navigate([this.workFlowService.redirectFlow('envioPinSignature',this.object.servicios)]);   
        return false;
      } else {
        return true;
      }
    }
  }

  async sendPIN(){
    this.spinner.show();
    this.intentsEnvioPin =  parseInt(this.intentsEnvioPin) + 1;
    sessionStorage.setItem('intentsPINSignature', this.intentsEnvioPin);

    sessionStorage.setItem('envioPinSignature', "si");
    //sessionStorage.setItem('patron', this.razonPatron);
    const device = this.analyticService.getTypeDevice();
    const analytics = this.analyticService.updateAnalytics("envioPinSignature", device, this.intentsEnvioPin, "NO", false);
    console.log("El valor actual de analytics es:", analytics);
    await this.middle.updateDataUser(analytics, this.id);
    const response = await this.middle.enviarPINSignature(this.id);
    
    console.log("response", response);
   // await this.middle.updateDataUser({cedula}, this.id);
    if(response === 200){
      this.showError = false;
      const object = this.session.getObjectSession();
      object.envioPinSignature = true;
      this.session.updateModel(object);
      this.continue();
      this.spinner.hide();
    }else{
      this.showError = true;
      this.spinner.hide();
      return;
    }
    
    
  }


  async continue(){
    this.router.navigate([this.workFlowService.redirectFlow('envioPinSignature',this.object.servicios)]); 
   // this.router.navigate([Rutas.ocrValidation]);
  }

  async cancelar(){
    //Pantalla de fin
    this.router.navigate(['/services/final']); 
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { LigaUtil } from 'src/app/model/LigasUtil';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { request } from 'http';
import { timeout } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MiddleMongoService {

  constructor(private http: HttpClient) { }

  headers = new HttpHeaders().set('Content-Type', 'application/json');


  async creaTrackId(oferta: string, apiToken: string, requestString: string, valoresAdic: string) {
    let objectResponse;
    const body = {
      merchant : apiToken,
      id : oferta,
      tag: requestString,
      valoresAdic
    };
    console.log(body);
    await this.http.post(LigaUtil.urlMiddleMongo(), body, { headers: this.headers })
    .toPromise().then((data) => {
      console.log(data);
      if ( data['trackId']) {
        objectResponse = {};
        objectResponse['id'] = data['trackId'];
        objectResponse['callback'] = data['callback'];
      }
    }).catch((err) => {
      // console.log(err);
      objectResponse = undefined;
    });
    console.log('regresare:  ' + objectResponse);
    return objectResponse;
  }

  async getDataUser(id: string) {
    // console.log('El id que recibo es: ' + id);
    let result;
    // console.log("URL= " , LigaUtil.urlMiddleMongo() + `/${id}`, " headers ", this.headers);
    const servicio = this.http.get(LigaUtil.urlMiddleMongo() + `/${id}`, { headers: this.headers });
    await servicio.toPromise().then(data => {
      // console.log('los datos que vienen son>>>>: ');
      // console.log(JSON.stringify(data));
      if (data['estatus'] === 'nuevo' || data['estatus'] === 'en progreso') {
        result = data;
        
        if (data['sesion']) {
          result.daon=data['sesion'];
        } else {
          result.daon = {};
        }
        delete result.sesion;
        // console.log('el final');
        // console.log(result);
      }
    }).catch((err) => {
      console.log('entre al catch',err);
    });
    // console.log('ya voy a regresar los datos');
    // console.log(result);
    return result;
  }

  async updateTermsDataUser(datos, id: string) {
    // console.log('lo que voy actulizar es: ');
    // console.log(datos);
    let mongoUpdate;
    mongoUpdate = this.http.put(LigaUtil.urlMiddleMongo() + `/${id}`, datos, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
    await mongoUpdate.toPromise().then(data => {
      // console.log("respuesta promise: ",data);
      mongoUpdate = data;
      
    }).catch((err) => { 
      console.log(err);
  });
  }

  async updateDataUser(datos, id: string) {
    let response = 'OK';
    // console.log('lo que voy actulizar es 1: ');
    // console.log(datos);
    var copy = Object.assign({}, datos);
    copy.servicios=undefined;
    console.log('lo que voy actulizar es 2: ');
    let mongoUpdate;
    mongoUpdate = await this.http.put(LigaUtil.urlMiddleMongo() + `/${id}`, copy, { headers: this.headers }).toPromise().then(data => {
      console.log(data);
      if(data['errorMessage']) {
        response = 'ERROR';
      }
    }).catch((err) => {
      console.log('No actualice ',err);
      response = 'ERROR';
    });
    return response;
  }
  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


  async getOferta(id: string) {
    let response = {};
    const token = sessionStorage.getItem('jwtToken');
    this.headers = this.headers.set('Authorization', token);
    await this.http.get(LigaUtil.urlMiddleMongo() + `/oferta/${id}/`, { headers: this.headers })
      .toPromise().then((res) => {
        // console.log(res);
        if (!res || res['errorType']) {
          response = { error: 'Error, favor de volver a intentar' };
        } else {
          response = res;
        }
      }).catch((err) => {
        console.log(err);
        response = { error: 'Error, favor de volver a intentar' };
      });
    return response;
  }

  async getInfoOferta(id:string, merchant:string ){
    var response;

    // console.log(LigaUtil.urlMiddleMongo() + `?id=${id}&merchant=${merchant}`);
    await this.http.get(LigaUtil.urlMiddleMongo() + `?id=${id}&merchant=${merchant}`)
      .toPromise().then((res) => {
        // console.log(res);
        response = res;
      }).catch((err) => {
        console.log(err);
        response = err;
      });
    return response;
  }

  async getLinkMail(id: string,ti: string) {
    let result;
    ///usuario/{trackId}/email/link
    const servicio = this.http.get(LigaUtil.urlMiddleRoot(id) + 'email/link?ti='+ti, { headers: this.headers });
    await servicio.toPromise().then(data => {
      
      result=data;
       console.log("dataMailLink123= ",data);
      
    }).catch((err) => {
      console.log('entre al catch',err);
      result=err;
    });
    // console.log('ya voy a regresar los datos');
    // console.log(result);
    return result;
  }

  async enviarPIN(track: string) {
    let result = 200;
    await this.http.post(LigaUtil.urlMiddleMongo() + `/credito/email`, {trackId: track}, { headers: this.headers, })
    .toPromise().then(datos => {
      if (datos['errorType'] 
      || datos['errorMessage']
      ) {
        result = 404;
      }
    }).catch((err) => {
      console.log('error');
      console.log(err);
      result = 404;
    });
    return result;
  }

  ///usuario/credito/email/verify
  async validaCodigoPIN(id: string, codigo: string) {
    let result = 200;
    await this.http.post(LigaUtil.urlMiddleMongo() + `/credito/email/verify`, {trackId:id , code: codigo}, { headers: this.headers, })
    .toPromise().then(datos => {
      if (datos['errorType'] 
      || datos['errorMessage']
      ) {
        result = 404;
      }
    }).catch((err) => {
      console.log('error');
      //console.log(err);
      result = 404;
    });
    return result;
  }

  //https://dev-api.mitidentity.com/catalogo/zipcode/01048
  async getInfoStreet(code) {
    let result;
    ///usuario/{trackId}/email/link
   // this.headers.append( "timeout", "1000" );
    const servicio = this.http.get(LigaUtil.urlMiddleNubarium() + `/catalogo/zipcode/${code}`, { headers: this.headers }).pipe(timeout(10000));
    await servicio.toPromise().then(data => {
      console.log("la data es", data);
      result=data;
    }).catch((err) => {
      console.log('entre al catch',err);
      result="ERROR";
    });
    // console.log('ya voy a regresar los datos');
    // console.log(result);
    return result;
  }

  async enviarPINSignature(track: string) {
    let result = 200;
    await this.http.post(LigaUtil.urlMiddleMongo() + `/signature/email`, {trackId: track}, { headers: this.headers, })
    .toPromise().then(datos => {
      if (datos['errorType'] 
      || datos['errorMessage']
      ) {
        result = 404;
      }
    }).catch((err) => {
      console.log('error');
      console.log(err);
      result = 404;
    });
    return result;
  }

  ///usuario/credito/email/verify
  async validaCodigoPINsignature(id: string, codigo: string) {
    let result = 200;
    await this.http.post(LigaUtil.urlMiddleMongo() + `/signature/email/verify`, {trackId:id , code: codigo}, { headers: this.headers, })
    .toPromise().then(datos => {
      if (datos['errorType'] 
      || datos['errorMessage']
      ) {
        result = 404;
      }
    }).catch((err) => {
      console.log('error');
      //console.log(err);
      result = 404;
    });
    return result;
  }


  async decipherText(cipherText: string) {
    console.log("el paramatro a enviar es:", cipherText);
    let text; 
    await this.http.post(LigaUtil.urlIdMission() + `/precarga`, {cipherText}, { headers: this.headers })
    .toPromise().then((data) => {
      console.log(data);
      text = data; 
    }).catch((err) => {
      // console.log(err);
      text = err;
 
    });
    //text = {"email":"ivan.ake@mitec.com.mx", "rfc":"XAXX010101000"}
    console.log('regresare:  ', text);
    return text;
  }

  async test() {
    console.log('entre al tes: ' );
    let result;
    // console.log("URL= " , LigaUtil.urlMiddleMongo() + `/${id}`, " headers ", this.headers);
    const servicio = this.http.post("https://fcqa.mitec.com.mx/pinpadWeb/crypto/llaves"
    , "yJRiv6ZVxnjJzEKVkEhd0Q==/Q4AXjobI48UTREHRNYFP1UYlONXbjPnGw6fSaffpWaagU6zfcfkWeAOe0S59Ls0UB6yl/I86BJqpzEBah36qSSnMAb9+Wa9EEAxmf7aVhNbBVcaSwJYiMtEErXA+KS/5NfOXffLtCvQb/+fEWvXHOfD8zxlE9NKndZ6uqPb//U=" ,
    { headers: 
      { data: 'bw8v0YbYa4Y+HF9fppJyoeE5ToWK+NfozXMCdGfeknC8q1uDlTJoB9fMDvuJoakmg+XIkqr0yfg1WrKR7VDwXNQWVeNgKP+qwD7DAbjnvgHYpOTtxuMNc24c637gjg9A0FKD/W7vGjCqjjLyikpFleUvFmqIi8nRs+5Lz9UTBPZ5AUz1eSOvfhHF90xSz599W8RilSZmsBq300/oGEuHuPfjiqSSpci6t8UOy+zxI4/48cJ4yjCuThrqOF4AJr8pcE4rSAt8ARlPys6zxCtV1+xU6BOfbffVLicsa9wcAPiI/qgOrStfV7nWAnZg4STeUyslX16oupWchMgWlW/51g==' , 
//      data2: 'DqFsL5%2FDyyMzQLXgMJpJSA%2FGrCYgC1%2BctGS9zdFTBEDAzoQvLXuZTsFjAz8tD0ZX%2FWHSwtQPIbnwvEFHlD50JDvYTkCOjoNvUIivcTqqI9lR8WvApcNjHqsdH%2BvaoR314SozQLFch5mAKp3pjzmpF2jIFUKxpl26knJVAaphkQeMYUDxiiKRJ9T2vA0ZhCt4yrh6Q6bazDXnXuljIndn5kCds49imaqUGQ4hPuFGsu3y9t1zcGHI5L%2FmpDzE315VBvrEIxlu5InD43wmyf8YLxf0sjEWfhc4Iocyl2cqB8p%2F2Qh1tuNQiLW%2BHPVfI9hv5%2BzgvFGd8zXQQhHD0IkBhg%3D%3D',
      'Content-Type': 'text/plain'}});

    //  const servicio = this.http.get("https://m.mit.com.mx/pinpadWeb/ppw/index.html");
//    this.headers.set('token' , 'DyG3QWYO4IuQ6qa9igTaJsBWLxKlN8geumzo_fv4zAj6E7Rnxzej2pWrd0uxjyTg9gSTjagh_ZBlZYujW7ELLtCEfCGTr_pImRfSVEk');
    // const servicio = this.http.post('https://apiwcapitald.mit.com.mx/validaCampaign',{} ,{ headers: this.headers } );
    result = await servicio.toPromise();
    console.log('el result es: ' , result);
    // console.log('ya voy a regresar los datos');
    // console.log(result);
    return result;
  }
  



}

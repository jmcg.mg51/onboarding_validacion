import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';
import { async } from 'rxjs/internal/scheduler/async';
import { Rutas } from 'src/app/model/RutasUtil';
import { Router } from '@angular/router';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { SessionService } from 'src/app/services/session/session.service';
import { GlobalInstructionComponent } from 'src/app/components/global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { WorkFlowService } from 'src/app/services/session/work-flow.service';

@Component({
  selector: 'app-captura-datos',
  templateUrl: './captura-datos.component.html',
  styleUrls: ['./captura-datos.component.css']
})
export class CapturaDatosComponent implements OnInit {

  showOtherColony: boolean;
  arrayStreet: any;
  resulSearch: any;
  coloniaInput: any;
  errorMessage: string;
  id: string;
  object: sesionModel;
  listSexo = ['Masculino','Femenino'];
  listEstado = [
    {clave:'AGU', nombre:'Aguascalientes'},
    {clave:'BCN', nombre:'Baja California'},
    {clave:'BCS', nombre:'Baja California Sur'},
    {clave:'CAMP', nombre:'Campeche'},
    {clave:'CHP', nombre:'Chiapas'},
    {clave:'CHH', nombre:'Chihuahua'},
    {clave:'COA', nombre:'Coahuila'},
    {clave:'COL', nombre:'Colima'},
    {clave:'DIF', nombre:'Ciudad de México'},
    {clave:'DUR', nombre:'Durango'},
    {clave:'GUA', nombre:'Guanajuato'},
    {clave:'GRO', nombre:'Guerrero'},
    {clave:'HID', nombre:'Hidalgo'},
    {clave:'JAL', nombre:'Jalisco'},
    {clave:'MEX', nombre:'México'},
    {clave:'MIC', nombre:'Michoacán'},
    {clave:'MOR', nombre:'Morelos'},
    {clave:'NAY', nombre:'Nayarit'},
    {clave:'NLE', nombre:'Nuevo León'},
    {clave:'OAX', nombre:'Oaxaca'},
    {clave:'PUE', nombre:'Puebla'},
    {clave:'QUE', nombre:'Querétaro'},
    {clave:'ROO', nombre:'Quintana Roo'},
    {clave:'SLP', nombre:'San Luis Potosí'},
    {clave:'SIN', nombre:'Sinaloa'},
    {clave:'SON', nombre:'Sonora'},
    {clave:'TAB', nombre:'Tabasco'},
    {clave:'TAM', nombre:'Tamaulipas'},
    {clave:'TLA', nombre:'Tlaxcala'},
    {clave:'VER', nombre:'Veracruz'},
    {clave:'YUC', nombre:'Yucatán'},
    {clave:'ZAC', nombre:'Zacatecas'}
];
  
  updateValuesForm = this.formBuilder.group({
    nombres: ['', { validators: [Validators.required] }],
    aPaterno: [''],
    aMaterno: [''],
    curp: ['', { validators: [Validators.required] }],
    calle: ['', { validators: [Validators.required] }],
    numero: ['', { validators: [Validators.required] }],
    colonia: ['', { validators: [Validators.required] }],
    codigoPostal: ['', { validators: [Validators.required] }],
    ciudad: ['', { validators: [Validators.required] }],
    sexo: ['', { validators: [Validators.required] }],
    estado: ['', { validators: [Validators.required] }]
  });
  intentsDocument;

  constructor(private formBuilder: FormBuilder, private spinner: NgxSpinnerService,
    private middle: MiddleMongoService, public router: Router,
    private session: SessionService, private global: GlobalInstructionComponent,
    private analyticService: AnaliticService, private workFlowService: WorkFlowService) { }

  async ngOnInit() {
    await this.spinner.show();
    if (!(await this.alredySessionExist())) { return; }
    console.log(this.object);
    this.id = this.object._id;

    if(sessionStorage.getItem('intentsDocument')){
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    }else{
      sessionStorage.setItem('intentsDocument', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("documento", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsDocument = sessionStorage.getItem('intentsDocument'); 
    }
    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    }
    return true;
  }


  async searchCode($event: Event) {
    const code = ($event.target as HTMLInputElement).value;
    const value = (document.getElementById("ciudadSelect") as HTMLInputElement).value;
    if (code !== "") {
      this.spinner.show();
      console.log("voy a bucar:", code);
      this.resulSearch = await this.middle.getInfoStreet(code);
      console.log(this.resulSearch);
      if (this.resulSearch === "ERROR" || this.resulSearch === undefined || this.resulSearch.errorType) {
        //Eliminar 
        //this.arrayStreet = this.testDireccion;
        //console.log("array", this.arrayStreet);
        console.log("true");
        this.arrayStreet = [];
        this.showOtherColony = true;
        const select = document.getElementById("ciudadSelect") as HTMLInputElement;
        select.value = "OTRO";
        select.disabled = true;
        select.style.display = "none";

      } else {
        console.log("validacion");
        const select = document.getElementById("ciudadSelect") as HTMLInputElement;
        select.style.display = "block";
        this.showOtherColony = false;
        this.arrayStreet = this.resulSearch;

      }

      this.spinner.hide();
    }
  }

  async getInfoCiudad($event: Event) {
    const value = (document.getElementById("ciudadSelect") as HTMLInputElement).value;
    console.log("el value es", value);
    if (value === "OTRO") {
      this.showOtherColony = true;
      this.updateValuesForm.patchValue({
        ciudad: '',
        estado: '',
        colonia: ''
      });
    } else {
      this.showOtherColony = false;
      for (var i = 0; i < this.arrayStreet.length; i++) {
        console.log("iteracion", this.arrayStreet[i]);
        if (this.arrayStreet[i].colony === value) {
          this.updateValuesForm.patchValue({
            ciudad: this.arrayStreet[i].municipality,
            estado: this.arrayStreet[i].state,
            colonia: this.arrayStreet[i].colony
          });
        }
      }
    }
  }
  async cancelar() {
    this.errorMessage = '';
    this.router.navigate([Rutas.ocrValidation]);
  }

  curpValidate(curp) {
    var regexCurp = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
    if (curp.match(regexCurp)) {
      console.log("validado");
      return true;
    } else {
      return false;
    }
  }

  async saveValues() {
    await this.spinner.show();
    this.errorMessage = '';
    let curp = this.updateValuesForm.controls.curp.value.toUpperCase();
    if((document.getElementById('ciudadSelect') as HTMLSelectElement).value === 'OTRO'){
      let coloniaForm = (document.getElementById('colonia') as HTMLInputElement).value;
      this.updateValuesForm.controls['colonia'].setValue(coloniaForm); // = (document.getElementById('colonia') as HTMLInputElement).value;
    }
    console.log("los valores del form son:", this.updateValuesForm.value);
    if (!this.updateValuesForm.valid || this.updateValuesForm.controls.estado.value === 'noData' || this.updateValuesForm.controls.sexo.value === 'noData') {
      console.log('no es un form valido');
      this.errorMessage = 'Favor de llenar todos los campos';
      await this.spinner.hide();
      return;
    }
    if (!this.curpValidate(curp)) {
      this.errorMessage = 'El CURP es Incorrecto';
      await this.spinner.hide();
      return;
    }
    curp = curp.toUpperCase();
    let sexo = this.updateValuesForm.controls.sexo.value.toUpperCase() === 'MASCULINO' ? 'M' : 'F';
    const reqAddress = {
      calle: (this.updateValuesForm.controls.calle.value).toUpperCase(),
      numero: this.updateValuesForm.controls.numero.value.toUpperCase(),
      colonia: this.updateValuesForm.controls.colonia.value.toUpperCase(),
      zip: this.updateValuesForm.controls.codigoPostal.value.toUpperCase(),
      city: this.updateValuesForm.controls.ciudad.value.toUpperCase(),
      edo: this.updateValuesForm.controls.estado.value.toUpperCase()
    };
    const reqName = {
      nombres: this.updateValuesForm.controls.nombres.value.toUpperCase(),
      aPaterno: this.updateValuesForm.controls.aPaterno.value.toUpperCase(),
      aMaterno: this.updateValuesForm.controls.aMaterno.value.toUpperCase(),
      sexo 
    };


    if (sessionStorage.getItem('nombres') !== reqName.nombres 
    || sessionStorage.getItem('aPaterno') !== reqName.aPaterno
    || sessionStorage.getItem('aMaterno') !== reqName.aMaterno) {
      reqName['cambio'] = true;
    }

    let responseName = await this.middle.updateDataUser({ name: reqName }, this.id);
    let responseAddress = await this.middle.updateDataUser({ address: reqAddress }, this.id);
    let resultCurp = await this.middle.updateDataUser({ curp: curp }, this.id);

    const device = this.analyticService.getTypeDevice();
    this.intentsDocument =  parseInt(this.intentsDocument) + 1;
    const analytics = this.analyticService.updateAnalytics("documento", device, this.intentsDocument, "NO", false);
    console.log("El valor actual de analytics es:", JSON.stringify(analytics));
    await this.middle.updateDataUser(analytics, this.id);

    if (responseAddress === 'ERROR' || responseName === 'ERROR' || resultCurp === 'ERROR') {
      await this.spinner.hide();
      this.errorMessage = 'Ocurrio un error. Favor de reintentar';
      return;
    }
    sessionStorage.setItem("contratante", (reqName.nombres + ' ' + reqName.aPaterno + ' ' + reqName.aMaterno) );

    await this.spinner.hide();
    this.router.navigate([this.workFlowService.redirectFlow('documentoManual',this.object.servicios)]);
  }

  public getColorButton() {
    return this.global.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.global.getLabelColorSystem();
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesGeneralService, isMobile } from '../../../../services/general/services-general.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { NgxSpinnerService } from 'ngx-spinner';
import { WorkFlowService } from '../../../../services/session/work-flow.service';
import {GlobalInstructionComponent} from '../../../global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { MiddleMongoService } from '../../../../services/http/middle-mongo.service';

declare const deviceInfo: any;
@Component({
  selector: 'app-verify-identity',
  templateUrl: './verify-identity.component.html',
  styleUrls: ['./verify-identity.component.css']
})
export class VerifyIdentityComponent implements OnInit {
 
  constructor(private workFlowService :WorkFlowService, private serviciogeneralService: ServicesGeneralService, private spinner: NgxSpinnerService, public router: Router, private session: SessionService, public servicesGeneralService: ServicesGeneralService,
              private actRoute: ActivatedRoute, private global: GlobalInstructionComponent,
              private analyticService: AnaliticService, private middle: MiddleMongoService) { }

  filtersLoaded: Promise<boolean>;
  typeIdentity: string;
  error: string;
  id: string;
  showINE: boolean;
  showPass: boolean;
  type: string;
  object: sesionModel;
  isMobileBool: boolean;
  sessionGetAlert: string;
  url;
  copiar:string = '      ';
  intentsDocument;
  async ngOnInit() {
  this.spinner.show();
  var responseNavigator = this.validateDevive();
  console.log("validación del navegador", responseNavigator);
  this.isMobileBool = isMobile(navigator.userAgent);
  this.sessionGetAlert = sessionStorage.getItem('alertBrowser');

    if(this.isMobileBool){
      console.log("valor de session alert",this.sessionGetAlert);
      if(this.sessionGetAlert == 'true'){
        
      }else{
        await this.spinner.show();
      }
    }else{
      this.spinner.show();
    }

  if(responseNavigator){
    await this.spinner.show();
    if (!(await this.alredySessionExist())) { return; }
    console.log("obj= ",this.object);
    this.typesIdentity();
    this.id = this.object._id;

    if(sessionStorage.getItem('intentsDocument')){
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    }else{
      sessionStorage.setItem('intentsDocument', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics('documento', device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsDocument = sessionStorage.getItem('intentsDocument'); 
    }
    var baseUrl = document.location.origin;
    console.log(baseUrl);
    console.log(this.id);
    this.url = baseUrl + '#' + Rutas.globalInstruction + `${this.id}`;  

    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    this.error = sessionStorage.getItem('errorDocument');
    this.filtersLoaded = Promise.resolve(true);
    $( "#INEOption" ).trigger( "click" );

    

    $("#INEOption").hover(function(){
      var colorHover = sessionStorage.getItem("colorButton");
      var colorLabelHover = sessionStorage.getItem("colorLabel");
      $(this).css("background-color", colorHover);
      $(this).css("color", colorLabelHover);
      var PassaportOption = document.getElementById("PassaportOption") as HTMLInputElement;
      PassaportOption.style.backgroundColor = "white"; 
      PassaportOption.style.color = "#6c757d";
      });

      $("#PassaportOption").hover(function(){
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        var INEtOption = document.getElementById("INEOption") as HTMLInputElement;
        INEtOption.style.backgroundColor = "white"; 
        INEtOption.style.color = "#6c757d";

        });
  }else{
    this.router.navigate([Rutas.deviceConnection]);
  }
  await this.spinner.hide();
  }

  
  
  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.identity===true){
        //QUITAR LIVE
       // this.router.navigate([Rutas.livenessInstruction]);
        return false;
      } else {
        return true;
      }
    }
  }

    ti="";
    sentTi(ti_){
      this.ti=ti_;
      console.log(this.ti);
      this.sendDoc();

    }

    sendDoc() {
    if(this.ti!==""){
      this.typeIdentity = this.ti;
      console.log('ti= ' + this.typeIdentity );
      if(this.typeIdentity === "ID_CARD"){
        sessionStorage.setItem("serviceId", 'VID')
      }else{
        sessionStorage.setItem("serviceId", 'PP')
      }
      //let type= (this.typeIdentity === "ID_CARD") ? sessionStorage.setItem("serviceId", 'VID') : sessionStorage.setItem("serviceId", 'PP') ;
      //console.log("valor de type", type);
      this.servicesGeneralService.settI(this.typeIdentity);
      this.servicesGeneralService.setFrontAndBack('front');
      sessionStorage.removeItem('errorDocument');
      this.router.navigate([Rutas.documentInstruction]);
    }else{this.error='Seleccione un tipo de documento'}

  }
  typesIdentity(){
    this.servicesGeneralService.setFrontAndBack('front');
    if(this.object.servicios['documento']['propiedades']){
    this.type = this.object.servicios['documento']['propiedades'];
    console.log("=== " + this.type);
      if(this.type.includes('INE') && this.type.includes('PASSPORT') ){
        this.showINE=true;
        this.showPass=true;
        if(sessionStorage.getItem('ti'))
        {
          if(sessionStorage.getItem('ti').includes('ID_CARD')){
            this.servicesGeneralService.settI('ID_CARD');
            this.router.navigate([Rutas.documentInstruction]);
          }else if (sessionStorage.getItem('ti').includes('PASSPORT')){
            this.servicesGeneralService.settI('PASSPORT');
            this.router.navigate([Rutas.documentInstruction]);
          }
        }
      }else if(this.type.includes('INE')){
        this.servicesGeneralService.settI('ID_CARD');
        this.router.navigate([Rutas.documentInstruction]);
      }else if(this.type.includes('PASSPORT')){
        this.servicesGeneralService.settI('PASSPORT');
        this.router.navigate([Rutas.documentInstruction]);
      }
    }else{
      this.servicesGeneralService.settI('ID_CARD');
      this.router.navigate([Rutas.documentInstruction]);
    }
    
  }

  async cerrarModal() {
    const modal = document.getElementById('modalAlertBrowser');
    modal.style.display = 'none';
  }
  
  async abrirModal() {
    const modal = document.getElementById('modalAlertBrowser');
    modal.style.display = 'block';
    sessionStorage.setItem('alertBrowser', 'true');
  }


  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

  validateDevive(){
    let device = deviceInfo();
    console.log("información del dispositivo", device);
    switch (device['os.name']) {
      case 'Macintosh':
        console.log("Soy una mac con:", device['browser.name']);
        return true; 
        break;
      case 'Windows':
        console.log("Soy una windows con:", device['browser.name']);
        return true; 
        break;
      case 'Android':
        console.log("Soy un android con:", device['browser.name']);
        if(device['browser.name'] === "Firefox" || device['browser.name'] === "Chrome" || device['browser.name'] === "Opera" 
        || device['browser.name'] === "Safari" ){
          if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
            console.log("Soy un navegador valido android con:", device['browser.name'])
            return true; 
          }else{
            console.log("HOLA NAVEGADOR RECHAZED");
            return false;
          }
          
        }else{
          return false; 
        }
        break;
      case 'iPad':
        console.log("Soy un ipad con:", device['browser.name']);
        if( device['browser.name'] === "Safari" ){
          console.log("Soy un navegador valido android con:", device['browser.name']);
          if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
            console.log("Soy un navegador valido android con:", device['browser.name'])
            return true; 
          }else{
            console.log("HOLA NAVEGADOR RECHAZED");
            return false;
          }
        }else{
          //Redirect a página de error
          return false;
        }
        break;
      case 'iPhone':
        console.log("Soy un iphone con:", device['browser.name']);
        if( device['browser.name'] === "Safari" ){
          console.log("Soy un navegador valido android con:", device['browser.name']);
          if("mediaDevices" in navigator && "getUserMedia" in navigator.mediaDevices){
            console.log("Soy un navegador valido android con:", device['browser.name'])
            return true; 
          }else{
            console.log("HOLA NAVEGADOR RECHAZED");
            return false;
          }
        }else{
          //Redirect a página de error
          return false; 
        }
        break;
      default:
        return false; 
        break;
    }
  }

  copyLink(){
    var aux = document.createElement("input");
    var value = $("#link").val().toString();
    aux.setAttribute("value", value);
    document.body.appendChild(aux);
    console.log(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
    this.copiar="Copiado!";
    
    setTimeout(() => {
      this.copiar=" ";
    }, 2000);
  }
  
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesGeneralService, isMobile } from '../../../../services/general/services-general.service';
import { Rutas } from 'src/app/model/RutasUtil.js';
import { SessionService } from 'src/app/services/session/session.service.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { sesionModel } from '../../../../model/sesion/SessionPojo';
import { WorkFlowService } from '../../../../services/session/work-flow.service';
import {GlobalInstructionComponent} from '../../../global-instruction/global-instruction.component';
import { async } from '@angular/core/testing';
import { AnaliticService } from 'src/app/services/session/analitic.service';
import { MiddleMongoService } from '../../../../services/http/middle-mongo.service';
import { MiddleIdMissionService } from '../../../../services/http/middle-idMission.service';


declare function initializeSdk(): any;
declare function captureIDsdk(token): any;
declare function captureIDBacksdk(token): any
declare function detectFaceSdk(token): any;
declare function submitSdk(token): any;
 
@Component({
  selector: 'app-capture-document',
  templateUrl: './capture-document.component.html',
  styleUrls: ['./capture-document.component.css']
})
export class CaptureDocumentComponent implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  cameraDenied: boolean; 
  cameraCapture: boolean;
  PASSPORT_ASPECT_RADIO = 1.4205;
  ID_CARD_ASPECT_RADIO = 1.5858;
  filtersLoaded: Promise<boolean>;
  mensaje: string;
  id: string;
  dc: any;
  img: any;
  isMobileBool: boolean;
  isEdge: boolean;
  object: sesionModel;
  ti:string;
  dispositivosDeVideo = [];
  bNcams:boolean=true;
  intentsDocument;
  inc=1;
  videoEl;
  feedback:string;
  countIntentsResponse;
  showGaleryOption: boolean;
  showCameraOption: boolean;
  token;

  constructor(private workFlowService :WorkFlowService, public router: Router, public serviciogeneralService: ServicesGeneralService,
              private session: SessionService, private actRoute: ActivatedRoute, private spinner: NgxSpinnerService,
              private global: GlobalInstructionComponent, private analyticService: AnaliticService,
              private middle: MiddleMongoService, private idMission: MiddleIdMissionService) {
    this.htmlCanvasToBlob();
    if (serviciogeneralService.gettI() !== undefined) {
      sessionStorage.setItem('ti', serviciogeneralService.gettI());
    } else if (sessionStorage.getItem('ti') === undefined) {
      this.router.navigate([Rutas.chooseIdentity]);
    }

    console.log("ti= " + sessionStorage.getItem('ti'));
    this.ti=sessionStorage.getItem('ti');


  }

  async ngOnInit() {
    //this.token = await this.idMission.getToken();
    this.token = "6CE6CE8662D92C9B636AE283C1AFA4DD5A1F1A61BA2F0BFF30353D345B6A57A0.AXJLBC1PZHMWMTFBMTAUMC4XMDGUNTLD";
    initializeSdk();




  //  await this.spinner.show();
  //  this.showGaleryOption = false;
  //  this.cameraCapture = true; 

  //  this.dc = new DocumentCapture.Daon.DocumentCapture({
  //    url: environment.daonDocumentURL, // 'https://dobsdemo-docquality-first.identityx-cloud.com/rest/v1/quality/assessments',
  //    documentType: sessionStorage.getItem('ti')
  //  });

    /* if (!(await this.alredySessionExist())) { return; }
    this.id = this.object._id;
    this.countIntentsResponse = 0; 
    console.log("El tipo de documento es:", this.ti);

    if(sessionStorage.getItem('intentsDocument')){
      this.intentsDocument = sessionStorage.getItem('intentsDocument');
    }else{
      sessionStorage.setItem('intentsDocument', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("documento", device)
      console.log("El id es:",this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middle.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);
      this.intentsDocument = sessionStorage.getItem('intentsDocument'); 
    } */

    //
    /* const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    await this.spinner.hide();
    this.filtersLoaded = Promise.resolve(true);
    this.isMobileBool = isMobile(navigator.userAgent);
    this.isEdge = window.navigator.userAgent.indexOf('Edge') > -1;
    
    let dispositivos = await navigator.mediaDevices.enumerateDevices();
    dispositivos = dispositivos.filter(device => device.kind === "videoinput");
    for(let dispositivo of dispositivos){
      if ( dispositivo.label.includes('back') || dispositivo.label.includes('trasera') || dispositivo.label==='' ) {
        this.dispositivosDeVideo.push(dispositivo);
      }
    }
  
    this.bNcams=(this.dispositivosDeVideo.length==1);
    this.dc.stopCamera();
    console.log(this.dispositivosDeVideo);
    this.capturar(undefined); */
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.identity===true){
        this.router.navigate([Rutas.livenessInstruction]);
        return false;
      } else {
        return true;
      }
    }
  }

  captureID() {
    captureIDsdk(this.token);
  }

  captureIDBack() {
    captureIDBacksdk(this.token);
  }

  submit() {
    submitSdk(this.token);
   }

  async enter() {
    this.showGaleryOption = false;
    this.cameraCapture = true; 
    await this.spinner.show();

    // const { videoWidth, videoHeight } = this.dc.camera.videoEl;
    // const { clientWidth, clientHeight } = document.querySelector(".ui-canvas-container");

    // const documentTypeRatio=1.5858;
    // const rectCoords = getRectCoordsFromContainer({
    //   video: {
    //     x: videoWidth,
    //     y: videoHeight
    //   },
    //   container: {
    //     x: clientWidth,
    //     y: clientHeight
    //   },
    //   documentTypeRatio
    // })

    // function getContainerProjection({ ratios, container }) {
    //   const ratio = Math.min(ratios.x, ratios.y);
    //   return {
    //     x: container.x * ratio,
    //     y: container.y * ratio
    //   }
    // }
    // function getContainerProjectionRatios({ video, container }) {
    //   return {
    //     x: video.x / container.x,
    //     y: video.y / container.y
    //   }
    // }
    // function getRectCoordsFromContainer({ video, container, documentTypeRatio }) {
    //   const ratios = getContainerProjectionRatios({ video, container });
    //   const pContainer = getContainerProjection({ ratios, container });
    //   const offset = pContainer.x * 6.25 / 100;
    //   const width = pContainer.x - offset;
    //   return {
    //     upperLeftX: Math.trunc((video.x - pContainer.x) / 2 + offset / 2),
    //     upperLeftY: Math.trunc((video.y - pContainer.y) / 2 + offset * 2),
    //     width: Math.trunc(width),
    //     height: Math.trunc(width / documentTypeRatio)
    //   }
    // }
    // console.log("<<>> " + JSON.stringify(rectCoords) +
    // this.serviciogeneralService.getFrontAndBack());

    const queryParams = this.getAllCords();
    delete queryParams.coordenadasValue;
    delete queryParams.video;
    // this.dc.captureFrame()
      // .then(blob => 
        this.dc.captureMultipleFrames(
          {croppingTolerance:0.06,
        hintFacePresent: this.ti === 'INE' && sessionStorage.getItem('fb') === 'front',
        queryParams,
        numOfFrames: 2,
        timeout:600000
      }
        // )
        )
      .then(response => this.onServerFeedback(response))
      .catch(async err => {
        console.log('err= ' + err);
        await this.spinner.hide();
      });
  }

  getAllCords() {
    let coordenadasValue = this.measureOverlayCoordinates();
    const { videoWidth, videoHeight } = this.dc.camera.videoEl;
    const { clientWidth, clientHeight } = document.querySelector(".ui-canvas-container");
    let containerRatioX = videoWidth / clientWidth;
    let containerRatioY = videoHeight / clientHeight;
    const upperLeftX = Math.round(
      coordenadasValue.upperCanvasX * containerRatioX
    );
    const upperLeftY = Math.round(
      coordenadasValue.upperCanvasY * containerRatioY
    );
    const scanningRegionHeight = Math.round(
      coordenadasValue.scanningCanvasRegionHeight * containerRatioY
    );
    const scanningRegionWidth = Math.round(
      coordenadasValue.scanningCanvasRegionWidth * containerRatioX
    );
    const queryParams = {
      upperLeftX,
      upperLeftY,
      width: scanningRegionWidth,
      height: scanningRegionHeight,
      coordenadasValue,
      video : {
        videoWidth, videoHeight
      }
    };
    return queryParams;
  }

  async onServerFeedback(response) {
    if (response.result === 'FAIL') {
      this.countIntentsResponse = this.countIntentsResponse + 1; 
      this.feedback = response.feedback  && response.feedback !== null ? response.feedback : '';
      if(this.feedback.includes("Unable to find document,"))
      {this.mensaje = "No se encontró el documento."; }
      else if(this.feedback.includes("away from strong light")){
        this.mensaje = "busque un lugar más obscuro y sin reflejos";}
      else if(this.feedback.includes("fetch")){
        this.mensaje = "Error al leer el documento";}
      else if(this.feedback.includes("STRAIGHTEN")){
        this.mensaje = "Favor de ajustar el documento";}
      else if(this.feedback.includes("TypeError: Failed to fetch")){
          this.mensaje = "Favor de ajustar el documento";}
        else{
        this.mensaje = "No se encontró el documento.";
      }
      
      //TODO
      if(this.countIntentsResponse % 2 == 0) {
       this.showGaleryOption = true; 
       this.cameraCapture = false; 
    }
      console.log('no pasa');
      await this.spinner.hide();
    } else {
      console.log('si pasa + ' + JSON.stringify(response));
      this.showGaleryOption = false; 
      this.cameraCapture = true; 
      this.img = 'data:image/jpeg;base64,' + response.responseBase64Image;
      this.dc.stopCamera();
      console.log(this.img);
      this.serviciogeneralService.setImg64(this.img);
      this.serviciogeneralService.setIsUpload(false);
      await this.spinner.hide();
      this.router.navigate([Rutas.documentConfirm]);
    }
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  measureOverlayCoordinates = () => {
    //defining rectangle overlay coordinates
    const { clientWidth, clientHeight } = document.querySelector(
      ".ui-canvas-container"
    );
    console.log(clientWidth);
    console.log(clientHeight);
    const upperCanvasX = 0.06 * clientWidth;
    const scanningCanvasRegionWidth = clientWidth - 2 * upperCanvasX;
    const scanningCanvasRegionHeight =
    this.ti === "PASSPORT"
        ? scanningCanvasRegionWidth / this.PASSPORT_ASPECT_RADIO
        : scanningCanvasRegionWidth / this.ID_CARD_ASPECT_RADIO;
    const upperCanvasY = clientHeight / 2 - scanningCanvasRegionHeight / 2;
    // return coordinates so we can use them to draw on canvas and send coords on docquality
    return {
      upperCanvasX,
      upperCanvasY,
      scanningCanvasRegionHeight,
      scanningCanvasRegionWidth,
      clientWidth,
      clientHeight
    };
  };

  drawCanvasRectangle = (coords) => {
    console.log('voy a escribir en el canvas');
    var canvas = <HTMLCanvasElement> document.getElementById("canvas") ;
    console.log(canvas);
    canvas.width = coords.clientWidth;
    canvas.height = coords.clientHeight;
    var ctx = canvas.getContext("2d");
    ctx.lineWidth = 1;
    ctx.strokeStyle = "white";
    ctx.fillStyle = "rgba(255,255,255,0.5)";
    ctx.beginPath();
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.clearRect(
      coords.upperCanvasX,
      coords.upperCanvasY,
      coords.scanningCanvasRegionWidth,
      coords.scanningCanvasRegionHeight
    );
    ctx.rect(
      coords.upperCanvasX,
      coords.upperCanvasY,
      coords.scanningCanvasRegionWidth,
      coords.scanningCanvasRegionHeight
    );
    ctx.stroke();
  };


  blobToBase64(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      try {
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
          resolve(reader.result);
        };
      } catch (err) {
        reject(err);
      }
    });
  }

  async enfocar(){    
    await this.dc.stopCamera();
    if(this.dispositivosDeVideo.length>this.inc){
      this.inc=this.inc+1;
    }else{
      this.inc=1;
    }
    this.capturar(this.dispositivosDeVideo[this.dispositivosDeVideo.length-this.inc].deviceId);
  }

  capturar(idC) {
    const c = this.canvas;
    this.mensaje = 'Posiciona tu documento dentro del área';
    console.log('captura');
    this.drawCanvasRectangle(this.measureOverlayCoordinates());

    this.videoEl = document.querySelector('video');
    
    this.dc.startCamera(this.videoEl, idC).then((response) => {
      console.log(response);
      let cordenadas = this.getAllCords();
      this.videoEl.onloadedmetadata = function () {
        cordenadas.width = cordenadas.video.videoWidth; 
        cordenadas.height = cordenadas.video.videoHeight;
      };
      this.cameraCapture = true; 
    }).catch(err => {
      this.abrirModal();
      console.log(err);
    });
  }

  reload(){
    console.log("permisos");
    location.reload();
  }

  htmlCanvasToBlob() {
    if (!HTMLCanvasElement.prototype.toBlob) {
      console.log('HTMLCanvasElement.prototype.toBlob 1 ' + HTMLCanvasElement.prototype.toBlob);
      Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
        value: function (callback, type, quality) {
          let canvas = this;
          setTimeout(() => {
            var binStr = atob(canvas.toDataURL(type, quality).split(',')[1]),
              len = binStr.length,
              arr = new Uint8Array(len);
            for (let i = 0; i < len; i++) {
              arr[i] = binStr.charCodeAt(i);
            }
            callback(new Blob([arr], { type: type || 'image/png' }));
          });
        }
      });
    }
  }

  async regresar() {
    const modal = document.getElementById('modalPermission');
    modal.style.display = 'none';
    this.cameraCapture = false;  
    this.router.navigate([Rutas.documentInstruction]);
  }

  stopCameraJs(){
    var stream = navigator.mediaDevices.getUserMedia({video: true}).then(mediaStream => {
      document.querySelector('video').srcObject = mediaStream;
      // Stop the stream after 5 seconds
      setTimeout(() => {
        const tracks = mediaStream.getTracks();
        console.log("los tracks son", tracks);
        tracks[0].stop()
      }, .001);

    });
  }

  async cerrarModal() {
    const modal = document.getElementById('modalPermission');
    modal.style.display = 'none';
    // this.enfocar();
    
  }
  
  async abrirModal() {
    console.log("abrir modal");
    const modal = document.getElementById('modalPermission');
    modal.style.display = 'block';
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }

  async processFile(imageInput: any) {
    await this.spinner.show();
    this.intentsDocument =  parseInt(this.intentsDocument) + 1;
    sessionStorage.setItem('intentsDocument', this.intentsDocument);

    try {
      this.mensaje = '';
      const file: File = imageInput.files[0];
      console.log(file);
      let requestDaonDocument = {
        blob:file, croppingTolerance: 0.05
      }
      if(sessionStorage.getItem('ti')==='ID_CARD' && sessionStorage.getItem('fb') === 'front') {
        requestDaonDocument['hintFacePresent'] = true;
      }
      this.dc.assessQuality(requestDaonDocument,"",(this.serviciogeneralService.getFrontAndBack() ==='front'))
      // this.dc.captureMultipleFrames({blob:file, croppingTolerance: 0.06,})
        .then(async response => {
          console.log(response);
          if (response.result === 'FAIL') {
            this.intentsDocument =  parseInt(this.intentsDocument) + 1;
            sessionStorage.setItem('intentsDocument', this.intentsDocument);
          //  this.titulo = "Error en el documento";
            this.mensaje = 'Asegúrate que el documento esté sobre un fondo oscuro';
            console.log(this.mensaje);
            console.log('no pasa');
            await this.spinner.hide();
          } else if (response.result === 'PASS') {
            this.intentsDocument =  parseInt(this.intentsDocument) + 1;
            sessionStorage.setItem('intentsDocument', this.intentsDocument);
            this.dc.stopCamera();
            this.dc.stopAutoCapture();
            this.img = 'data:image/jpeg;base64,' + response.responseBase64Image;
            this.serviciogeneralService.setImg64(this.img);
            this.serviciogeneralService.setIsUpload(true);

            await this.spinner.hide();
            this.router.navigate([Rutas.documentConfirm]);
          }
        })
        .catch(async err => {
          console.log('err= ' + err);
          //this.titulo = "Error en el documento";
          this.mensaje = "Imagen no permitida. Asegúrate que el documento esté sobre un fondo oscuro";
          await this.spinner.hide();
        });
    } catch (e) {
      console.log('Error en el servicio intente de nuevo: ', e);
      this.mensaje = 'Error en el servicio intente de nuevo ';
      await this.spinner.hide();
    }
  }

  continueCamera(){
    this.showGaleryOption = false; 
    this.cameraCapture = true;
    this.mensaje = "";
  }
}

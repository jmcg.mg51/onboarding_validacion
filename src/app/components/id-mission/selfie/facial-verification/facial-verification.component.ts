import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { SelfieSend } from 'src/app/model/DaonPojos/Selfie';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { SessionService } from 'src/app/services/session/session.service';
import { ErrorSelfieService } from 'src/app/services/errores/error-selfie.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesGeneralService, isMobile, isAndroid } from 'src/app/services/general/services-general.service';
import { MiddleMongoService } from 'src/app/services/http/middle-mongo.service';
import { MiddleDaonService } from 'src/app/services/http/middle-daon.service';
import { Rutas } from 'src/app/model/RutasUtil';
import { environment } from '../../../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import {NgxImageCompressService} from 'ngx-image-compress';
import { WorkFlowService } from '../../../../services/session/work-flow.service';
import {GlobalInstructionComponent} from '../../../global-instruction/global-instruction.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
@Component({
  selector: 'app-facial-verification',
  templateUrl: './facial-verification.component.html',
  styleUrls: ['./facial-verification.component.css']
})

export class FacialVerificationComponent implements OnInit {
  constructor(private workFlowService :WorkFlowService, private imageCompress: NgxImageCompressService, private serviciogeneralService: ServicesGeneralService, private router: Router,
              private http: HttpClient, private session: SessionService, private actRoute: ActivatedRoute,
              private mongoMid: MiddleMongoService, private middleDaon: MiddleDaonService,
              private errorSelfieService: ErrorSelfieService, private spinner: NgxSpinnerService,
              private global: GlobalInstructionComponent, private analyticService: AnaliticService) { 
                this.img = this.serviciogeneralService.getImg64();
              }

  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private data: SelfieSend;
  filtersLoaded: Promise<boolean>;
  @Input() public id: string;
  //@Input() public foto: any;
  error: any;
  isMobileBool: boolean;
  isEdge: boolean;
  isAndroid: boolean;
  isNative: boolean;
  img: any;
  public foto = ' ';
  object: sesionModel;
  typeIdentity: string;
  intentsSelfie;

  async ngOnInit() {
    if (!(await this.alredySessionExist())) { return; }
    if(sessionStorage.getItem('intentsSelfie')){
      this.intentsSelfie = sessionStorage.getItem('intentsSelfie');
    }else{
      sessionStorage.setItem('intentsSelfie', '0')
      this.intentsSelfie = sessionStorage.getItem('intentsSelfie'); 
    }
    this.id = this.object._id;
    this.foto="";
    const fp = await FP.load({token: environment.fingerJsToken, 
 endpoint: environment.fpDomain});
    fp.get({tag: {'tag':this.id}});
    console.log('img = ' + this.img);
    this.foto = 'data:image/png;base64,' + await this.blobToBase64(this.img);
    console.log('Foto= ' + this.foto);
    this.filtersLoaded = Promise.resolve(true);
    this.isMobileBool = isMobile(navigator.userAgent);
    this.isAndroid = isAndroid(navigator.userAgent);
    this.isEdge = window.navigator.userAgent.indexOf('Edge') > -1;
    this.isNative = this.serviciogeneralService.getIsCamNative();
    this.img = this.serviciogeneralService.getImg64();

    
  }

  
  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if(this.object.daon.selfie===true){
        this.router.navigate([this.workFlowService.redirectFlow('selfie',this.object.servicios)]);
        
        return false;
      } else {
        return true;
      }
    }
  }

  compressFileSelfie(img) {
    
    this.imageCompress.compressFile(img, -1, 50, 50).then(
      result => {
        
        this.serviciogeneralService.setFoto(result);
        
      }
    );

  }
  async enviar() {
    
    const jsonSendFaceDaon = {
      data: this.foto,
    };

    const resultCode = await this.middleDaon.sendInfoDaon(jsonSendFaceDaon, this.id, 'selfie');
    if (resultCode !== 200) {
      console.log('ocurrio un error, favor de reintentar');
      console.log('voy a redirigir a : ' + Rutas.selfie);
      this.errorSelfieService.mensaje = 'Error, favor de volver a intentar';
      this.router.navigate([Rutas.instrucciones]);
      return false;
    }
    return true;
  }

  async guardar() {
    await this.spinner.show();
    console.log('voy a enviar');
    if (await this.enviar()) {
      const object = this.session.getObjectSession();
      object.daon.selfie = true;
      this.session.updateModel(object);
      this.compressFileSelfie(this.foto);
      this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
      sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("selfie", device, this.intentsSelfie, "NO", false);
      console.log("El valor actual de analytics es:", analytics);
      await this.mongoMid.updateDataUser(analytics, this.id);
      //Guardar los valores de tiempo
      await this.middleDaon.updateDaonDataUser(object, this.id);
      console.log('ya termine' + JSON.stringify(object, null, 2));
      //this.router.navigate([Rutas.chooseIdentity]);
      this.selectTypeDocument();
    }
    await this.spinner.hide();
  }

  async selectTypeDocument(){
      sessionStorage.removeItem('errorDocument');
      this.router.navigate([this.workFlowService.redirectFlow('selfie',this.object.servicios)]);
  }

  blobToBase64(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      try {
        console.log("HOLAAAAAAA", blob );
        if(blob){
          reader.readAsDataURL(blob);
          reader.onloadend = () => {
            resolve(reader.result.toString().replace('data:image/jpeg;base64,', ''));
          };
          this.setColor();
        }else{
          this.router.navigate([Rutas.selfie]);
        }
        
      } catch (err) {
        this.setColor();
        reject(err);
      }
    });
  }

  setColor(){
    $("#againSelfie").css("color","#6c757d");

    $("#confirmSelfie").hover(function(){
      var movilContinue = document.getElementById("againSelfie") as HTMLInputElement;
      movilContinue.style.backgroundColor = "white"; 
      movilContinue.style.color = "#6c757d";
      });

      $("#againSelfie").hover(function(){
        var colorHover = sessionStorage.getItem("colorButton");
        var colorLabelHover = sessionStorage.getItem("colorLabel");
        $(this).css("background-color", colorHover);
        $(this).css("color", colorLabelHover);
        

        });
  }
  back() {
    this.intentsSelfie =  parseInt(this.intentsSelfie) + 1;
    sessionStorage.setItem('intentsSelfie', this.intentsSelfie);
    this.router.navigate([Rutas.selfie]);
  }

  public getColorButton(){
    return this.global.getButtonColorSystem();
  }
  
  public getColorLabel(){
    return this.global.getLabelColorSystem();
  }

  public getImageSystem(){
    return this.global.getImageConfig();
  }


}

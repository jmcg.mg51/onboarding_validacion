import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServicesGeneralService {

  img64:String;
  tI:string;
  frontAndBack:string;
  isUpload:boolean;
  resultLiveness:string;
  mensajeLiveness:string;
  isCamNative:boolean;
  correo:string;
  tipoIdentificacion: string;

  public INE = 'INE';
  public PASSPORT = 'PASSPORT';

  constructor(private http: HttpClient) { }

  setTipoIdentificacion(tipoIdentificacion: string) {
    this.tipoIdentificacion = tipoIdentificacion;
  }

  getTipoIdentificacion() {
    return this.tipoIdentificacion;
  }

  setIsCamNative(b){
    this.isCamNative=b;
  }

  getIsCamNative(){
    return this.isCamNative;
  }

  setIsUpload(b){
    this.isUpload=b;
  }

  getIsUpload(){
    return this.isUpload;
  }
  setResultLiveness(result){
    this.resultLiveness=result;
  }

  getResultLiveness() {
    return this.resultLiveness;
  }

  setMensaje(mensaje) {
    this.mensajeLiveness = mensaje;
  }

  getMensajeLiveness() {
    return this.mensajeLiveness;
  }

  settI(ti) {
    this.tI = ti;
  }

  gettI(): string{
    return this.tI;
  }

  setImg64(img) {
    
    this.img64 = img;
    console.log("serrrr= " +this.img64);
  }

  getImg64(): String {
    return this.img64;
  }

  setFoto(foto){
    sessionStorage.setItem('foto', foto);
  }

  getFoto(){
    return sessionStorage.getItem('foto');
  }

  setFrontAndBack(fb) {
    console.log("frontback", fb);
    this.frontAndBack = fb;
  }

  getFrontAndBack() {
    return this.frontAndBack;
  }

  setCorreo(correo_){
    this.correo = correo_;
  }

  getCorreo(){
    return this.correo;
  }

  headers = new HttpHeaders().set('Content-Type', 'application/json',).set('Accept','application/json');


  sendImgDaon(data): Observable<any> {
    const url = `api/mitsoluciones3/IdentityXServices/rest/v1/users/QTAznsU-sPmUj0XyvprQAjjE4Q/face/samples`;

    return this.http.post(url, data, { headers: this.headers,  }).pipe(
      catchError(this.errorMgmt)
    );
  }

  sendDocDaon(data): Observable<any> {
    const url = `https://dobsdemo-idx-first.identityx-cloud.com/mitsoluciones3/DigitalOnBoardingServices/rest/v1/users/QTAzJc8L4vVRCaztQuscK0B7uQ/idchecks/Yf7ALTADELDB_q13FHKHfw/documents`;

    return this.http.post(url, data, { headers: this.headers,  }).pipe(
      catchError(this.errorMgmt)
    );
  }


  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log("errrrrrrrrrrrr= " + errorMessage);
    return throwError(errorMessage);
  }
}

export function isMobile(userAgent) {
  return !!userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone)/i);
}



export function isAndroid(userAgent) {
  return !!userAgent.match(/(Android)/i);
}

export function isIphone(userAgent) {
  return !!userAgent.match(/(iPhone)/i);
}

export function isBrowserOk(userAgent){
  // console.log("userAgent",userAgent);
  return !!userAgent.match(/(Chrome|Safari|Opera|Firefox|Edge|Mozilla)/i);
  //this.isEdge = window.navigator.userAgent.indexOf('Edge') > -1;
}
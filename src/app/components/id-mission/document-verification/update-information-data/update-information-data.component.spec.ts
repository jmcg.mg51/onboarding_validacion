import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInformationDataComponent } from './update-information-data.component';

describe('UpdateInformationDataComponent', () => {
  let component: UpdateInformationDataComponent;
  let fixture: ComponentFixture<UpdateInformationDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInformationDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInformationDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

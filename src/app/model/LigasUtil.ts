import { environment } from '../../environments/environment';
class LigasUtil {

  urlMiddleMongo(): string {
    // console.log(environment);
    // return 'https://2u597e7kmf.execute-api.us-east-1.amazonaws.com/test/usuario';
    return environment.baseUrl;
  }

  urlMiddleDaon(idTracking: string) {
    // return `https://2u597e7kmf.execute-api.us-east-1.amazonaws.com/test/usuario/${idTracking}/daon`;
    return environment.baseUrl + `/${idTracking}/daon`;
  }

  urlMiddleRoot(idTracking: string) {
    // return `https://2u597e7kmf.execute-api.us-east-1.amazonaws.com/test/usuario/${idTracking}/daon`;
    return environment.baseUrl + `/${idTracking}/`;
  }

  urlMiddleNubarium(): string {
    // console.log(environment);
    // return 'https://2u597e7kmf.execute-api.us-east-1.amazonaws.com/test/usuario';
    return environment.nubariumUrl;
  }

  urlFingerPrintJs(): string {
    return environment.nubariumUrl + '/webhook/fingerprintjs';
  }

  urlIdMission(): string {
    return environment.idmission;
  }
}

export const LigaUtil = new LigasUtil();

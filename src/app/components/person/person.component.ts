import { Component, OnInit } from '@angular/core';
import { Rutas } from 'src/app/model/RutasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, NavigationEnd, RouterModule } from '@angular/router';
import { SessionService } from '../../services/session/session.service';
import { MiddleDaonService } from '../../services/http/middle-daon.service';
import { sesionModel } from '../../model/sesion/SessionPojo';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { environment } from '../../../environments/environment';
import FP from '@fingerprintjs/fingerprintjs-pro';
import { ɵAnimationGroupPlayer } from '@angular/animations';
import { Session } from 'protractor';
import { WorkFlowService } from '../../services/session/work-flow.service';
import { TermsComponent } from '../terms/terms.component';
import { AnaliticService } from 'src/app/services/session/analitic.service';
@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']

})
export class PersonComponent implements OnInit {

  title = '¿Eres persona física o moral';
  imgUrlPersonaFisica = '../../../../../assets/img/person/2.2.persona_fisica.png';
  imgUrlPersonaMoral = '../../../../../assets/img/person/2.1.persona_moral.png';
  labelPersonaFisica = 'Persona </br> física  ';
  labelPersonaMoral = 'Persona moral';
  btnTitlePersonaFisica = 'Solicitaremos tu información personal';
  btnTitlePersonaMoral = 'Solicitaremos la información del representante legal';
  btnTitleContinuar = 'Continuar';


  constructor(private workFlowService: WorkFlowService, private spinner: NgxSpinnerService, public router: Router,
    private session: SessionService, private actRoute: ActivatedRoute, private middleDaon: MiddleDaonService,
    private middleMongo: MiddleMongoService,
    private term: TermsComponent, private analyticService: AnaliticService) {
  }

  filtersLoaded: Promise<boolean>;
  errorMensaje: string;
  id: string;
  rfcModel: string;
  razonSocialModel: string;
  valorPersonaModel: string;
  object: sesionModel;
  respuesta: string;
  RFCRequest: boolean;
  colorButton: string;
  intentsSelectPerson;
  none = "none";
  noneImportant = "none !important";
  display = "block";
  nonSelectColor = "#6c757d";
  selectColor = sessionStorage.getItem("colorButton").concat(" !important");
  typePerson: string;
  isRfcEmpty: boolean;
  showRfcFisica: boolean;
  isRfcMoralEmpty = true;
  errorTipoPersona: string;
  errorMessageRFC: string;
  errorMessageMatch: string;
  //inputPersonaFisica; 

  async ngOnInit() {
    await this.spinner.show();

    this.colorButton = sessionStorage.getItem("colorButton");
    if (!(await this.alredySessionExist())) { return; }
    console.log('el objeto es: ' , this.object);
    
    (document.getElementById("contenido") as HTMLElement).style.display = "none";
    if(this.object && this.object.servicios && this.object.servicios['rfc']
      && this.object.servicios['rfc'].propiedades && this.object.servicios['rfc'].propiedades.includes('vacio')) {
        console.log('si entre a setear true');
        this.isRfcEmpty = true;
    }

    this.id = this.object._id;
    if (sessionStorage.getItem('intentsPerson')) {
      this.intentsSelectPerson = sessionStorage.getItem('intentsPerson');
    } else {
      sessionStorage.setItem('intentsPerson', '0')
      this.intentsSelectPerson = sessionStorage.getItem('intentsPerson');
      sessionStorage.setItem('intentsEmail', '0');
      const device = this.analyticService.getTypeDevice();
      const initAnalytics = this.analyticService.initAnalytics("rfc", device);
      console.log("El id es:", this.id);
      console.log("Lo que voy a guardar es:", initAnalytics);
      var result = await this.middleMongo.updateDataUser(initAnalytics, this.id);
      console.log("El resultado es:", result);

    }
    const fp = await FP.load({ token: environment.fingerJsToken, 
 endpoint: environment.fpDomain });
    fp.get({ tag: { 'tag': this.id } });

    this.object = this.session.getObjectSession();
    this.setStyleInputs(this.none, this.none, this.none, this.none, this.none);
    var valueRFC = this.object.servicios["rfc"];
    console.log("console.log", valueRFC);

    document.getElementById("LinkPersonaFisica").addEventListener("click", function (event) {
      event.preventDefault()
    });

    document.getElementById("linkPersonaMoral").addEventListener("click", function (event) {
      event.preventDefault()
    });

    if(sessionStorage.getItem("rfc") ){
      //this.typePerson = "fisica";
      if(sessionStorage.getItem("rfc") === "XAXX010101000"){
        // console.log("recupero el rfc");
        // this.title = '';
        // this.title = 'Persona física';
        // document.getElementById("divPersonMoral").style.display = "none";
        // document.getElementById("divPerson").classList.remove("col-6");
        // document.getElementById("divPerson").classList.add("col-12");
        // document.getElementById("borderFisica").classList.add("onlyFisica");
        // this.showRfcFisica = true;
       // this.typePerson = "fisica";
        // $('#rfc').val("");
        // $('#rfc').attr('maxlength', 13);
        this.setColorFisica(this.selectColor);
        this.setStyleInputs(this.none, this.none, this.none, this.display, this.none);
      }else{
        await this.saveDataPerson(this.typePerson, sessionStorage.getItem("rfc"));
      }
    }
    await this.setRFCView();
    this.filtersLoaded = Promise.resolve(true);
    this.deleteEmojis();
    await this.spinner.hide();
  }

  async alredySessionExist() {
    this.object = this.session.getObjectSession();
    console.log(this.object);
    if (this.object === null || this.object === undefined) {
      this.router.navigate([Rutas.error]);
      return false;
    } else {
      if (this.object.datosFiscales === true) {
        this.router.navigate([this.workFlowService.redirectFlow('rfc', this.object.servicios)]);
        return false;
      } else {
        return true;
      }
    }
  }

  selectedMoral() {
    this.showRfcFisica = true;
    this.isRfcMoralEmpty = false;
    $('#rfc').val("");
    $('#rfc').attr('maxlength', 12);
    this.typePerson = "moral"
    this.setColorMoral(this.selectColor);
    this.setStyleInputs(this.none, this.display, this.none, this.display, this.none);
  }

  checkNubariumStatus() {
    var valueRFC = this.object.servicios["rfc"];
    this.showRfcFisica = true;
    //valueRFC.capturarRfcFisica = true; 
    if (valueRFC && valueRFC.capturarRfcFisica === true) {
      console.log("checkNubariumStatus: persona fisica nabarium OFF", valueRFC.capturarRfcFisica);
      this.typePerson = "fisica"
      $('#rfc').val("");
      $('#rfc').attr('maxlength', 13);
      this.setColorFisica(this.selectColor);
      this.setStyleInputs(this.none, this.none, this.none, this.display, this.none);
    } else {
      console.log("checkNubariumStatus: persona fisica nabarium ON");
      var color = "black"
      this.setColorFisica(this.selectColor);
      this.setStyleInputs(this.none, this.none, this.none, this.none, this.none);
      this.saveDataPerson("fisica", '');

    }

  }

  setColorMoral(color) {
    //#6c757d
    var selectPersonaFisica = document.getElementById("borderFisica") as HTMLInputElement;
    var titlePersonFisica = document.getElementById("titleFisica") as HTMLInputElement;
    titlePersonFisica.setAttribute("style", "color: none !important");
    selectPersonaFisica.setAttribute("style", "border-color: none !important");

    var selectPersonaMoral = document.getElementById("borderMoral") as HTMLInputElement;
    var titlePersonMoral = document.getElementById("titleMoral") as HTMLInputElement;
    selectPersonaMoral.setAttribute("style", "border-color:" + color);
    titlePersonMoral.setAttribute("style", "color:" + color);
  }

  setColorFisica(color) {
    var selectPersonaMoral = document.getElementById("borderMoral") as HTMLInputElement;
    var titlePersonMoral = document.getElementById("titleMoral") as HTMLInputElement;
    titlePersonMoral.setAttribute("style", "color: #6c757d !important");
    selectPersonaMoral.setAttribute("style", "border-color: none !important");

    var selectPersonaFisica = document.getElementById("borderFisica") as HTMLInputElement;
    var titlePersonFisica = document.getElementById("titleFisica") as HTMLInputElement;
    selectPersonaFisica.setAttribute("style", "border-color:" + color);
    titlePersonFisica.setAttribute("style", "color:" + color);
  }

  async enter() {
    console.log("Funcion ENTER");
    // patron del RFC, persona moral
    this.intentsSelectPerson = parseInt(this.intentsSelectPerson) + 1;
    sessionStorage.setItem('intentsPerson', this.intentsSelectPerson);
    const _rfc_pattern_pm = '^(([A-Z�&]{3})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|' +
      '(([A-Z�&]{3})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|' +
      '(([A-Z�&]{3})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|' +
      '(([A-Z�&]{3})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$';
    const _rfc_pattern_pf = '^(([A-Z�&]{4})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|' +
      '(([A-Z�&]{4})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|' +
      '(([A-Z�&]{4})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|' +
      '(([A-Z�&]{4})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$';

    let inputRFC;
    var person = this.typePerson;
    if(this.object && this.object.servicios && this.object.servicios['rfc']
    && this.object.servicios['rfc'].propiedades && this.object.servicios['rfc'].propiedades.includes('vacio')) {
      if(person === 'fisica') {
        inputRFC = 'XAXX010101000';
      } else { 
        inputRFC = 'RDL0904102F4';
      }
    } else {
      inputRFC = this.rfcModel;
    }
    if (inputRFC) {
      inputRFC = inputRFC.toUpperCase();
    }
    // const person = $('#valorTipoPersona').val();
    console.log(person);
    console.log(inputRFC);
    if (person === 'fisica') {
      sessionStorage.setItem('typePerson', '1');
      console.log("La persona fisica");
      if (inputRFC.match(_rfc_pattern_pf)) {
        console.log('La estructura de la clave de RFC es valida');
        this.errorMessageRFC = undefined;
        this.errorTipoPersona = undefined;
        await this.saveDataPerson(person, inputRFC);

        return true;
      } else {
        console.log('La estructura de la clave de RFC fisica es INVALIDA');
        this.errorMessageRFC = 'Ingrese un RFC valido';

        return false;
      }
    } else if (person === 'moral') {
      sessionStorage.setItem('typePerson', '2');
      if (inputRFC.match(_rfc_pattern_pm) || inputRFC == 'RDL0904102F4') {

        console.log('La estructura de la clave de RFC moral es valida');
        this.errorMessageRFC = '';
        this.errorTipoPersona = undefined;

        await this.saveDataPerson(person, inputRFC);

      } else {
        console.log('La estructura de la clave de RFC moral es INVALIDA');
        this.errorMessageRFC = 'Ingrese un RFC valido';
        return false;
      }
    } else {
      this.errorTipoPersona = 'Debe seleccionar un tipo de persona';
      console.log('debes seleccionar un tipo de persona');
    }
  }

  async saveDataPerson(typePerson: string, rfc: string) {
    console.log('TIPO DE PERSONA: ', typePerson)
    if (typePerson === 'fisica') {
      await this.spinner.show();
      var rfcValue;
      if (this.rfcModel) {
        rfcValue = this.rfcModel
      } else {
        rfcValue = rfc;
      }
      rfcValue = rfcValue.toUpperCase();
      console.log("rfcValue", rfcValue);
      const objectPer = { datosFiscales: { rfc: rfcValue, tipoPersona: typePerson } };
      this.object.datosFiscales = true;
      this.session.updateModel(this.object);

      //await this.middleMongo.updateDataUser(objectPer, this.id);
      //this.analiticService.jsonFormatAnalitic('rfc', this.object, this.intentsSelectPerson);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("rfc", device, this.intentsSelectPerson, "NO", false);
      await this.middleMongo.updateDataUser(analytics, this.id);
      await this.middleMongo.updateDataUser(objectPer, this.id);
      await this.spinner.hide();
      this.router.navigate([this.workFlowService.redirectFlow('rfc', this.object.servicios)]);

    } else if (typePerson === 'moral') {
      rfc = rfc.toUpperCase();
      const objectPer = { datosFiscales: { rfc, nombre: this.razonSocialModel, tipoPersona: typePerson } };
      console.log('Este objeto no lo reconoce: ', objectPer)
      //this.analiticService.jsonFormatAnalitic('rfc', objectPer, this.intentsSelectPerson);
      const device = this.analyticService.getTypeDevice();
      const analytics = this.analyticService.updateAnalytics("rfc", device, this.intentsSelectPerson, "NO", false);
      await this.middleMongo.updateDataUser(analytics, this.id);
      await this.middleMongo.updateDataUser(objectPer, this.id).then(response => {
        console.log("Respuesta", response);
        if (response == "ERROR") {
          console.log('La estructura de la clave de RFC moral es INVALIDA');
          this.errorMessageMatch = 'El RFC no coincide con la razón social, verifique la información';
        }
        if (response == "OK") {
          this.respuesta = "200";
        }
        //response =>  {
        //console.log("envio datos", response);
        //this.respuesta = "200"; 
      })
        .catch(error => {
          console.warn('from component:', error);
          this.errorMessageMatch = 'El RFC no coincide con la razón social, verifique la información';
          // this console warn never gets logged out
        });

    }
    console.log("la respuesta", this.respuesta);
    if (this.respuesta == "200") {
      this.errorMessageMatch = '';
      this.object.datosFiscales = true;
      this.session.updateModel(this.object);
      //console.log('ya termine con los datos fiscales' + JSON.stringify(objectPer, null, 2));
      const modal = document.getElementById('modalPersonaMoral');
      modal.style.display = 'block';
    }


  }



  async aceptar() {
    console.log("Funcion ACEPTAR");
    await this.spinner.show();
    this.router.navigate([this.workFlowService.redirectFlow('rfc', this.object.servicios)]);
    await this.spinner.hide();
  }

  async cerrarModal() {
    const modal = document.getElementById('modalPersonaMoral');
    modal.style.display = 'none';
  }

  public deleteEmojis() {
    let body = <HTMLDivElement>document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/dontUseEmojis.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }

  setStyleInputs(errorRFC, razonSocial, errorMessage, rfc, errorTipo) {
    document.getElementById("razonSocial") && document.getElementById("razonSocial") != null
    ? document.getElementById("razonSocial").style.display = razonSocial : '';

    // document.getElementById('errorMessageMatch') && document.getElementById('errorMessageMatch') != null
    // ? document.getElementById('errorMessageMatch').style.display = errorMessage : '';

    document.getElementById("rfc") && document.getElementById("rfc") != null 
    ? document.getElementById("rfc").style.display = rfc : '';

    // document.getElementById("errorMessageTipoPersona") && document.getElementById("errorMessageTipoPersona") !=null 
    // ? document.getElementById("errorMessageTipoPersona").style.display = errorTipo : '';

    // document.getElementById("errorMessageRFC") && document.getElementById("errorMessageRFC") != null
    // ? document.getElementById("errorMessageRFC").style.display = errorRFC : '';

  }

  async setRFCView(){
    await this.spinner.show();
    if(sessionStorage.getItem("havePhysicalRFC") && sessionStorage.getItem("haveMoralRFC")){
      //view
    }else if(sessionStorage.getItem("havePhysicalRFC")){
        //console.log("recupero el rfc");
        this.showRfcFisica = true;
        this.title = '';
        this.title = 'Ingresa tu RFC';
        document.getElementById("divPersonMoral").style.display = "none";
        document.getElementById("divPerson").classList.remove("col-6");
        document.getElementById("divPerson").classList.add("col-12");
        document.getElementById("borderFisica").classList.add("onlyFisica");
       this.typePerson = "fisica";
        $('#rfc').val("");
        $('#rfc').attr('maxlength', 13);
        this.setColorFisica(this.selectColor);
        this.setStyleInputs(this.none, this.none, this.none, this.display, this.none);
        
    }else if(sessionStorage.getItem("haveMoralRFC")){
      console.log('entre a moral');
      this.showRfcFisica = false;
      this.title = '';
        this.title = 'Ingresa tu RFC';
       this.typePerson = "moral";
      document.getElementById("divPerson").style.display = "none";
      document.getElementById("divPersonMoral").classList.remove("col-6");
      document.getElementById("divPersonMoral").classList.add("col-12");
      document.getElementById("borderMoral").classList.add("onlyFisica");
        $('#rfc').val("");
        $('#rfc').attr('maxlength', 12);
        this.setColorFisica(this.selectColor);
        this.setStyleInputs(this.none, this.none, this.none, this.display, this.none);
        this.selectedMoral();
    }
    (document.getElementById("contenido") as HTMLElement).style.display = "block";
    this.spinner.hide();
  }

  public getColorButton() {
    return this.term.getButtonColorSystem();
  }

  public getColorLabel() {
    return this.term.getLabelColorSystem();
  }

  public getImageSystem() {
    return this.term.getImageConfig();
  }

}


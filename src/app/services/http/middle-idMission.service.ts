import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { sesionModel } from 'src/app/model/sesion/SessionPojo';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { LigaUtil } from 'src/app/model/LigasUtil';

@Injectable({
    providedIn: 'root'
  })
  export class MiddleIdMissionService {
  
    constructor(private http: HttpClient) { }
  
    headers = new HttpHeaders({ 'Content-Type': 'application/json', Accept: 'q=0.8;application/json;q=0.9' });

//LigaUtil.urlIdMission()
    async getToken() {
        let data = {};
        const result = this.http.post(LigaUtil.urlIdMission() + '/mission' , '', { headers: this.headers, });
        await result.toPromise().then(datos => {
          console.log(datos);
          data = datos;
          // console.log(">>datos>>>",JSON.stringify(datos));
          // if (datos['statusCode'] === "000") {
          //     data = datos;
          // }
        }).catch(err => {
          console.log(err);
          data = 400;
        });
        return data;
      }

      async saveSelfie(id: string, data: string) {
        let statusCode = 0;
        const result = this.http.post(LigaUtil.urlMiddleDaon(id) + `/selfie`, {data}, { headers: this.headers, });
        await result.toPromise().then(datos => {
          console.log('resultados');
          console.log(datos);
          if (datos['errorType'] || datos['errorMessage']) {
            statusCode = 400;
          } else {
            statusCode = 200;
          }
        }).catch(err => {
          console.log(err);
          statusCode = 400;
        });
        return statusCode;
      }

  }